import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import time
import scipy as sp
import os

import sys
sys.path.append("../src/")
from main import prepare_EVS, weight_features, draw_projections
from clusters import get_clusters_for_waves, match_clusters
from nnp import nnp
from eval import *
from unify_dimensions import unify_with_autoencoder

def run():
    print("Running NNP-ae-073-027-distance.py...")
    start_time = time.time()

    # prepare EVS data
    survey = prepare_EVS()

    # TF clustering
    survey = unify_with_autoencoder(survey, extra_n=2)
    weight_features(survey, weights={"core": 0.73, "extra": 0.27})
    survey = get_clusters_for_waves(survey, columns="all")

    # average linkage as cluster matching strategy
    survey = match_clusters(survey, method="centroid")

    # project waves using a neural network
    nnp_df, loss, val_loss = nnp(survey, "all")

    # save a figure with the learning curve
    px = 1 / plt.rcParams['figure.dpi']  # pixel in inches
    fig, ax = plt.subplots(1, 1, figsize=(1080 * px, 720 * px))
    ax.plot(loss)
    ax.plot(val_loss)
    ax.legend(['train', 'val'], loc='upper left')
    fig.suptitle("NNP-ae-073-027 learning curve", fontsize=16)
    #plt.savefig( "NNP-ae-073-027-learning-curve.png", dpi=200)

    projections = []
    for i, wave in enumerate(survey.waves):
        projection = nnp_df[nnp_df[survey.wave_col] == i + 1][["x", "y"]].to_numpy()
        projections.append(projection)

    #draw_projections(survey, projections, suptitle="NNP-ae-073-027", save=True)

    # evaluate the projections with evaluation metrics
    # static metrics first
    static_result_df = pd.DataFrame(
        columns=["wave", "stress", "spearman", "pearson", "kendall"])
    for i in range(survey.num_waves - 1, -1, -1):
        high_dim = survey[i].df.to_numpy()
        low_dim = projections[i]

        num_partitions = 20

        # distance preservation metrics
        print("Computing distance preservation metrics on wave", i, "with size", len(high_dim), "...")
        print("Computing distance list in high dimensions on wave", i, "...")
        dist_list_high_full = sp.spatial.distance.pdist(high_dim, metric="euclidean")
        np.save("dist_list_high", dist_list_high_full)
        del dist_list_high_full
        print("Computing distance list in low dimensions on wave", i, "...")
        dist_list_low_full = sp.spatial.distance.pdist(low_dim, metric="euclidean")
        np.save("dist_list_low", dist_list_low_full)
        del dist_list_low_full

        dist_list_high_full = np.load("dist_list_high.npy", mmap_mode='r')
        dist_list_low_full = np.load("dist_list_low.npy", mmap_mode='r')

        stress_sum_diff, stress_sum_power = 0, 0
        spearman_partial, pearson_partial, kendall_partial = [], [], []
        partition_size = len(dist_list_high_full) // num_partitions + 1
        for part in range(num_partitions):
            print("Partition", part, "of", num_partitions)
            indexes = np.arange(part * partition_size, min((part + 1) * partition_size, len(dist_list_high_full)))
            dist_list_high = dist_list_high_full[indexes]
            dist_list_low = dist_list_low_full[indexes]

            print("Computing normalized stress on wave", i, "...")
            stress_sum_diff += np.sum((dist_list_high - dist_list_low) ** 2)
            stress_sum_power += np.sum(dist_list_high ** 2)

            print("Computing spearman correlation on wave", i, "...")
            spearman_partial.append(metric_spearman_correlation_from_dists(dist_list_high, dist_list_low))
            print("Computing pearson correlation on wave", i, "...")
            pearson_partial.append(metric_pearson_correlation_from_dists(dist_list_high, dist_list_low))
            print("Computing kendall tau on wave", i, "...")
            kendall_partial.append(metric_kendall_tau_from_dists(dist_list_high, dist_list_low))

        stress = stress_sum_diff / stress_sum_power
        spearman = np.mean(spearman_partial)
        pearson = np.mean(pearson_partial)
        kendall = np.mean(kendall_partial)

        static_result_df = static_result_df._append({
            "wave": i,
            "stress": stress,
            "spearman": spearman,
            "pearson": pearson,
            "kendall": kendall,
        }, ignore_index=True)

    # save and pring whole dataframes
    static_result_df.to_csv("static_results_NNP_ae_073_027_distance.csv")

    with pd.option_context('display.max_rows', None,
                           'display.max_columns', None,
                           'display.precision', 3,
                           ):
        print(static_result_df)

    print("Execution time:", time.time() - start_time)


if __name__ == '__main__':
    run()
