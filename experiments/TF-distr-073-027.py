import pandas as pd
import numpy as np
import time
import scipy as sp
import os

import sys
sys.path.append("../src/")
from main import prepare_EVS, weight_features, draw_projections
from clusters import get_clusters_for_waves, match_clusters
from distances import dr
from eval import *
from unify_dimensions import fill_missing_values_by_distribution


def run():
    print("Running TF-distr-073-027.py...")
    start_time = time.time()

    # prepare EVS data
    survey = prepare_EVS()

    # TF clustering
    survey = fill_missing_values_by_distribution(survey)
    weight_features(survey, weights={"core": 0.73, "extra": 0.27})
    survey = get_clusters_for_waves(survey, columns="all")

    # average linkage as cluster matching strategy
    survey = match_clusters(survey, method="centroid")

    # project waves one by one
    projections = []
    for i in range(0, survey.num_waves):
        projection = dr(survey[i].df)
        projections.append(projection)

    draw_projections(survey, projections, suptitle="TF-distr-073-027", save=True)

    # evaluate the projections with evaluation metrics
    # static metrics first
    static_result_df = pd.DataFrame(
        columns=["wave", "neighbourhood_preservation", "neighbourhood_hit", "trustworthiness", "continuity", "stress",
                 "spearman", "pearson", "kendall", "cluster_size", "cluster_adjacency"])
    for i in range(survey.num_waves - 1, -1, -1):
        high_dim = survey[i].df.to_numpy()
        low_dim = projections[i]

        # k for k-nearest neighbours
        k = range(1, 21)
        preservations = []
        hits = []
        trusts_sklearn = []
        conts_sklearn = []

        num_partitions = 20

        for k_val in k:
            preservations_partial = []
            trusts_sklearn_partial = []
            conts_sklearn_partial = []

            print("Computing neighbourhood hit on wave", i, "with k =", k_val / 4, "% ...")
            neighbourhood_hit = metric_neighbourhood_hit(low_dim, np.array(survey[i].cluster_labels), k=k_val / 4)

            partition_size = len(high_dim) // num_partitions + 1
            for part in range(num_partitions):
                print("Partition", part, "of", num_partitions)
                indexes = np.arange(part * partition_size, min((part + 1) * partition_size, len(high_dim)))
                high_dim_partial = high_dim[indexes, :]
                low_dim_partial = low_dim[indexes, :]
                dist_matrix_high = sp.spatial.distance.cdist(high_dim_partial, high_dim, metric="euclidean")
                dist_matrix_low = sp.spatial.distance.cdist(low_dim_partial, low_dim, metric="euclidean")

                print("Computing neighbourhood preservation on wave", i, "with k =", k_val, "% ...")
                neighbourhood_preservation = metric_neighbourhood_preservation_from_dists(dist_matrix_high,
                                                                                          dist_matrix_low, k=k_val)
                print("Computing trustworthiness on wave", i, "with k =", k_val, "% ...")
                trustworthiness_sklearn = metric_trust_sklearn(high_dim_partial, low_dim_partial, k=k_val)
                print("Computing continuity on wave", i, "with k =", k_val, "% ...")
                continuity_sklearn = metric_continuity_sklearn(high_dim_partial, low_dim_partial, k=k_val)

                preservations_partial.append(neighbourhood_preservation)
                trusts_sklearn_partial.append(trustworthiness_sklearn)
                conts_sklearn_partial.append(continuity_sklearn)

            preservations.append(np.mean(preservations_partial))
            hits.append(neighbourhood_hit)
            trusts_sklearn.append(np.mean(trusts_sklearn_partial))
            conts_sklearn.append(np.mean(conts_sklearn_partial))

        print("---Finished computing neighbourhood metrics---")

        # distance preservation metrics
        print("Computing distance preservation metrics on wave", i, "with size", len(high_dim), "...")
        print("Computing distance list in high dimensions on wave", i, "...")
        dist_list_high_full = sp.spatial.distance.pdist(high_dim, metric="euclidean")
        np.save("dist_list_high", dist_list_high_full)
        del dist_list_high_full
        print("Computing distance list in low dimensions on wave", i, "...")
        dist_list_low_full = sp.spatial.distance.pdist(low_dim, metric="euclidean")
        np.save("dist_list_low", dist_list_low_full)
        del dist_list_low_full

        dist_list_high_full = np.load("dist_list_high.npy", mmap_mode='r')
        dist_list_low_full = np.load("dist_list_low.npy", mmap_mode='r')

        stress_sum_diff, stress_sum_power = 0, 0
        spearman_partial, pearson_partial, kendall_partial = [], [], []
        partition_size = len(dist_list_high_full) // num_partitions + 1
        for part in range(num_partitions):
            print("Partition", part, "of", num_partitions)
            indexes = np.arange(part * partition_size, min((part + 1) * partition_size, len(dist_list_high_full)))
            dist_list_high = dist_list_high_full[indexes]
            dist_list_low = dist_list_low_full[indexes]

            print("Computing normalized stress on wave", i, "...")
            stress_sum_diff += np.sum((dist_list_high - dist_list_low) ** 2)
            stress_sum_power += np.sum(dist_list_high ** 2)

            print("Computing spearman correlation on wave", i, "...")
            spearman_partial.append(metric_spearman_correlation_from_dists(dist_list_high, dist_list_low))
            print("Computing pearson correlation on wave", i, "...")
            pearson_partial.append(metric_pearson_correlation_from_dists(dist_list_high, dist_list_low))
            print("Computing kendall tau on wave", i, "...")
            kendall_partial.append(metric_kendall_tau_from_dists(dist_list_high, dist_list_low))

        stress = stress_sum_diff / stress_sum_power
        spearman = np.mean(spearman_partial)
        pearson = np.mean(pearson_partial)
        kendall = np.mean(kendall_partial)

        # cluster preservation metrics
        print("Computing static clustering on wave", i, "...")
        cluster_size, cluster_adjacency = metric_static_clustering(high_dim, low_dim,
                                                                   np.array(survey[i].cluster_labels))

        static_result_df = static_result_df._append({
            "wave": i,
            "neighbourhood_preservation": preservations,
            "neighbourhood_hit": hits,
            "trustworthiness": trusts_sklearn,
            "continuity": conts_sklearn,
            "stress": stress,
            "spearman": spearman,
            "pearson": pearson,
            "kendall": kendall,
            "cluster_size": cluster_size,
            "cluster_adjacency": cluster_adjacency
        }, ignore_index=True)

    if os.path.isfile("dist_list_high.npy"):
        os.remove("dist_list_high.npy")
    if os.path.isfile("dist_list_low.npy"):
        os.remove("dist_list_low.npy")

    # dynamic metrics
    dynamic_result_df = pd.DataFrame(
        columns=["waves", "cluster", "mahalanobis_dist_low", "cosine_sim_low", "mahalanobis_dist_high",
                 "cosine_sim_high"])
    for i in range(survey.num_waves - 1):
        high_dim = survey[i].df.to_numpy()
        low_dim = projections[i]
        labels = survey[i].cluster_labels
        high_dim_next = survey[i + 1].df.to_numpy()
        low_dim_next = projections[i + 1]
        labels_next = survey[i + 1].cluster_labels

        # compare pairs of clusters between the two waves
        for cluster in range(6):
            # similarity in low dimensions
            cluster_a = [low_dim[j] for j in range(len(low_dim)) if labels[j] == cluster]
            cluster_b = [low_dim_next[j] for j in range(len(low_dim_next)) if labels_next[j] == cluster]
            print("Computing mahalanobis distance on waves", i, "and", i + 1, "for cluster", cluster, "...")
            mahalanobis_dist_low = metric_cluster_movement(cluster_a, cluster_b)
            print("Computing cosine similarity on waves", i, "and", i + 1, "for cluster", cluster, "...")
            cosine_sims_low = metric_cosine_sim(cluster_a, cluster_b)

            # similarity in high dimensions
            cluster_a_high = [high_dim[j] for j in range(len(high_dim)) if labels[j] == cluster]
            cluster_b_high = [high_dim_next[j] for j in range(len(high_dim_next)) if labels_next[j] == cluster]
            print("Computing mahalanobis distance on waves", i, "and", i + 1, "for cluster", cluster, "...")
            mahalanobis_dist_high = metric_cluster_movement(cluster_a_high, cluster_b_high)
            print("Computing cosine similarity on waves", i, "and", i + 1, "for cluster", cluster, "...")
            cosine_sims_high = metric_cosine_sim(cluster_a_high, cluster_b_high)

            dynamic_result_df = dynamic_result_df._append({
                "waves": f"{i} -> {i + 1}",
                "cluster": cluster,
                "mahalanobis_dist_low": mahalanobis_dist_low,
                "cosine_sim_low": cosine_sims_low,
                "mahalanobis_dist_high": mahalanobis_dist_high,
                "cosine_sim_high": cosine_sims_high,
            }, ignore_index=True)

    # save and pring whole dataframes
    static_result_df.to_csv("static_results_tf_distr_073_027.csv")
    dynamic_result_df.to_csv("dynamic_results_tf_distr_073_027.csv")

    with pd.option_context('display.max_rows', None,
                           'display.max_columns', None,
                           'display.precision', 3,
                           ):
        print(static_result_df)
        print(dynamic_result_df)

    print("Execution time:", time.time() - start_time)


if __name__ == '__main__':
    run()
