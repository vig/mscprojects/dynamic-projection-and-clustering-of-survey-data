import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import time
import scipy as sp
import os
from matplotlib.ticker import FuncFormatter
from matplotlib.colors import FuncNorm

import sys
sys.path.append("../src/")
from main import prepare_EVS, weight_features, draw_projections
from clusters import get_clusters_for_waves, match_clusters
from nnp import nnp
from eval import *
from unify_dimensions import unify_with_autoencoder

def format_thousands(number, pos):
    if number >= 1000:
        return f"{round(number / 1000, 1)}k"
    return str(int(number))

def run():
    print("Running NNP-ae-091-009.py...")
    start_time = time.time()

    # prepare EVS data
    survey = prepare_EVS()

    # TF clustering
    survey = unify_with_autoencoder(survey, extra_n=2)
    weight_features(survey, weights={"core": 0.91, "extra": 0.09})
    survey = get_clusters_for_waves(survey, columns="all")

    # average linkage as cluster matching strategy
    survey = match_clusters(survey, method="centroid")

    # project waves using a neural network
    nnp_df, loss, val_loss = nnp(survey, "all")

    projections = []
    for i, wave in enumerate(survey.waves):
        projection = nnp_df[nnp_df[survey.wave_col] == i + 1][["x", "y"]].to_numpy()
        projections.append(projection)

    # evaluate the projections with evaluation metrics
    # static metrics first

    px = 1 / plt.rcParams['figure.dpi']  # pixel in inches
    dist_fig, dist_axs = plt.subplots(1, 5, figsize=(720 * px, 720 * px))
    dist_dense_fig_linear, dist_dense_axs_linear = plt.subplots(1, 5, figsize=(720 * px, 720 * px))
    dist_dense_fig_sr, dist_dense_axs_sr = plt.subplots(1, 5, figsize=(720 * px, 720 * px))
    neighbour_fig, neighbour_axs = plt.subplots(1, 5, figsize=(720 * px, 720 * px))
    neighbour_dense_fig_linear, neighbour_dense_axs_linear = plt.subplots(1, 5, figsize=(720 * px, 720 * px))
    neighbour_dense_fig_sr, neighbour_dense_axs_sr = plt.subplots(1, 5, figsize=(720 * px, 720 * px))

    for i in range(survey.num_waves - 1, -1, -1):
        high_dim = survey[i].df.to_numpy()
        low_dim = projections[i]
        num_partitions = 20

        '''
        # k for k-nearest neighbours
        k = range(1, 21)
        neighbour_ax = neighbour_axs[i]
        neighbour_ax = neighbour_axs[i]
        neighbour_dense_ax_linear = neighbour_dense_axs_linear[i]
        neighbour_dense_ax_sr = neighbour_dense_axs_sr[i]
        neighbour_ax.set_box_aspect(1)
        neighbour_dense_ax_linear.set_box_aspect(1)
        neighbour_dense_ax_sr.set_box_aspect(1)
        neighbour_ax.tick_params(axis='both', which='major', labelsize=3)
        neighbour_dense_ax_linear.tick_params(axis='both', which='major', labelsize=3)
        neighbour_dense_ax_sr.tick_params(axis='both', which='major', labelsize=3)
        neighbour_ax.set_title("Wave " + str((i + 1)), size='large')
        neighbour_dense_ax_linear.set_title("Wave " + str((i + 1)), size='large')
        neighbour_dense_ax_sr.set_title("Wave " + str((i + 1)), size='large')
        neighbour_ax.set_xlabel("no. of kNN of x", size='small')
        if i == 0:
            neighbour_ax.set_ylabel("no. of kNN of x present\n in kNN of P(x)", size='small')

        neighbours_all, isin_all = np.array([]), np.array([])
        for k_val in k:
            partition_size = len(high_dim) // num_partitions + 1

            for part in range(num_partitions):
                print("Partition", part, "of", num_partitions)
                indexes = np.arange(part * partition_size, min((part + 1) * partition_size, len(high_dim)))
                high_dim_partial = high_dim[indexes, :]
                low_dim_partial = low_dim[indexes, :]
                dist_matrix_high = sp.spatial.distance.cdist(high_dim_partial, high_dim, metric="euclidean")
                dist_matrix_low = sp.spatial.distance.cdist(low_dim_partial, low_dim, metric="euclidean")

                print("Computing neighbourhood preservation on wave", i, "with k =", k_val, "% ...")
                isin, k_neighbours = num_neighbours_from_dists(dist_matrix_high, dist_matrix_low, k=k_val)
                neighbours_all = np.append(neighbours_all, k_neighbours)
                isin_all = np.append(isin_all, isin)


        neighbours_all = np.array(neighbours_all)
        isin_all = np.array(isin_all)

        neighbour_ax.scatter(neighbours_all, isin_all, c='g', alpha=0.5, s=0.1)
        Z, xedges, yedges = np.histogram2d(neighbours_all, isin_all, bins=20, density=False)
        hist_max = np.max(Z)
        hist_min = np.min(Z)
        hist_mean_linear = (hist_max + hist_min) // 2
        hist_mean_sr = ((np.sqrt(hist_max) + np.sqrt(hist_min)) / 2) ** 2
        map_linear = neighbour_dense_ax_linear.pcolormesh(xedges, yedges, Z.T, cmap='magma')
        colorbar = plt.colorbar(map_linear, location='bottom', shrink=1, pad=0.05,
                                ticks=[hist_min, hist_mean_linear, hist_max],
                                format=FuncFormatter(format_thousands))
        colorbar.ax.tick_params(labelsize=5)
        map_sr = neighbour_dense_ax_sr.pcolormesh(xedges, yedges, Z.T, cmap='magma',
                                                  norm=FuncNorm(functions=(lambda x: x ** (1 / 2), lambda x: x ** 2)))
        colorbar = plt.colorbar(map_sr, location='bottom', shrink=1, pad=0.05, ticks=[hist_min, hist_mean_sr, hist_max],
                                format=FuncFormatter(format_thousands))
        colorbar.ax.tick_params(labelsize=5)

        print("---Finished computing neighbourhood metrics---")
        '''

        # distance preservation metrics
        print("Computing distance preservation metrics on wave", i, "with size", len(high_dim), "...")
        print("Computing distance list in high dimensions on wave", i, "...")
        np.random.seed(1)
        random_indexes = np.random.uniform(0, len(high_dim), min(50000, len(high_dim))).astype(int)
        dist_list_high_full = sp.spatial.distance.pdist(high_dim[random_indexes], metric="euclidean")
        np.save("dist_list_high", dist_list_high_full)
        np.save("dist_list_high_NNP_ae_091_009_wave_" + str(i), dist_list_high_full)
        del dist_list_high_full
        print("Computing distance list in low dimensions on wave", i, "...")
        dist_list_low_full = sp.spatial.distance.pdist(low_dim[random_indexes], metric="euclidean")
        np.save("dist_list_low", dist_list_low_full)
        np.save("dist_list_low_NNP_ae_091_009_wave_" + str(i), dist_list_low_full)
        del dist_list_low_full

        dist_list_high_full = np.load("dist_list_high.npy", mmap_mode='r')
        dist_list_low_full = np.load("dist_list_low.npy", mmap_mode='r')

        dist_ax = dist_axs[i]
        dist_dense_ax_linear = dist_dense_axs_linear[i]
        dist_dense_ax_sr = dist_dense_axs_sr[i]
        dist_ax.set_box_aspect(1)
        dist_dense_ax_linear.set_box_aspect(1)
        dist_dense_ax_sr.set_box_aspect(1)
        dist_ax.tick_params(axis='both', which='major', labelsize=3)
        dist_dense_ax_linear.tick_params(axis='both', which='major', labelsize=3)
        dist_dense_ax_sr.tick_params(axis='both', which='major', labelsize=3)
        dist_ax.set_title("Wave " + str((i + 1)), size='large')
        dist_dense_ax_linear.set_title("Wave " + str((i + 1)), size='large')
        dist_dense_ax_sr.set_title("Wave " + str((i + 1)), size='large')

        partition_size = len(dist_list_high_full) // num_partitions + 1

        dists_high_all, dists_low_all = np.array([]), np.array([])
        for part in range(num_partitions):
            print("Partition", part, "of", num_partitions)
            indexes = np.arange(part * partition_size, min((part + 1) * partition_size, len(dist_list_high_full)))
            dist_list_high = dist_list_high_full[indexes]
            dist_list_low = dist_list_low_full[indexes]

            np.random.seed(1) # to always randomize the same points
            random_indexes = np.random.uniform(0, min(partition_size, len(indexes)),
                                               min(50000, len(indexes))).astype(int)

            dists_high_all = np.append(dists_high_all, dist_list_high[random_indexes])
            dists_low_all = np.append(dists_low_all, dist_list_low[random_indexes])

            np.save("dists_high_NNP_ae_091_009_wave_" + str(i), dists_high_all)
            np.save("dists_low_NNP_ae_091_009_wave_" + str(i), dists_low_all)


        dist_ax.scatter(dists_high_all, dists_low_all, alpha=0.5, c='b', s=0.01)

        Z, xedges, yedges = np.histogram2d(dists_high_all, dists_low_all, bins=50, density=False)
        hist_max = np.max(Z)
        hist_min = np.min(Z)
        hist_mean_linear = (hist_max + hist_min) // 2
        hist_mean_sr = ((np.sqrt(hist_max) + np.sqrt(hist_min)) / 2) ** 2
        map_linear = dist_dense_ax_linear.pcolormesh(xedges, yedges, Z.T, cmap='magma')
        colorbar = plt.colorbar(map_linear, location='bottom', shrink=1, pad=0.05,
                                ticks=[hist_min, hist_mean_linear, hist_max],
                                format=FuncFormatter(format_thousands))
        colorbar.ax.tick_params(labelsize=5)
        map_sr = dist_dense_ax_sr.pcolormesh(xedges, yedges, Z.T, cmap='magma',
                                             norm=FuncNorm(functions=(lambda x: x ** (1 / 2), lambda x: x ** 2)))
        colorbar = plt.colorbar(map_sr, location='bottom', shrink=1, pad=0.05, ticks=[hist_min, hist_mean_sr, hist_max],
                                format=FuncFormatter(format_thousands))
        colorbar.ax.tick_params(labelsize=5)

        np.save("dists_high_NNP_distr_091_009_wave_" + str(i), dists_high_all)
        np.save("dists_low_NNP_distr_091_009_wave_" + str(i), dists_low_all)


    dist_fig.savefig("distance_scatterplot_NNP_ae_091_009", bbox_inches="tight", dpi=200)
    dist_dense_fig_linear.savefig("distance_scatterplot_density_NNP_ae_091_009_linear", bbox_inches="tight", dpi=200)
    dist_dense_fig_sr.savefig("distance_scatterplot_density_NNP_ae_091_009_square_root", bbox_inches="tight", dpi=200)
    #neighbour_fig.savefig("neighbour_scatterplot_NNP_ae_091_009", bbox_inches="tight", dpi=200)
    #neighbour_dense_fig_linear.savefig("neighbour_scatterplot_density_NNP_ae_091_009_linear", bbox_inches="tight", dpi=200)
    #neighbour_dense_fig_sr.savefig("neighbour_scatterplot_density_NNP_ae_091_009_square_root", bbox_inches="tight", dpi=200)

    print("Execution time:", time.time() - start_time)


if __name__ == '__main__':
    run()
