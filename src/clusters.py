from sklearn.cluster import KMeans
from distances import *

default_centers = [[0.44741313, 0.74702703, 0.73842986, 0.44895753, 0.45045045, 0.67160875,
  0.88458172, 0.67135135, 0.58996139, 0.95217503, 0.91176319],
 [0.31743743, 0.59955773, 0.52036663, 0.27840272, 0.28654296, 0.37163093,
  0.63320835, 0.46399385, 0.40480082, 0.72736596, 0.66657052],
 [0.70905652, 0.85575211, 0.86915888, 0.77080552, 0.8267134, 0.9073765,
  0.94831998, 0.85775478, 0.85714286, 0.94531598, 0.9346907 ],
 [0.519243, 0.65986005, 0.65263995, 0.51269084, 0.59860051, 0.63963104,
  0.64592875, 0.6091285, 0.62862595, 0.68781807, 0.61281807],
 [0.14514241, 0.36012485, 0.31037846, 0.09754194, 0.13350241, 0.14982442,
  0.23273508, 0.22467161, 0.20737417, 0.40538432, 0.26576928],
 [0.34994979, 0.50655832, 0.49216349, 0.31638212, 0.37520923, 0.37633525,
  0.36510545, 0.38184363, 0.40433367, 0.51057549, 0.344624]]


# centroids = "propagate" | "default" | "custom"
# columns = "core" | "all"
# per-timeframe clustering
def get_clusters_for_waves(dataset, k=6, columns="all"):
    for i, wave in enumerate(dataset.waves):
        kmeans_instance = KMeans(n_clusters=k, init="k-means++", n_init=20)
        train_df = wave.df
        if columns == "core":
            train_df = wave.df[wave.core_cols]
        else:
            train_df = wave.df
        wave.df = train_df  #comment this line if you want to keep the original size of df and not only the columns used for clustering
        kmeans_instance.fit(train_df)
        wave.cluster_centers = kmeans_instance.cluster_centers_
        wave.cluster_labels = kmeans_instance.labels_
        wave.cluster_distortions = kmeans_instance.inertia_ / len(train_df)

        #print(wave.cluster_centers)

    return dataset


cluster_colors = ["#d65f5f", "#6acc64", "#4878d0", "#956cb4", "#ee854a", "#8c613c"]
# global clustering
def get_clusters_for_waves_partition(df, k=6, columns="all"):
    kmeans_instance = KMeans(n_clusters=k, init="k-means++", n_init=20)
    train_df = df
    if columns == "core":
        train_df = df[core_cols]

    kmeans_instance.fit(train_df)
    centers = kmeans_instance.cluster_centers_
    labels = kmeans_instance.labels_
    distortion = kmeans_instance.inertia_ / len(train_df)

    return {
        "df": train_df,
        "centers": centers,
        "labels": labels,
        "distortion": distortion,
    }

# method = "pearson" | "centroid" | "average"
def match_clusters(dataset, method="centroid", transform=False):
    closest_pairs = []
    if method == "pearson":
        for i in range(dataset.num_waves - 1, 0, -1):
            closest_pairs.append(pearson(dataset[i].df, dataset[i].cluster_labels, dataset[i - 1].df, dataset[i - 1].cluster_labels, transform))

    elif method == "centroid":
        for i in range(dataset.num_waves - 1, 0, -1):
            closest_pairs.append(centroid_distances(dataset[i].cluster_centers, dataset[i - 1].cluster_centers))

    elif method == "average":
        for i in range(dataset.num_waves - 1, 0, -1):
            closest_pairs.append(pointwise_distances(dataset[i].df[dataset[i].core_cols], dataset[i].cluster_labels, dataset[i - 1].df[dataset[i].core_cols], dataset[i - 1].cluster_labels))

    # match the cluster label using a recursive search
    core_nodes = [ClusterMatchNode(i, i, dataset.num_waves - 1) for i in map(lambda x: x[0], closest_pairs[0])]
    def recursive_search(node, closest_pairs, wave):
        if wave == -1:
            return
        current_layer = closest_pairs[0]
        for pair in current_layer:
            if pair[0] == node.old_label:
                new_node = ClusterMatchNode(pair[1], pair[0], wave)
                node.add_child(new_node)
                recursive_search(new_node, closest_pairs[1:], wave - 1)
                break

    for node in core_nodes:
        recursive_search(node, closest_pairs, dataset.num_waves - 2)
    for node in core_nodes:
        node.update_label(node.old_label)
    for node in core_nodes:
        node.print()

    label_map = {wave.text: {} for wave in dataset.waves}

    def recursive_label_map(node):
        label_map[dataset[node.wave].text][node.old_label] = node.new_label
        for child in node.children:
            recursive_label_map(child)
    for node in core_nodes:
        recursive_label_map(node)

    for wave in dataset.waves:
        wave.cluster_labels = [label_map[wave.text][label] for label in wave.cluster_labels]
        #cluster["labels"] = [label_map[wave_name][label] for label in cluster["labels"]]
    #dataset.waves = dataset.waves[::-1]

    return dataset

class ClusterMatchNode():
    def __init__(self, old_label, new_label, wave):
        self.old_label = old_label
        self.children = []
        self.new_label = new_label
        self.wave = wave

    def add_child(self, child):
        self.children.append(child)

    def update_label(self, new_label):
        self.new_label = new_label
        for child in self.children:
            child.update_label(new_label)

    def print(self, indent=0):
        print(" " * indent, self.old_label, "->", self.new_label)
        for child in self.children:
            child.print(indent + 2)