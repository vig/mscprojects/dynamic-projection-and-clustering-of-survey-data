trust_questions_values = [i for i in range(-5, 5) if i != 0]
missing_values = [-1, -2, -3, -4, -5]
allowed_values = [val for val in trust_questions_values if val not in missing_values]
print(allowed_values)

core_cols = [
    "E069_03",
    "E069_04",
    "E069_05",
    "E069_06",
    "E069_17",
    "E069_07",
    "E069_08",
]

wave1_cols = core_cols + []
wave2_cols = core_cols + ["E069_09"]
wave3_cols = core_cols + ["E069_16", "E069_09"]
wave4_cols = core_cols + ["E069_16", "E069_09", "E069_12", "E069_11"]
wave5_cols = core_cols + ["E069_16", "E069_09", "E069_12", "E069_11"]

trust_questions_cols = [
    "E069_03",
    "E069_04",
    "E069_05",
    "E069_06",
    "E069_16", #
    "E069_17",
    "E069_07",
    "E069_08",
    "E069_09", #
    "E069_12", #
    "E069_11", #
]

trust_questions_labels = [
    "Education",
    "Press",
    "Trade Unions",
    "Police",
    "Health Care", #
    "Justice",
    "Parliament",
    "Civil Service",
    "Social Security", #
    "Political Parties", #
    "Government", #
]

col2label = {col: label for col, label in zip(trust_questions_cols, trust_questions_labels)}

country_col = "S009"
allowed_countries = ["DK", "FR", "DE", "IS", "IT", "NL", "ES", "SE", "GB"]
wave_col = "S002EVS"
allowed_waves = ["1", "2", "3", "4", "5"]