
from shapely import Point, Polygon

# implementation of the doubly connected edge list (DCEL) data structure
class Face:
    def __init__(self, label):
        self.outer_component = None
        self.inner_components = None
        self.cluster_label = label
        self.centroid = None

        self.points = []

    def add_boundary_point(self, point):
        self.points.append(point)

    def get_polygon(self):
        points = [(p.x, p.y) for p in self.points]
        return Polygon(points)

    def contains_point(self, vertex):
        point = Point(vertex.x, vertex.y)
        return point.within(self.get_polygon())

    def area(self):
        return self.get_polygon().area

    def __eq__(self, other):
        return self.cluster_label == other.cluster_label

class HalfEdge:
    def __init__(self, v1, v2, face):
        self.origin = None
        self.face = None
        self.twin = None
        self.next_edge = None
        self.prev_edge = None

        self.v1 = v1
        self.v2 = v2
        self.face1 = face
        self.face2 = None

    def __eq__(self, other):
        return (self.v1 == other.v1 and self.v2 == other.v2) or (self.v1 == other.v2 and self.v2 == other.v1)

class Vertex:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.incident_edges = []

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

class DCEL:
    def __init__(self):
        self.vertices = []
        self.half_edges = []
        self.faces = []

    def add_vertex(self, x, y):
        old_v = self.get_vertex(x, y)
        if old_v:
            return old_v
        v = Vertex(x, y)
        self.vertices.append(v)
        return v

    def get_vertex(self, x, y):
        return next((v for v in self.vertices if v.x == x and v.y == y), None)

    def add_half_edge(self, v1, v2, face):
        new_e = HalfEdge(v1, v2, face)
        old_e = None
        for e in self.half_edges:
            if e == new_e:
                old_e = e
                break
        if old_e:
            if old_e.face2 is None:
                old_e.face2 = face
            return old_e
        self.half_edges.append(new_e)
        return new_e

    def assign_twin_to_edge(self, edge):
        if edge.twin:
            return edge.twin
        for e in self.half_edges:
            if e != edge and e.origin == edge.next_edge.origin and e.next_edge.origin == edge.origin:
                edge.twin = e
                e.twin = edge
                return e
        #print("No twin found for edge", edge)
        return None

    def add_face(self, label):
        f = Face(label)
        self.faces.append(f)
        return f

    def get_faces(self, label):
        return [f for f in self.faces if f.cluster_label == label]