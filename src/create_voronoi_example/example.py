import pandas as pd
import numpy as np

from voronoi import VoronoiForMap
from plotter import draw_voronoi

# You'll probably want to rescale you data...
#    My experience is with the algorithm is
#    that the scale of the datapoints
#    has a big impact on the effect of
#    the hyperparameters on the final visualization
def rescale(arr, min_old=0, max_old=1, min_new=-15, max_new=15):
    """
    Takes an 1d-array as input and returns it as a linearly re-scaled 1d array.
    """
    return (max_new - min_new) / (max_old - min_old) *  (arr - max_old) + max_new

# Data
D = pd.read_csv('dataset.csv')

# projection and cluster indices
projection = D[['x', 'y']].values
cluster = np.array(D.cluster)

# Rescale the data
for i in range(projection.shape[1]):
    # get 1d array
    arr = projection.T[i]

    # setup rescaling parameters
    min_old, max_old = arr.min(), arr.max()
    min_new, max_new = -15, 15  # Set new range, just don't use [0, 1], trust me...

    # rescale
    scaled = rescale(arr, min_old, max_old, min_new, max_new)  # actual scaling

    # store result
    projection[:, i] = scaled

# Prepare input
# Clusterset = [(str [points])]
#    --> points = [point]
#    --> point = (float, float)
n_clusters = len(np.unique(cluster))
cluster_set = [(f'{i}', list(map(tuple, projection[cluster == i]))) for i in range(n_clusters)]
labels = np.arange(D.shape[0])

# Compute Map
vm = VoronoiForMap()
vm_map_results = vm.compute_voronoi(
    labels=labels,
    cluster_set=cluster_set,
    distance=0.2,  # default 0.2 - you can tinker and see what happens
    amount_of_random_points=10000,  # default 8000 - 10000 seems to work well for me
    random_operation='uniform',
    # default uniform - uniform works best for me. Options are 'circle', 'ellipse', 'uniform'
    radius=2,  # default 2 - don't recall if this matter for 'uniform', but try 0.01, 0.2, and 1.0, and see what happens
)

polygon_areas = {str(i): [] for i in range(n_clusters)}
for i in range(len(vm_map_results)):
    polygons = vm_map_results[str(i)][1]
    for j in range(len(polygons)):
        polygon = polygons[j]
        point_sum = 0
        for k in range(len(polygon) - 1):
            point_sum += polygon[k][0] * polygon[k + 1][1] - polygon[k + 1][0] * polygon[k][1]
        point_sum += polygon[-1][0] * polygon[0][1] - polygon[0][0] * polygon[-1][1]
        polygon_areas[str(i)].append(abs(point_sum / 2))

for i in range(n_clusters):
    print(f'Cluster {i} has {len(polygon_areas[str(i)])} polygons with a total area of {sum(polygon_areas[str(i)])}')
    print(f'Cluster {i} has {len(vm_map_results[str(i)][0])} points')
    print(f'Area to point ratio of cluster {i} is {sum(polygon_areas[str(i)]) / len(vm_map_results[str(i)][0])}')
    print(f'Area to polygon ratio of cluster {i} is {sum(polygon_areas[str(i)]) / len(polygon_areas[str(i)])}\n')


draw_voronoi(vm_map_results, projection, cluster)