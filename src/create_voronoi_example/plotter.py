import numpy as np
import plotly.graph_objects as go
import colorsys

def generate_unique_colors(n):
    """
    Thanks ChatGPT
    
    Generate n unique colors in RGBA format.

    :param n: The number of unique colors to generate.
    :return: A list of n unique colors in RGBA format.
    """
    colors = []
    colors_bright = []
    colors_dark = []
    for i in range(n):
        # Generate a color in HSL space.
        hue = i / n  # Hue varies from 0 to 1, evenly spaced.
        saturation = 0.7  # Choose a high saturation for vivid colors.
        lightness = 0.5  # Middle lightness for neither too bright nor too dark.

        # Convert HSL color to RGB.
        r, g, b = colorsys.hls_to_rgb(hue, lightness, saturation)

        # Convert RGB from 0-1 range to 0-255 range and append an alpha value of 255 for full opacity.
        rgba = (
            int(r * 255), 
            int(g * 255), 
            int(b * 255), 
            255
        )
        rgba_bright = tuple([min(255, int(c*1.8)) for c in rgba])
        rgba_dark = tuple([min(255, int(c * 1.2)) for c in rgba])

        colors.append(f'rgba{rgba}')
        colors_bright.append(f'rgba{rgba_bright}')
        colors_dark.append(f'rgba{rgba_dark}')

    return colors, colors_bright, colors_dark


def draw_voronoi(vm_map_results, projection, cluster):
    # prelimenaries
    n_clusters = np.unique(cluster).size
    
    # Country colors, datapoint colors
    colors, colors_bright, colors_dark = generate_unique_colors(n_clusters)
    print(colors)
    
    # Setup a canvas
    f = go.Figure()
    f.update_layout(
        width = 600, 
        height=600,
        margin=dict(l=20, r=20, t=20, b=20),
        showlegend=False,
        paper_bgcolor='rgba(0, 0, 0, 0)', # entire plot
        plot_bgcolor='rgba(0, 0, 0, 0)', # inside x and y
    )
    f.update_xaxes(showgrid=False, zeroline=False, showticklabels=False)
    f.update_yaxes(showgrid=False, zeroline=False, showticklabels=False)
    
    
    # This part is what you need to figure out
    # for cluster ...
    for i in range(len(vm_map_results)):
        # for the country's polygons
        polygons = vm_map_results[str(i)][-1]
        for j in range(len(polygons)):
            # get x and y of polygon vertices
            x, y = np.array(polygons[j]).T
    
            # draw polygon
            f.add_trace(
                go.Scattergl(
                    x=x, 
                    y=y, 
                    fill='toself',
                    fillcolor=colors[i],
                    name=f'Cluster {i}', 
                    marker=dict(size=0.1, color=colors[i]),
                    line=dict(color=colors[i]),
                    hoverinfo = 'skip',
                )
            )

    # for areas ...
    for i in range(len(vm_map_results)):
        areas = vm_map_results[str(i)][1]
        for j in range(len(areas)):
            x, y = np.array(areas[j]).T

            f.add_trace(
                go.Scattergl(
                    x=x,
                    y=y,
                    hoverinfo='skip',
                    line=dict(width=0.7, color=colors_dark[i]),
                    marker=dict(size=0.1, color=colors_dark[i]),
                )
            )
    
    # Add the datapoints
    for c in range(n_clusters):
        msk = cluster == c
        x, y = projection[msk].T
        
        f.add_trace(
            go.Scattergl(
                x=x,
                y=y,
                mode='markers',
                marker=dict(color=colors_bright[c], size=5),
                hoverinfo='skip'
            )
        )
    # Done
    f.show()