# This program has been developed by students from the bachelor
# Computer Science at Utrecht University within the Software Project course.
# Copyright Utrecht University
# (Department of Information and Computing Sciences)

"""
This is the basic voronoi class. You can get simple voronoi diagrams.
It is not optimised nor specifically made for hierarchical clustering.
"""
from __future__ import division

from scipy.spatial import Voronoi
import random
import math
from collections import Counter
import numpy as np
from numpy import linalg
from scipy import spatial
from matplotlib.patches import Polygon
import matplotlib.path as mpltPath
import matplotlib.pyplot as plt
from dcel import DCEL, Face, Vertex, HalfEdge

# from shapely.geometry import Point, Polygon

# These are custom types for this class.
point = (float, float)
edge = (point, point)
edges = [edge]
points = [point]
dictionaryPolygonsBoundary = dict[str, (points, [points], [points])]
dictionaryPolygonsEdges = dict[str, (points, [points], edges)]


def check_point_equals(x: tuple, y: tuple):
    """
    Checks if two points are approximately equal
    param x: First point
    type x: Tuple
    param y: Second point
    type y: Tuple

    returns: If two points are approximately equal
    rtype: Bool
    """
    return ("%.10f" % x[0]) == ("%.10f" % y[0]) and ("%.10f" % x[1]) == ("%.10f" % y[1])


def reorder_polygons(outer_edges, outer_points):
    """
    This function orders the edges into a polyline
    param outer_edges: List of boundary edges
    type outer_edges: List of a tuple of points
    param outer_points: List of point corresponding to the boundary edges
    type outer_points: List of points

    returns: Tuple with a list of polylines and a list of lists with the
    corresponding points
    rtype: A tuple of lists
    """

    if not outer_edges or not outer_points:
        return []
    result = []
    points_in_cluster = []
    while len(outer_edges) > 0:
        start = outer_edges[0]
        outer_edges.remove(start)
        match = False
        first_elem = start[0]
        partial = [start[0]]
        partial_points = []
        for (y, i) in zip(outer_edges, range(len(outer_edges))):
            if check_point_equals(start[1], y[0]):
                outer_edges.remove(y)
                match = True
                partial.append(y[0])
                partial_points.append(outer_points[i])
                start = y
                break
            elif check_point_equals(start[1], y[1]):
                outer_edges.remove(y)
                match = True
                partial.append(y[1])
                partial_points.append(outer_points[i])
                start = (y[1], y[0])
                break
        while match:
            match = False
            for (y, i) in zip(outer_edges, range(len(outer_edges))):
                if check_point_equals(start[1], y[0]):
                    outer_edges.remove(y)
                    match = True
                    partial.append(y[0])
                    partial_points.append(outer_points[i])
                    start = y
                    break
                elif check_point_equals(start[1], y[1]):
                    outer_edges.remove(y)
                    match = True
                    partial.append(y[1])
                    partial_points.append(outer_points[i])
                    start = (y[1], y[0])
                    break
        partial.append(first_elem)
        if len(partial) > 2:
            result.append(partial)
            points_in_cluster.append(partial_points)
    return result, points_in_cluster

def darker_color(color, factor=0.7):
    rgb_color = {
        'red': (1, 0, 0),
        'green': (0, 1, 0),
        'blue': (0, 0, 1),
        'cyan': (0, 1, 1),
        'magenta': (1, 0, 1),
        'yellow': (1, 1, 0),
        'orange': (1, 0.5, 0),
        'lime': (0.5, 1, 0),
    }
    r, g, b = rgb_color[color]
    rgb = (r * factor, g * factor, b * factor)
    hex = '#%02x%02x%02x' % (int(rgb[0] * 255), int(rgb[1] * 255), int(rgb[2] * 255))
    return hex

class VoronoiForMap:
    """
    This is the basic voronoi class. You can get simple voronoi diagrams.
    It is not optimised nor specifically made for hierarchical clustering.
    """

    def __init__(self):
        self.randomOperation = {
            "circle": self.circle,
            "ellipse": self.ellipse,
            "uniform": self.uniform,
        }

    def compute_voronoi(
        self,
        labels,
        cluster_set: [(str, points)],
        amount_of_random_points: int = 8000,
        distance: int = 0.2,
        random_operation="uniform",
        radius: int = 2,
    ) -> dictionaryPolygonsBoundary:
        """
        This computes the voronoi diagram. Assumption: all ids are unique.

        :param cluster_set: The method computes the voronoi diagram on this
                            point set, which needs to be divided into
                            sets per cluster.
        :type cluster_set: [(str, points)]
        :param amount_of_random_points: This defines how smooth the boundary
                                        will be.
        :type amount_of_random_points: int
        :param distance: This determines how close the points we consider
                         for the boundary can be to the points of the set.
        :type distance: int
        :param layer: Gives in which layer of hierarchical clustering this
                      voronoi is generated.
        :returns: The function returns a dictionary where for every cluster
                  the value is their points, the points' polygons and
                  the boundary polygon of the whole cluster.
        :rtype: dictionaryPolygonsBoundary
        """
        list_points, _ = self.__create_final_points_list(cluster_set)
        if not cluster_set:
            return {}

        new_cluster_set = []
        for (q, p) in cluster_set:
            non_duplicates = list(
                dict.fromkeys(map(lambda x_: (round(x_[0], 10), round(x_[1], 10)), p)))
            new_cluster_set.append(tuple([q, non_duplicates]))
        # get list without cluster labels
        list_points, labels = self.__create_final_points_list(new_cluster_set)

        # create random points
        random_points = self.randomOperation[random_operation](
            amount_of_random_points, new_cluster_set, distance
        )

        # create a new list of points. We add random points to cluster_set based on distance.
        labels.sort()

        # delete duplicate random points
        new_cluster_set, random_points = self.combine(labels,
                                                      new_cluster_set,
                                                      list_points,
                                                      random_points,
                                                      radius)
        list_points, _ = self.__create_final_points_list(new_cluster_set)
        # Setting up the voronoi diagram.
        voronoi_points = list_points + random_points
        voronoi = Voronoi(voronoi_points)

        # Create output.
        polygons_edges = self.__make_polygon_list(new_cluster_set, voronoi)
        output = {}

        for cluster in polygons_edges:
            output[cluster] = (
                polygons_edges[cluster][0],
                polygons_edges[cluster][1],
                self.__make_boundary_polygon(polygons_edges[cluster][2]),
            )

        list_points, _ = self.__create_final_points_list(cluster_set)
        for i in range(0, len(output)):
            output[str(i)] = tuple([cluster_set[i][1], output[str(i)][1], output[str(i)][2]])

        # Create DCEL from polygon edges
        dcel = DCEL()
        for i in range(0, len(output)):
            for boundary in output[str(i)][2]:
                face = dcel.add_face(str(i))
                centroid = (0, 0)
                for j in range(0, len(boundary)):
                    v1 = boundary[j]
                    v2 = boundary[(j + 1) % len(boundary)]
                    vertex1 = dcel.add_vertex(v1[0], v1[1])
                    vertex2 = dcel.add_vertex(v2[0], v2[1])
                    centroid = (centroid[0] + v1[0], centroid[1] + v1[1])
                    half_edge = dcel.add_half_edge(vertex1, vertex2, face)
                    vertex1.incident_edges.append(half_edge)
                    vertex2.incident_edges.append(half_edge)
                    face.add_boundary_point(vertex1)
                    face.add_boundary_point(vertex2)

                face.centroid = (centroid[0] / len(boundary), centroid[1] / len(boundary))


        has_only_face1 = 0
        has_only_face2 = 0
        has_both_faces = 0
        for edge in dcel.half_edges:
            if edge.face1 is not None and edge.face2 is None:
                has_only_face1 += 1
            elif edge.face1 is None and edge.face2 is not None:
                has_only_face2 += 1
            elif edge.face1 is not None and edge.face2 is not None:
                has_both_faces += 1
        print("has only face1", has_only_face1)
        print("has only face2", has_only_face2)
        print("has both faces", has_both_faces)

        # fix missing faces in half-edges
        for edge in dcel.half_edges:
            if edge.face1 is not None and edge.face2 is None:
                v1 = edge.v1
                v2 = edge.v2

                for face in dcel.faces:
                    if v1 not in face.points or v2 not in face.points:
                        if face.contains_point(v1) and face.contains_point(v2):
                            edge.face2 = face
                            break

        has_only_face1 = 0
        has_only_face2 = 0
        has_both_faces = 0
        for edge in dcel.half_edges:
            if edge.face1 is not None and edge.face2 is None:
                has_only_face1 += 1
            elif edge.face1 is None and edge.face2 is not None:
                has_only_face2 += 1
            elif edge.face1 is not None and edge.face2 is not None:
                has_both_faces += 1
        print("has only face1", has_only_face1)
        print("has only face2", has_only_face2)
        print("has both faces", has_both_faces)

        face_labels = np.unique(sorted([f.cluster_label for f in dcel.faces])).tolist()
        num_clusters = len(face_labels)
        boundary_table = np.zeros((num_clusters, num_clusters), dtype=int)
        for edge in dcel.half_edges:
            if edge.face2 is not None and edge.face1.cluster_label != edge.face2.cluster_label:
                boundary_table[int(edge.face1.cluster_label)][int(edge.face2.cluster_label)] += 1
                boundary_table[int(edge.face2.cluster_label)][int(edge.face1.cluster_label)] += 1
        new_col = np.array(face_labels)
        new_col.shape = (num_clusters, 1)
        boundary_table = np.hstack((new_col, boundary_table))
        new_row = np.array(["cluster"] + face_labels)
        new_row.shape = (1, num_clusters + 1)
        boundary_table = np.vstack((new_row, boundary_table))
        print(boundary_table.tolist())

        face2color = {
            0: 'red',
            1: 'cyan',
            2: 'lime',
            3: 'yellow',
            4: 'magenta',
            5: 'orange',
        }
        head_l = 0.05

        fig, ((boundary_ax, table_ax), (color_ax, point_ax)) = plt.subplots(2, 2)
        for v in dcel.vertices:
            pass
            #ax.plot(v.x, v.y, 'bo')

        for e in dcel.half_edges:
            if e.face2 is None:
                boundary_ax.plot([e.v1.x, e.v2.x], [e.v1.y, e.v2.y], 'r-')
            else:
                boundary_ax.plot([e.v1.x, e.v2.x], [e.v1.y, e.v2.y], 'b-')

        for f in dcel.faces:
            boundary_ax.text(f.centroid[0], f.centroid[1], f.cluster_label, fontsize=12)
            #edge = f.outer_component
            #ax.plot([edge.origin.x, edge.next_edge.origin.x], [edge.origin.y, edge.next_edge.origin.y], 'b-')
            #ax.arrow(edge.origin.x, edge.origin.y, edge.next_edge.origin.x - edge.origin.x, edge.next_edge.origin.y - edge.origin.y, head_width=0.3, head_length=0.2, fc='g', ec='g')
            #ax.text((edge.origin.x + edge.next_edge.origin.x) / 2, (edge.origin.y + edge.next_edge.origin.y) / 2, f.cluster_label, fontsize=10, color='g')

        table_ax.axis('off')
        table_ax.table(cellText=boundary_table.tolist(), cellLoc='center', loc='center', fontsize=16)

        sorted_faces = list(sorted(dcel.faces, key=lambda f: f.area(), reverse=True))
        for f in sorted_faces:
            points = [(p.x, p.y) for p in f.points]
            patch1 = Polygon(points, closed=True, edgecolor='black', facecolor=face2color[int(f.cluster_label)])
            color_ax.add_patch(patch1)
            patch2 = Polygon(points, closed=True, edgecolor='black', facecolor=face2color[int(f.cluster_label)])
            point_ax.add_patch(patch2)


        for edge in polygons_edges:
            for polygon in polygons_edges[edge][1]:
                for i in range(len(polygon) - 1):
                    color_ax.plot([polygon[i][0], polygon[i + 1][0]], [polygon[i][1], polygon[i + 1][1]], 'k-', linewidth=0.3, alpha=0.5)
                    point_ax.plot([polygon[i][0], polygon[i + 1][0]], [polygon[i][1], polygon[i + 1][1]], 'k-', linewidth=0.3, alpha=0.5)
                color_ax.plot([polygon[-1][0], polygon[0][0]], [polygon[-1][1], polygon[0][1]], 'k-', linewidth=0.3, alpha=0.5)
                point_ax.plot([polygon[-1][0], polygon[0][0]], [polygon[-1][1], polygon[0][1]], 'k-', linewidth=0.3, alpha=0.5)

        for cluster, points in cluster_set:
            label = int(cluster)
            dark_color = darker_color(face2color[label])
            for p in points:
                point_ax.plot(p[0], p[1], 'o', color=dark_color, markersize=3, alpha=1, markeredgecolor='black', markeredgewidth=0.5)
        color_ax.set_xlim(-20, 20)
        color_ax.set_ylim(-20, 20)
        point_ax.set_xlim(-20, 20)
        point_ax.set_ylim(-20, 20)

        plt.show()

        print("haha")



        return output

    def combine(self, labels, clusterset, list_points, random_points, radius):
        """ we loop through the list of random points. For each point
        we calculate the euclidean distance to list_points. If the euclidean
        distance is smaller than 5, we add the point to the cluster_set with label
        equal to the label of the point it is closest to """
        rest = []
        for pt in random_points:
            # check of the min distance to a point in list_point is less or equal to 5
            distance, index = spatial.KDTree(list_points).query(pt)
            if (distance) < radius:
                clusterset[labels[index]][1].append(pt)
            else:
                rest.append(pt)
        return clusterset, rest

    def __create_final_points_list(self, cluster_set: [(str, points)]):
        """
        This computes the total list of input points.

        :param cluster_set: The method computes the voronoi diagram on this
                            point set, which needs to be divided into sets
                            per cluster.
        :type cluster_set: [(str, points)]
        :returns: The full list of points.
        :rtype: points
        """
        output = []
        labels = []
        for index, cluster in enumerate(cluster_set):
            output += cluster[1]
            labels += [index]*len(cluster[1])
        return output, labels

    def __make_polygon_list(
        self, cluster_set: [(str, points)], voronoi: Voronoi
    ) -> dictionaryPolygonsEdges:
        """
        This computes the total list of polygons and their edges.

        :param cluster_set: The method computes the voronoi diagram on this
                            point set, which needs to be divided into sets
                            per cluster.
        :type cluster_set: [(str, points)]
        :param voronoi: The voronoi computed based on the input list.
        :type voronoi: Voronoi
        :returns: List of polygons and their edges per cluster.
        :rtype: dictionaryPolygonsEdges
        """
        if not cluster_set:
            raise Exception("__make_polygon_list: Invalid input.")
        # Possible place for a bug: if there are duplicates of points and so
        # duplicates of edges. The round could also be a cause for a bug
        # later on.
        output = {}
        index_add = 0
        i = 0

        for cluster in cluster_set:
            polygons_cluster = []
            edges_cluster = []
            i += 1
            for current_index in range(len(cluster[1])):
                current_point = cluster[1][current_index]
                polygon = []
                region = voronoi.point_region[index_add + current_index]

                for vertex in voronoi.regions[region]:
                    if vertex == -1:
                        polygon += [
                            (
                                round(current_point[0], 10),
                                round(current_point[1], 10),
                            )
                        ]
                    else:
                        polygon += [
                            (
                                round(voronoi.vertices[vertex][0], 10),
                                round(voronoi.vertices[vertex][1], 10),
                            )
                        ]
                polygon += [polygon[0]]

                for i in range(len(polygon) - 1):
                    edges_cluster += [((polygon[i], polygon[i + 1]), tuple(current_point))]

                polygons_cluster += [polygon]

            index_add += len(cluster[1])
            output[cluster[0]] = (cluster[1], polygons_cluster, edges_cluster)
        return output

    def __make_boundary_polygon(self, polygons_edges: edges) -> [points]:
        """
        This computes a boundary polygon based on inputted edges.

        :param polygons_edges: All edges of the polygons within a cluster.
        :type polygons_edges: edges
        :returns: Boundary polygon of the inputted edges.
        :rtype: [points]
        """
        if not polygons_edges:
            raise Exception("__make_boundary_polygon: Invalid input.")

        # Sort edges based on highest point
        edges_sorted = []
        points_sorted = []
        for (polygon_edge, point_in_polygon) in polygons_edges:
            res = tuple(sorted(polygon_edge))
            edges_sorted.append(res)
            points_sorted.append(point_in_polygon)
        # Remove double edges
        count = Counter(edges_sorted)
        outer_edges = []
        outer_points = []
        for edge_ in list(count.keys()):
            (a, b) = edge_
            if (a != b) and (count[(a, b)] % 2 == 1):
                corresponding_point = points_sorted[edges_sorted.index(edge_)]
                outer_edges.append((a, b))
                outer_points.append(corresponding_point)
        # Make outer polylines of the clusters
        (final, points_in_cluster) = reorder_polygons(
            outer_edges=outer_edges, outer_points=outer_points
        )
        toberemoved = []

        # we have to check for donuts
        for indexOuter in range(0, len(final)):
            for indexInner in range(0, len(final)):
                if indexInner != indexOuter:
                    # make list
                    points = [list(point) for point in final[indexInner]]

                    # make polygon outline
                    path = mpltPath.Path(final[indexOuter])

                    # check if points are in polygon
                    inside2 = path.contains_points(points)

                    # if all points lay inside the polygon, we remove the inner circle of the donut
                    if all(inside2):
                        toberemoved.append(indexInner)

        toberemoved.sort()
        for i in reversed(toberemoved):
            final.pop(i)
        return final

    # We filter out all random points too close to the points given.
    def __clean_random_points(
        self, list_points: points, random_points: points, distance: float
    ) -> points:
        """
        This filters the random points on whether they are too close to the
        points of the input.

        :param list_points: The full list of points.
        :type list_points: points
        :param random_points: The full list of random points for the boundary.
        :type random_points: points
        :param distance: This determines how close the points we consider
                         for the boundary can be to the points of the set.
        :type distance: int
        :returns: Only the random points further away than distance from all
                  points in list_points.
        :rtype: points
        """
        if distance < 0:
            raise Exception("__clean_random_points: Invalid distance.")
        output = []
        for x in random_points:
            can_be_added = True
            for (y, z) in list_points:
                if math.dist(x, (y, z)) < distance:
                    can_be_added = False
            if can_be_added:
                output += [x]
        return output

    # Within the bounding rectangle we give back an amount of random points.
    def __random_points_in_rectangle(
        self,
        amount: int,
        x_min: float,
        x_max: float,
        y_min: float,
        y_max: float,
        border: float,
        seed=None,
    ) -> points:
        """
        This computes random points in the given rectangle.

        :param amount: The amount of random points wanted.
        :type amount: int
        :param x_min: Minimum x-coordinate of the point set.
        :type x_min: float
        :param x_max: Maximum x-coordinate of the point set.
        :type x_max: float
        :param y_min: Minimum y-coordinate of the point set.
        :type y_min: float
        :param y_max: Maximum y-coordinate of the point set.
        :type y_max: float
        :param border: The border needed around the min-max rectangle.
        :type border: float
        :param seed: For testing purposes, this is to make the function
                     deterministic.
        :type seed: int
        :returns: A list of random points in the given rectangle.
        :rtype: points
        """

        if x_min > x_max or y_min > y_max or border < 0 or amount < 0:
            raise Exception("__random_points_in_rectangle: Invalid input.")
        random.seed(a=seed)
        output = []
        for i in range(amount):
            x = random.random() * (x_max - x_min + 10 * border) + x_min - 5 * border
            y = random.random() * (y_max - y_min + 10 * border) + y_min - 5 * border
            output += [(x, y)]
        return output

    def get_random_points_circle(self, cluster_set, amount_of_points):
        """
        This generates random points in a circle.

        :param amount_of_points: The amount of random points wanted.
        :type amount_of_points: int
        :param cluster_set: A list of (cluster number, cluster points).
        :type cluster_set: list

        :returns: A tuple of a list of random points per cluster and circles.
        :rtype: tuple
        """
        result = []
        circles = []
        amount_of_cluster = len(cluster_set)
        amount_per_cluster = max(50, int(amount_of_points / amount_of_cluster))
        for _, cluster_points in cluster_set:
            x, y, r = self.make_circle(cluster_points)
            center = (x, y)
            radius = r

            # Make circle
            circle = {"radius": radius, "center": center}
            circles.append(circle)

            sigma = radius / 5

            random_points = []
            for _ in range(amount_per_cluster):
                Z = np.random.standard_normal()
                while Z < -1 or Z > 1:
                    Z = np.random.standard_normal()
                phi = random.random() * 2 * np.pi
                random_radius = radius + sigma * (Z + 3) + 0.1
                random_point = (
                    random_radius * math.cos(phi) + center[0],
                    random_radius * math.sin(phi) + center[1],
                )
                random_points.append(random_point)
            result.extend(random_points)

        return result, circles

    def clean_random_points_circle(self, random_points, circles):
        """
        This function points that would are to close to other clusters

        :param random_points: A list af random points per cluster
        :type random_points: list
        :param circles: A list of circles per cluster
        :type circles: list

        :returns: A list of the cleaned random points
        :rtype: list
        """
        if not circles or not random_points:
            raise Exception("get_random_points_ellipse: Invalid input")

        cleaned_points = {}
        for p in random_points:
            cleaned_points[str(p)] = p

        for circle in circles:
            radius = circle["radius"]
            center = circle["center"]
            for p in random_points:
                check = (center[0] - p[0]) ** 2 + (center[1] - p[1]) ** 2 - (1.1 * radius) ** 2
                if check <= 0 and str(p) in cleaned_points:
                    cleaned_points.pop(str(p))
        return list(cleaned_points.values())

    def clean_random_points_ellipse(self, random_points, ellipses):
        """
        This function points that would are to close to other clusters

        :param random_points: A list af random points per cluster
        :type random_points: list
        :param ellipses: A list of ellipses per cluster
        :type ellipses: list

        :returns: A list of the cleaned random points
        :rtype: list
        """

        if not ellipses or not random_points:
            raise Exception("get_random_points_ellipse: Invalid input")

        cleaned_points = {}
        for p in random_points:
            cleaned_points[str(p)] = p

        for ellipse, point_set in ellipses:
            new_points = []
            center = ellipse[0]
            inv_rotation_matrix = linalg.inv(ellipse[2])

            check_ellipse = []
            for p in point_set:
                point_ = np.matrix(p).transpose() - np.matrix(center).transpose()
                rotated_point = np.matmul(
                    np.matrix([[-1, 0], [0, 1]]),
                    np.matmul(inv_rotation_matrix, point_),
                ).tolist()
                check_ellipse.append((rotated_point[0][0], rotated_point[1][0]))
            radii = self.getMinVolEllipse(np.array(check_ellipse))[1].tolist()

            for p in random_points:
                point_ = np.matrix(p).transpose() - np.matrix(center).transpose()
                rotated_point = np.matmul(inv_rotation_matrix, point_).tolist()

                new_points.append((rotated_point[0][0], rotated_point[1][0]))

                check = ((rotated_point[0][0]) ** 2) / ((radii[0] * 0.9) ** 2) + (
                    (rotated_point[1][0]) ** 2
                ) / ((radii[1] * 0.9) ** 2)

                if check <= 1 and str(p) in cleaned_points:
                    cleaned_points.pop(str(p))

        return list(cleaned_points.values())

    def get_random_points_ellipse(
        self, cluster_set: [(str, points)], amount_of_random_points: int = 2000
    ):
        """
        This generates random points in an ellipse.

        :param amount_of_points: The amount of random points wanted.
        :type amount_of_points: int
        :param cluster_set: A list of (cluster number, cluster points).
        :type cluster_set: list

        :returns: A tuple of a list of random points per cluster and ellipses.
        :rtype: tuple
        """

        if amount_of_random_points < 1 or not cluster_set:
            raise Exception("get_random_points_ellipse: Invalid input")

        result = []
        ellipses = []
        amount_of_cluster = len(cluster_set)
        amount_per_cluster = max(50, int(amount_of_random_points / amount_of_cluster))
        for _, cluster_points in cluster_set:
            # Make ellipse
            ellipse = self.getMinVolEllipse(np.array(cluster_points))
            center = ellipse[0].tolist()
            radii = ellipse[1].tolist()
            rotation_matrix = ellipse[2]

            sigma = self.get_max(center, cluster_points)[0] / 5

            random_points = []
            for _ in range(amount_per_cluster):
                Z = np.random.standard_normal()
                while Z < -1 or Z > 1:
                    Z = np.random.standard_normal()

                phi = random.random() * 2 * np.pi
                width = radii[0] + sigma * (Z + 3) + 0.1
                height = radii[1] + sigma * (Z + 3) + 0.1

                random_point = np.matrix([[width * math.cos(phi)], [height * math.sin(phi)]])
                rotated_point = (rotation_matrix * random_point).tolist()
                random_points.append(
                    (
                        rotated_point[0][0] + center[0],
                        rotated_point[1][0] + center[1],
                    )
                )
            result.extend(random_points)
            ellipses.append((ellipse, random_points))

        return result, ellipses

    def get_bounding_box(self, points_list):
        """
        This computes the minimum and maximum x- and y-coordinates of the
        given set.

        :param points_list: The full list of points.
        :type points_list: points
        :returns: The minimum and maximum x- and y-coordinates.
        :rtype: (float, float, float, float)
        """
        if not points_list:
            raise Exception("get_bounding_box: No points were given.")

        x, y = self.unzip(points_list)

        x_min = min(x)
        x_max = max(x)
        y_min = min(y)
        y_max = max(y)

        return float(x_min), float(x_max), float(y_min), float(y_max)

    @staticmethod
    def get_max(center, point_set):
        """
        This computes the minimum and maximum distance from a given points
        given a point set, returning the corresponding points.

        :param center: A center points of a cluster.
        :type center: point
        :param point_set: The points of a cluster.
        :type point_set: points

        :returns: The minimum and maximum x- and y-coordinates.
        :rtype: (float, float, float, float)
        """
        max = (0, None)
        min = (10000, None)
        for i in range(len(point_set)):
            x, y = point_set[i]
            dist = (center[0] - x) ** 2 + (center[1] - y) ** 2
            if dist < min[0]:
                min = (dist, (x, y))
            elif dist > max[0]:
                max = (dist, (x, y))
        return max[0], max[1], min[0], min[1]

    @staticmethod
    def unzip(points_list):
        """
        Unzipping a list (i.e. [(a, b)] -> [[a],[b]])

        :param points_list: A list of tuples.
        :type points_list: list

        :returns: Unzipped list.
        :rtype: list
        """
        return [[i for i, j in points_list], [j for i, j in points_list]]

    def get_average(self, points_list):
        """
        Computes average points

        :param points_list: A list of points.
        :type points_list: list

        :returns: The average point in the point set.
        :rtype: tuple
        """
        length = len(points_list)
        x, y = self.unzip(points_list)
        return sum(x) / length, sum(y) / length

    def getMinVolEllipse(self, point_set=None, tolerance=0.01):
        """
        Find the minimum volume ellipsoid which holds all the points

        Based on work by Nima Moshtagh
        http://www.mathworks.com/matlabcentral/fileexchange/9542
        and also by looking at:
        http://cctbx.sourceforge.net/current/python/scitbx.math.minimum_covering_ellipsoid.html
        Which is based on the first reference anyway!

        Here, P is a numpy array of N dimensional points like this:
        P = [[x,y,z,...], <-- one point per line
             [x,y,z,...],
             [x,y,z,...]]

        Returns:
        (center, radii, rotation)

        """
        (N, d) = np.shape(point_set)
        d = float(d)

        # Q will be our working array
        Q = np.vstack([np.copy(point_set.T), np.ones(N)])
        QT = Q.T

        # initializations
        err = 1.0 + tolerance
        u = (1.0 / N) * np.ones(N)

        # Khachiyan Algorithm
        while err > tolerance:
            V = np.dot(Q, np.dot(np.diag(u), QT))
            M = np.diag(
                np.dot(QT, np.dot(linalg.inv(V), Q))
            )  # M the diagonal vector of an NxN matrix
            j = np.argmax(M)
            maximum = M[j]
            step_size = (maximum - d - 1.0) / ((d + 1.0) * (maximum - 1.0))
            new_u = (1.0 - step_size) * u
            new_u[j] += step_size
            err = np.linalg.norm(new_u - u)
            u = new_u

        # center of the ellipse
        center = np.dot(point_set.T, u)

        # the A matrix for the ellipse
        A = (
            linalg.inv(
                np.dot(point_set.T, np.dot(np.diag(u), point_set))
                - np.array([[a * b for b in center] for a in center])
            )
            / d
        )

        # Get the values we'd like to return
        U, s, rotation = linalg.svd(A)
        radii = 1.0 / np.sqrt(s)

        return center, radii, rotation

    def make_circle(self, point_set):
        # Convert to float and randomize order
        shuffled = [(float(x), float(y)) for (x, y) in point_set]
        random.shuffle(shuffled)

        # Progressively add points to circle or recompute circle
        c = None
        for (i, p) in enumerate(shuffled):
            if c is None or not self.is_in_circle(c, p):
                c = self._make_circle_one_point(shuffled[: i + 1], p)
        return c

    # One boundary point known
    def _make_circle_one_point(self, point_set, p):
        c = (p[0], p[1], 0.0)
        for (i, q) in enumerate(point_set):
            if not self.is_in_circle(c, q):
                if c[2] == 0.0:
                    c = self.make_diameter(p, q)
                else:
                    c = self._make_circle_two_points(point_set[: i + 1], p, q)
        return c

    # Two boundary points known
    def _make_circle_two_points(self, point_set, p, q):
        circ = self.make_diameter(p, q)
        left = None
        right = None
        px, py = p
        qx, qy = q

        # For each point not in the two-point circle
        for r in point_set:
            if self.is_in_circle(circ, r):
                continue

            # Form a circumcircle and classify it on left or right side
            cross = self._cross_product(px, py, qx, qy, r[0], r[1])
            c = self.make_circumcircle(p, q, r)
            if c is None:
                continue
            elif cross > 0.0 and (
                left is None
                or self._cross_product(px, py, qx, qy, c[0], c[1])
                > self._cross_product(px, py, qx, qy, left[0], left[1])
            ):
                left = c
            elif cross < 0.0 and (
                right is None
                or self._cross_product(px, py, qx, qy, c[0], c[1])
                < self._cross_product(px, py, qx, qy, right[0], right[1])
            ):
                right = c

        # Select which circle to return
        if left is None and right is None:
            return circ
        elif left is None:
            return right
        elif right is None:
            return left
        else:
            return left if (left[2] <= right[2]) else right

    @staticmethod
    def make_diameter(a, b):
        cx = (a[0] + b[0]) / 2
        cy = (a[1] + b[1]) / 2
        r0 = math.hypot(cx - a[0], cy - a[1])
        r1 = math.hypot(cx - b[0], cy - b[1])
        return cx, cy, max(r0, r1)

    @staticmethod
    def make_circumcircle(a, b, c):
        # Mathematical algorithm from Wikipedia: Circumscribed circle
        ox = (min(a[0], b[0], c[0]) + max(a[0], b[0], c[0])) / 2
        oy = (min(a[1], b[1], c[1]) + max(a[1], b[1], c[1])) / 2
        ax = a[0] - ox
        ay = a[1] - oy
        bx = b[0] - ox
        by = b[1] - oy
        cx = c[0] - ox
        cy = c[1] - oy
        d = (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)) * 2.0
        if d == 0.0:
            return None
        x = (
            ox
            + (
                (ax * ax + ay * ay) * (by - cy)
                + (bx * bx + by * by) * (cy - ay)
                + (cx * cx + cy * cy) * (ay - by)
            )
            / d
        )
        y = (
            oy
            + (
                (ax * ax + ay * ay) * (cx - bx)
                + (bx * bx + by * by) * (ax - cx)
                + (cx * cx + cy * cy) * (bx - ax)
            )
            / d
        )
        ra = math.hypot(x - a[0], y - a[1])
        rb = math.hypot(x - b[0], y - b[1])
        rc = math.hypot(x - c[0], y - c[1])
        return x, y, max(ra, rb, rc)

    @staticmethod
    def is_in_circle(c, p):
        _MULTIPLICATIVE_EPSILON = 1 + 1e-14
        return (
            c is not None and math.hypot(p[0] - c[0], p[1] - c[1]) <= c[2] * _MULTIPLICATIVE_EPSILON
        )

    # Returns twice the signed area of the triangle defined by (x0, y0), (x1, y1), (x2, y2).
    @staticmethod
    def _cross_product(x0, y0, x1, y1, x2, y2):
        return (x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0)

    def circle(self, amount_of_random_points, cluster_set, distance):
        r_points, circles = self.get_random_points_circle(cluster_set, amount_of_random_points)
        return self.clean_random_points_circle(r_points, circles)

    def ellipse(self, amount_of_random_points, cluster_set, distance):
        r_points, ellipses = self.get_random_points_ellipse(cluster_set, amount_of_random_points)
        return self.clean_random_points_ellipse(r_points, ellipses)

    def uniform(self, amount_of_random_points, cluster_set, distance):
        list_points, _ = self.__create_final_points_list(cluster_set)
        x_min, x_max, y_min, y_max = self.get_bounding_box(list_points)

        # Setting up the list that will create the boundary.
        random_points = self.__random_points_in_rectangle(
            amount_of_random_points, x_min, x_max, y_min, y_max, border=5
        )
        return self.__clean_random_points(list_points, random_points, distance)