import numpy as np

# generic classes to work with a static and dynamic dataset
class Dataset:
    def __init__(self, df, core_cols, all_cols=None):
        self.df = df
        self.core_cols = core_cols
        self.text = ""
        self.cluster_labels = None
        self.cluster_centers = None
        self.cluster_distortions = None
        if all_cols is None:
            self.all_cols = self.df.columns
        else:
            self.all_cols = all_cols

    def shuffle_clusters(self):
        replacement_map = {value: value for value in np.unique(self.cluster_labels)}
        replacement_values = np.random.permutation(np.unique(self.cluster_labels))
        for i, key in enumerate(replacement_map.keys()):
            replacement_map[key] = replacement_values[i]
        self.cluster_labels = np.array([replacement_map[value] for value in self.cluster_labels])
        new_centers = np.zeros(self.cluster_centers.shape)
        for key, value in replacement_map.items():
            new_centers[key] = self.cluster_centers[replacement_values[value]]
        self.cluster_centers = np.array(new_centers)


class DynamicDataset(Dataset):
    def __init__(self, df, core_cols, all_cols=None):
        super().__init__(df, core_cols, all_cols)
        self.num_waves = -1
        self.wave_col = ""
        self.waves = []

    def init_waves(self, wave_col="", wave_dfs=None, wave_labels=None):
        """
            Either set up wave_col OR (wave_dfs AND wave_labels)
        """
        if wave_col == "" and wave_dfs is None and wave_labels is None:
            raise ValueError("Missing argument: either setup wave_col OR (wave_dfs AND wave_labels)")
        if wave_col != "" and (wave_dfs is not None or wave_labels is not None):
            raise ValueError("Either setup wave_col OR (wave_dfs AND wave_labels)")
        if wave_dfs is not None and wave_labels is None:
            raise ValueError("wave_labels must be set if wave_dfs is set")
        if wave_dfs is None and wave_labels is not None:
            raise ValueError("wave_dfs must be set if wave_labels is set")

        if wave_col != "":
            self.wave_col = wave_col
            wave_labels = list(self.df[wave_col].unique())
            wave_labels.sort()
            self.num_waves = len(wave_labels)
            for label in wave_labels:
                wave_df = self.df[self.df[wave_col] == label]
                wave_df = wave_df.drop(self.wave_col, axis=1)
                wave_df = wave_df.loc[:, wave_df.notna().all()]
                wave = Dataset(wave_df, self.core_cols)
                wave.text = f"Wave {label}"
                self.waves.append(wave)
        else:
            self.num_waves = len(wave_labels)
            for i in range(self.num_waves):
                wave_df = wave_dfs[i]
                wave = Dataset(wave_df, self.core_cols)
                wave.text = f"Wave {wave_labels[i]}"
                self.waves.append(wave)

    def update_wave_dfs(self):
        for i in range(self.num_waves):
            self.waves[i].df = self.df[self.df[self.wave_col] == i + 1]
            self.waves[i].df = self.waves[i].df.drop(self.wave_col, axis=1)

    def __getitem__(self, item):
        if isinstance(item, int):
            if item < 0 or item >= self.num_waves:
                raise IndexError("Index out of bounds")
            return self.waves[item]
        raise TypeError("Invalid index type. Expecting int")

    def get_wave(self, index):
        return self[index]
