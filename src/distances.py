import numpy as np
import scipy as sp
import math
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KernelDensity
from ortools.linear_solver import pywraplp
from scipy.spatial.distance import mahalanobis
from consts import *

# optimizer for cluster matching
def find_closest_pairs(sim_matrix, unique=True):
    closest_pairs = []
    if not unique:
        for i in range(sim_matrix.shape[0]):
            closest = np.argmax(sim_matrix[i, :])
            closest_pairs.append((i, closest, round(sim_matrix[i, closest], 2)))
    else:
        # get sorted labels by maximum possible similarity
        solver = pywraplp.Solver.CreateSolver("GLOP")
        n, m = sim_matrix.shape
        X = [[solver.IntVar(0, 1, f"X_{i}_{j}") for j in range(m)] for i in range(n)]

        for i in range(n):
            solver.Add(solver.Sum(X[i]) == 1)
        for j in range(m):
            solver.Add(solver.Sum([X[i][j] for i in range(n)]) == 1)

        solver.Maximize(solver.Sum([sim_matrix[i][j] * X[i][j] for i in range(n) for j in range(m)]))
        status = solver.Solve()
        if status == pywraplp.Solver.OPTIMAL:
            for i in range(n):
                for j in range(m):
                    if X[i][j].solution_value() == 1:
                        closest_pairs.append((i, j, round(sim_matrix[i, j], 2)))

    return closest_pairs

# methods for cluster-matching
def centroid_distances(centers1, centers2):
    # calculate closest pairs of centers for wave 1 and wave 2
    dist_matrix = sp.spatial.distance.cdist(centers1, centers2, metric="euclidean")
    sim_matrix = 1 / (1 + dist_matrix)
    closest_pairs = find_closest_pairs(sim_matrix)
    return closest_pairs

def pointwise_distances(points1, labels1, points2, labels2):
    point_dist_matrix = sp.spatial.distance.cdist(points1, points2, metric="euclidean")
    dist_matrix = np.zeros((len(np.unique(labels1)), len(np.unique(labels2))))
    unique_labels1 = np.unique(labels1)
    unique_labels2 = np.unique(labels2)
    for i, label1 in enumerate(unique_labels1):
        for j, label2 in enumerate(unique_labels2):
            relevant_distances = point_dist_matrix[labels1 == label1][:, labels2 == label2]
            dist = np.mean(relevant_distances)
            dist_matrix[i, j] = round(dist, 2)

    sim_matrix = 1 / (1 + dist_matrix)
    closest_pairs = find_closest_pairs(sim_matrix)
    return closest_pairs


def pearson(df1, labels1, df2, labels2, transform=False):
    labels1 = np.array(labels1)
    labels2 = np.array(labels2)
    count_matrix1 = np.zeros((len(np.unique(labels1)), len(trust_questions_cols), len(allowed_values)))
    count_matrix2 = np.zeros((len(np.unique(labels1)), len(trust_questions_cols), len(allowed_values)))

    cols1 = [(i, col) for i, col in enumerate(trust_questions_cols) if col in df1.columns]
    cols1_labels = [col for i, col in cols1]
    for label in range(len(np.unique(labels1))):
        cluster = df1[labels1 == label]
        #cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].apply(lambda x: ((x - x.min()) / (x.max() - x.min())) * (4 - 1) + 1, axis=0)
        if transform:
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(0, 1)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(1/4, 2)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(3/4, 3)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(1, 4)
        else:
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(0, 1)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(1 / 3, 2)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(2 / 3, 3)
            cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].replace(1, 4)
        cluster.loc[:, cols1_labels] = cluster.loc[:, cols1_labels].astype(int)
        for question_index, question in cols1:
            for answer_index, answer in enumerate(allowed_values):
                try:
                    count_matrix1[label, question_index, answer_index] = cluster[question].value_counts()[answer]
                except KeyError:
                    count_matrix1[label, question_index, answer_index] = 0

    cols2 = [(i, col) for i, col in enumerate(trust_questions_cols) if col in df2.columns]
    cols2_labels = [col for i, col in cols2]
    for label in range(len(np.unique(labels2))):
        cluster = df2[labels2 == label]
        #cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].apply(lambda x: ((x - x.min()) / (x.max() - x.min())) * (4 - 1) + 1, axis=0)
        if transform:
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(0, 1)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(1 / 4, 2)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(3 / 4, 3)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(1, 4)
        else:
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(0, 1)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(1 / 3, 2)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(2 / 3, 3)
            cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].replace(1, 4)
        cluster.loc[:, cols2_labels] = cluster.loc[:, cols2_labels].astype(int)
        for question_index, question in cols2:
            for answer_index, answer in enumerate(allowed_values):
                try:
                    count_matrix2[label, question_index, answer_index] = cluster[question].value_counts()[answer]
                except KeyError:
                    count_matrix2[label, question_index, answer_index] = 0

    unique_labels1 = np.unique(labels1)
    unique_labels2 = np.unique(labels2)
    sim_matrix = np.zeros((len(unique_labels1), len(unique_labels2)))
    common_cols = list(map(lambda x: x[0], sorted(set(cols1).intersection(set(cols2)), key=lambda x: x[0])))
    for label1 in unique_labels1:
        for label2 in unique_labels2:
            values1 = count_matrix1[label1, common_cols, :].flatten()
            #values1_norm = (values1 / np.sum(values1)).flatten()
            values2 = count_matrix2[label2, common_cols, :].flatten()
            #values2_norm = (values2 / np.sum(values2)).flatten()
            pearson = sp.stats.pearsonr(values1, values2)[0]
            #print("pearson", pearson, "pearson_norm", sp.stats.pearsonr(values1.flatten(), values2.flatten())[0])
            sim_matrix[label1, label2] = pearson

    closest_pairs = find_closest_pairs(sim_matrix)
    return closest_pairs

def dr(df, n_components=2):
    tsne = TSNE(n_components=n_components, verbose=1, perplexity=70, n_iter=300)
    common_cols = [col for col in trust_questions_cols if col in df.columns]
    tsne_results = tsne.fit_transform(df[common_cols])
    tsne_results = MinMaxScaler().fit_transform(tsne_results)
    return tsne_results




