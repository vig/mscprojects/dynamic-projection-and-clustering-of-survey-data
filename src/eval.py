import numpy as np
from sklearn.neighbors import KNeighborsClassifier, NearestNeighbors
import sklearn.manifold as manifold
from scipy import spatial
from scipy import stats
from voronoi import VoronoiForMap
import matplotlib.pyplot as plt

#metrics taken from:
# https://github.com/mespadoto/dlmp
# https://github.com/EduardoVernier/dynamic-projections/blob/master/Metrics/template.ipynb

def compute_distance_list(X):
    return spatial.distance.pdist(X, 'euclidean')

def compute_distance_matrix(X):
    D = spatial.distance.pdist(X, 'euclidean')
    return spatial.distance.squareform(D)

def shepard_diagram(x, y, colors="blue"):
    plt.scatter(x, y, c=colors)
    plt.title('Shepard diagram')
    plt.show()

def compute_neighbours(X, k=1, algorithm='ball_tree'):
    n_items = X.shape[0]
    k = max(int((k / 100) * n_items), 1)  # Avoid divisions by 0
    dists, nbrs = NearestNeighbors(n_neighbors=k, metric='euclidean', algorithm=algorithm).fit(X).kneighbors(X)
    return (dists, nbrs)

def num_neighbours_from_dists(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])

    n = D_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    isin = np.zeros(knn_proj.shape)
    for i in range(knn_proj.shape[0]):
        isin[i] = np.isin(knn_proj[i], knn_orig[i])

    return np.sum(isin, axis=1), np.ones(knn_proj.shape[0]) * k

def metric_neighbourhood_preservation_from_dists(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])

    n = D_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    isin = np.zeros(knn_proj.shape)
    for i in range(knn_proj.shape[0]):
        isin[i] = np.isin(knn_proj[i], knn_orig[i])

    out = float(np.sum(isin) / (n * k))
    return out

def metric_neighbourhood_preservation_from_dists_faster(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])

    n = D_high.shape[0]
    sum = 0
    for i in range(n):
        nn_orig = D_high[i].argsort()
        nn_proj = D_low[i].argsort()

        knn_orig = nn_orig[1:k + 1]
        knn_proj = nn_proj[1:k + 1]

        sum += np.sum(np.isin(knn_proj, knn_orig))

    out = float(sum / (n * k))
    return out


# k means percentage of samples
def metric_neighbourhood_preservation(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    # The fraction of the k-nearest neighbours of X_high that are also among k-nearest neighbours of X_low
    D_high = compute_distance_matrix(X_high)
    D_low = compute_distance_matrix(X_low)

    n = X_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    isin = np.zeros(knn_proj.shape)
    for i in range(knn_proj.shape[0]):
        isin[i] = np.isin(knn_proj[i], knn_orig[i])

    return float(np.sum(isin) / (n * k))

def metric_neighbourhood_hit(X, y, k=0.25):
    k = k / 100
    k = int(k * X.shape[0])
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X, y)

    neighbors = knn.kneighbors(X, return_distance=False)
    return np.mean(np.mean((y[neighbors] == np.tile(y.reshape((-1, 1)), k)).astype('uint8'), axis=1))

def metric_trust_2(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    n_items = X_high.shape[0]

    dists_nd, nbrs_nd = NearestNeighbors(n_neighbors=n_items, metric='euclidean',
                                         algorithm='ball_tree').fit(X_high).kneighbors(X_high)
    dists_md, nbrs_md = NearestNeighbors(n_neighbors=n_items, metric='euclidean',
                                         algorithm='kd_tree').fit(X_low).kneighbors(X_low)

    sum_i = 0
    for i in range(n_items):
        # Look for false neighbors in a k sized neighborhood
        knbrs_nd = nbrs_nd[i, 1:k + 1]
        knbrs_md = nbrs_md[i, 1:k + 1]
        U = np.setdiff1d(knbrs_nd, knbrs_md, assume_unique=True)

        # For each false neighbor in mD, find out its rank in nD and sum it
        sum_j = 0
        for j in U:
            sum_j += int(np.where(nbrs_nd[i] == j)[0] - 1) - k
        sum_i += int(sum_j)

    n = n_items
    out = float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)))
    return out


def metric_trust_sklearn(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    out = manifold.trustworthiness(X_high, X_low, n_neighbors=k)
    return out

def metric_trustworthiness_from_dists(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])
    n = D_high.shape[0]

    nn_orig = D_high.argsort(axis=1)
    nn_proj = D_low.argsort(axis=1)

    knn_orig = nn_orig[:, 1:k + 1]
    knn_proj = nn_proj[:, 1:k + 1]

    sum_i = 0

    for i in range(n):
        U = np.setdiff1d(knn_proj[i], knn_orig[i], assume_unique=True)
        sum_j = 0
        for j in range(U.shape[0]):
            where = np.where(nn_orig[i] == U[j])
            sum_j += where[0] - 1 - k
        sum_i += sum_j

    n = D_high.shape[1]
    val = 1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)
    out = float(val)
    return out

def trustworthiness_partial_sum(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])
    n = D_high.shape[0]

    nn_orig = D_high.argsort(axis=1)
    nn_proj = D_low.argsort(axis=1)

    knn_orig = nn_orig[:, 1:k + 1]
    knn_proj = nn_proj[:, 1:k + 1]

    sum_i = 0

    for i in range(n):
        U = np.setdiff1d(knn_proj[i], knn_orig[i], assume_unique=True)
        sum_j = 0
        for j in range(U.shape[0]):
            where = np.where(nn_orig[i] == U[j])
            sum_j += where[0] - 1 - k
        sum_i += sum_j
    return sum_i

def metric_trustworthiness(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    D_high = compute_distance_matrix(X_high)
    D_low = compute_distance_matrix(X_low)

    n = X_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    sum_i = 0

    for i in range(n):
        U = np.setdiff1d(knn_proj[i], knn_orig[i])
        sum_j = 0
        for j in range(U.shape[0]):
            sum_j += np.where(nn_orig[i] == U[j])[0] - k
        sum_i += sum_j

    val = 1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)

    return float((val).squeeze())

def metric_continuity_2(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    n_items = X_high.shape[0]

    dists_nd, nbrs_nd = NearestNeighbors(n_neighbors=n_items, metric='euclidean',
                                         algorithm='ball_tree').fit(X_high).kneighbors(X_high)
    dists_md, nbrs_md = NearestNeighbors(n_neighbors=n_items, metric='euclidean',
                                         algorithm='kd_tree').fit(X_low).kneighbors(X_low)

    sum_i = 0
    for i in range(n_items):
        # Look for false neighbors in a k sized neighborhood
        knbrs_nd = nbrs_nd[i, 1:k + 1]
        knbrs_md = nbrs_md[i, 1:k + 1]
        U = np.setdiff1d(knbrs_md, knbrs_nd, assume_unique=True)

        # For each false neighbor in nD, find out its rank in mD and sum it
        sum_j = 0
        for j in U:
            sum_j += int(np.where(nbrs_md[i] == j)[0] - 1) - k
        sum_i += int(sum_j)

    n = n_items
    out = float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)))
    return out


def metric_continuity_sklearn(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    out = manifold.trustworthiness(X_low, X_high, n_neighbors=k)
    return out

def metric_continuity_from_dists(D_high, D_low, k=1):
    k = k / 100
    k = int(k * D_high.shape[0])
    n = D_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    sum_i = 0

    for i in range(n):
        V = np.setdiff1d(knn_orig[i], knn_proj[i])
        sum_j = 0
        for j in range(V.shape[0]):
            sum_j += np.where(nn_proj[i] == V[j])[0] - k
        sum_i += sum_j

    n = D_high.shape[1]
    out = float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)).squeeze())
    return out

def metric_continuity(X_high, X_low, k=1):
    k = k / 100
    k = int(k * X_high.shape[0])
    D_high = compute_distance_matrix(X_high)
    D_low = compute_distance_matrix(X_low)

    n = X_high.shape[0]

    nn_orig = D_high.argsort()
    nn_proj = D_low.argsort()

    knn_orig = nn_orig[:, :k + 1][:, 1:]
    knn_proj = nn_proj[:, :k + 1][:, 1:]

    sum_i = 0

    for i in range(n):
        V = np.setdiff1d(knn_orig[i], knn_proj[i])
        sum_j = 0
        for j in range(V.shape[0]):
            sum_j += np.where(nn_proj[i] == V[j])[0] - k
        sum_i += sum_j

    return float((1 - (2 / (n * k * (2 * n - 3 * k - 1)) * sum_i)).squeeze())

def distance_preservation_metrics_from_dists(D_high, D_low, diagram=False):
    if diagram:
        shepard_diagram(D_high, D_low)

    stress = np.sum((D_high - D_low) ** 2) / np.sum(D_high ** 2)
    spearman = stats.spearmanr(D_high, D_low)[0]
    pearson = stats.pearsonr(D_high, D_low)[0]
    kendall = stats.kendalltau(D_high, D_low)[0]

    return {
        "stress": stress,
        "spearman": spearman,
        "pearson": pearson,
        "kendall": kendall
    }

def distance_preservation_metrics(X_high, X_low, diagram=False):
    D_high = compute_distance_list(X_high)
    D_low = compute_distance_list(X_low)

    if diagram:
        shepard_diagram(D_high, D_low)

    return {
        "stress": np.sum((D_high - D_low) ** 2) / np.sum(D_high ** 2),
        "spearman": stats.spearmanr(D_high, D_low)[0],
        "pearson": stats.pearsonr(D_high, D_low)[0],
        "kendall": stats.kendalltau(D_high, D_low)[0]
    }


def metric_normalized_stress_from_dists(D_high, D_low):
    return np.sum((D_high - D_low) ** 2) / np.sum(D_high ** 2)

def metric_spearman_correlation_from_dists(D_high, D_low):
    return stats.spearmanr(D_high, D_low)[0]

def metric_pearson_correlation_from_dists(D_high, D_low):
    return stats.pearsonr(D_high, D_low)[0]

def metric_kendall_tau_from_dists(D_high, D_low):
    return stats.kendalltau(D_high, D_low)[0]

def metric_normalized_stress(X_high, X_low):
    D_high = compute_distance_list(X_high)
    D_low = compute_distance_list(X_low)

    return np.sum((D_high - D_low) ** 2) / np.sum(D_high ** 2)

def metric_spearman_correlation(X_high, X_low):
    D_high = compute_distance_list(X_high)
    D_low = compute_distance_list(X_low)

    return stats.spearmanr(D_high, D_low)[0]

def metric_pearson_correlation(X_high, X_low):
    D_high = compute_distance_list(X_high)
    D_low = compute_distance_list(X_low)

    return stats.pearsonr(D_high, D_low)[0]

def metric_kendall_tau(X_high, X_low):
    D_high = compute_distance_list(X_high)
    D_low = compute_distance_list(X_low)

    return stats.kendalltau(D_high, D_low)[0]
def metric_static_clustering(X_high, X_low, label):
    label = np.array(label)
    n_clusters = len(np.unique(label))
    cluster_set = [(f'{i}', list(map(tuple, X_low[label == i]))) for i in range(n_clusters)]
    labels = np.arange(X_low.shape[0])
    vm = VoronoiForMap()
    vm_map_results, dcel = vm.compute_voronoi(
        labels=labels,
        cluster_set=cluster_set,
        distance=0.2,  # default 0.2 - you can tinker and see what happens
        amount_of_random_points=10000,  # default 8000 - 10000 seems to work well for me
        random_operation='uniform',
        # default uniform - uniform works best for me. Options are 'circle', 'ellipse', 'uniform'
        radius=2,
        # default 2 - don't recall if this matter for 'uniform', but try 0.01, 0.2, and 1.0, and see what happens
    )

    polygon_areas = {str(i): [] for i in range(n_clusters)}
    for i in range(len(vm_map_results)):
        polygons = vm_map_results[str(i)][1]
        for j in range(len(polygons)):
            polygon = polygons[j]
            point_sum = 0
            for k in range(len(polygon) - 1):
                point_sum += polygon[k][0] * polygon[k + 1][1] - polygon[k + 1][0] * polygon[k][1]
            point_sum += polygon[-1][0] * polygon[0][1] - polygon[0][0] * polygon[-1][1]
            polygon_areas[str(i)].append(abs(point_sum / 2))

    metric_cluster_size = []
    for i in range(n_clusters):
        cluster_len = len(X_low[label == i])
        metric_cluster_size.append((cluster_len, sum(polygon_areas[str(i)])))

    face_labels = np.unique(sorted([f.cluster_label for f in dcel.faces])).tolist()
    num_clusters = len(face_labels)
    boundary_table = np.zeros((num_clusters, num_clusters), dtype=float)
    for edge in dcel.half_edges:
        if edge.face2 is not None and edge.face1.cluster_label != edge.face2.cluster_label:
            edge_length = spatial.distance.euclidean((edge.v1.x, edge.v1.y), (edge.v2.x, edge.v2.y))
            boundary_table[int(edge.face1.cluster_label)][int(edge.face2.cluster_label)] += edge_length
            boundary_table[int(edge.face2.cluster_label)][int(edge.face1.cluster_label)] += edge_length

    metric_cluster_similarity = []
    for i in range(n_clusters):
        for j in range(i + 1, n_clusters):
            boudnary_len = boundary_table[i][j]
            # average distance between clusters in high dimensions
            point_dist_matrix = spatial.distance.cdist(X_high[label == i], X_high[label == j], metric="euclidean")
            high_dim_dist = np.mean(point_dist_matrix)
            high_dim_dist = 1 / (1 + high_dim_dist)
            metric_cluster_similarity.append((boudnary_len, high_dim_dist))

    return metric_cluster_size, metric_cluster_similarity

# computes mahalanobis distance between two clusters in two consecutive waves
def metric_cluster_movement(X_t1, X_t2):
    # mahalanobis in high dimensions
    X_t1 = np.array(X_t1)
    X_t2 = np.array(X_t2)
    cov_high_t1 = np.cov(X_t1.T)
    center_high_t1 = np.mean(X_t1, axis=0)
    cov_high_t2 = np.cov(X_t2.T)
    center_high_t2 = np.mean(X_t2, axis=0)
    dist_sum = 0
    for point in X_t2:
        dist_sum += spatial.distance.mahalanobis(center_high_t1, point, np.linalg.inv(cov_high_t1))
    for point in X_t1:
        dist_sum += spatial.distance.mahalanobis(center_high_t2, point, np.linalg.inv(cov_high_t2))
    dist_sum /= (X_t1.shape[0] + X_t2.shape[0])

    return dist_sum

def metric_cluster_movement_one_way(X_t1, X_t2):
    # mahalanobis in high dimensions
    cov_high_t1 = np.cov(X_t1.T)
    center_high_t1 = np.mean(X_t1, axis=0)
    dist_sum = 0
    for point in X_t2:
        dist_sum += spatial.distance.mahalanobis(center_high_t1, point, np.linalg.inv(cov_high_t1))
    dist_sum /= X_t2.shape[0]

    return dist_sum

def metric_cosine_sim(X_t1, X_t2):
    X_t1 = np.array(X_t1)
    X_t2 = np.array(X_t2)
    cov_1 = np.cov(X_t1.T)
    cov_2 = np.cov(X_t2.T)

    eigenvalues_1, eigenvectors_1 = np.linalg.eig(cov_1)
    eigenvalues_2, eigenvectors_2 = np.linalg.eig(cov_2)

    eigenvectors_1_sorted = eigenvectors_1[:, eigenvalues_1.argsort()]
    eigenvectors_2_sorted = eigenvectors_2[:, eigenvalues_2.argsort()]

    avg_sim = 0
    for i in range(len(eigenvectors_1_sorted)):
        v_1 = eigenvectors_1_sorted[i]
        v_2 = eigenvectors_2_sorted[i]
        sim = np.dot(v_1, v_2) / (np.linalg.norm(v_1) * np.linalg.norm(v_2))
        avg_sim += abs(sim)

    output = avg_sim / len(eigenvectors_1_sorted)
    return output







