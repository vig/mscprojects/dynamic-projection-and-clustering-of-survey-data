import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy import stats
from glob import glob
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from sklearn.manifold import TSNE


#methods = ['AE', 'VAE', 'h_spacer', 'tsne_s1', 'tsne_s4', 'dtsne', 'h_spacer', 'umap_s1', 'umap_s4', 'h_spacer', 'pca_s1', 'pca_s4']
#metrics = ['spat_pearson', 'spat_spearman' , 'spat_kendall', 'spat_stress_s', 'v_spacer_1', 'spat_nh_mean', 'spat_np_mean', 'spat_trust_mean', 'spat_cont_mean', 'v_spacer_2', 'stab_pearson', 'stab_spearman' , 'stab_kendall', 'stab_stress_s']
#invert_colormap = ['stab_stress_n', 'stab_stress_s', 'spat_stress_n', 'spat_stress_s']

# change static results into a dataframe
def collect_static_data():
    results_path = '../experiments/results/static_results_*_*.csv'
    csv_paths = sorted(glob(results_path))
    csv_paths = [p for p in csv_paths if 'distance' not in p]

    def cell_dr(x):
        x = x.split('_')
        return x[0]

    def cell_unification(x):
        x = x.split('_')
        return x[1]

    def cell_weight(x):
        x = x.split('_')
        if x[1] == 'core':
            return 'core'
        elif len(x) == 3:
            return x[2]
        return x[2] + "_" + x[3]

    def parse_cluster_metric(x):
        # x is in a form of '[(1,2),(3,4),(5,6)]'
        x = x[2:-2].split('), (')
        x = [i.split(', ') for i in x]
        x = [[float(j) for j in i] for i in x]
        return x

    df_metrics = pd.DataFrame()
    for p in csv_paths:
        name = p.split('static_results_')[1].split('.csv')[0]
        name = name.replace('tf', 'TF')
        df = pd.read_csv(p, sep=';')
        # Extract dataset and method into own column
        df.insert(0, 'name', name, True)
        df.insert(1, 'weight', df['name'].apply(cell_weight))
        df.insert(1, 'unification', df['name'].apply(cell_unification))
        df.insert(1, 'dr', df['name'].apply(cell_dr))
        df['cluster_size'] = df['cluster_size'].apply(parse_cluster_metric)
        df['cluster_adjacency'] = df['cluster_adjacency'].apply(parse_cluster_metric)
        df_metrics = pd.concat([df_metrics, df])

    return df_metrics

# change dynamic results into a dataframe
def collect_dynamic_data():
    results_path = '../experiments/results/dynamic_results_*_*.csv'
    csv_paths = sorted(glob(results_path))

    def cell_dr(x):
        x = x.split('_')
        return x[0]

    def cell_unification(x):
        x = x.split('_')
        return x[1]

    def cell_weight(x):
        x = x.split('_')
        if x[1] == 'core':
            return 'core'
        elif len(x) == 3:
            return x[2]
        return x[2] + "_" + x[3]

    df_metrics = pd.DataFrame()
    for p in csv_paths:
        name = p.split('dynamic_results_')[1].split('.csv')[0]
        name = name.replace('tf', 'TF')
        df = pd.read_csv(p, sep=';')
        # Extract dataset and method into own column
        df.insert(0, 'name', name, True)
        df.insert(1, 'weight', df['name'].apply(cell_weight))
        df.insert(1, 'unification', df['name'].apply(cell_unification))
        df.insert(1, 'dr', df['name'].apply(cell_dr))
        df_metrics = pd.concat([df_metrics, df])

    return df_metrics

# correct formatting of neighbourhood metrics
def average_neighbourhood_metrics(input_df):
    df = input_df.copy()
    def cell_avg(x):
        x = x[1:-1].split(', ')
        x = [float(i) for i in x]
        arr = np.array(x)
        return np.mean(arr)

    df['trustworthiness'] = df['trustworthiness'].apply(cell_avg)
    df['continuity'] = df['continuity'].apply(cell_avg)
    df['neighbourhood_hit'] = df['neighbourhood_hit'].apply(cell_avg)
    df['neighbourhood_preservation'] = df['neighbourhood_preservation'].apply(cell_avg)

    return df

# correct formatting of clustering metrics
def connect_clustering_metrics(static_df):
    df = static_df.groupby(['weight', 'unification', 'dr'])
    output_df_dict = {"weight": [], "unification": [], "dr": [], "cluster_size": [], "cluster_adjacency": []}
    for name, group in df:
        cluster_size = []
        for size in group['cluster_size'].values:
            cluster_size.extend(size)
        cluster_adjacency = []
        for adjacency in group['cluster_adjacency'].values:
            cluster_adjacency.extend(adjacency)
        #cluster_size = np.array(cluster_size)
        #cluster_adjacency = np.array(cluster_adjacency)

        output_df_dict["weight"].append(group['weight'].values[0])
        output_df_dict["unification"].append(group['unification'].values[0])
        output_df_dict["dr"].append(group['dr'].values[0])
        output_df_dict["cluster_size"].append(cluster_size)
        output_df_dict["cluster_adjacency"].append(cluster_adjacency)

    output_df = pd.DataFrame(output_df_dict)
    return output_df

# turn distances between clusters and waves into correlations
def compute_dynamic_from_raw(df):
    new_df = pd.DataFrame(columns=['dr', 'unification', 'weight', 'temp_spearman', 'temp_pearson', 'temp_kendall'])
    groupby = df.groupby(['dr', 'unification', 'weight'])
    for name, group in groupby:
        skip_waves = []

        mahalanobis_high = []
        mahalanobis_low = []
        cosine_high = []
        cosine_low = []

        for i in range(len(group)):
            waves = list(map(int, group['waves'][i].split(' -> ')))
            if waves[0] in skip_waves or waves[1] in skip_waves:
                continue

            mahalanobis_high.append(group['mahalanobis_dist_high'][i])
            mahalanobis_low.append(group['mahalanobis_dist_low'][i])
            cosine_high.append(group['cosine_sim_high'][i])
            cosine_low.append(group['cosine_sim_low'][i])

        high, low = [], []
        for i in range(len(mahalanobis_high)):
            high_val = 0.5 * (1 / (1 + mahalanobis_high[i])) + 0.5 * cosine_high[i]
            low_val = 0.5 * (1 / (1 + mahalanobis_low[i])) + 0.5 * cosine_low[i]
            high.append(1 - high_val)
            low.append(1 - low_val)
            #high.append(0.5 * mahalanobis_high[i] + 0.5 * (1 - cosine_high[i]))
            #low.append(0.5 * mahalanobis_low[i] + 0.5 * (1 - cosine_low[i]))
            #high.append(mahalanobis_high[i])
            #low.append(mahalanobis_low[i])
        high = np.array(high)
        low = np.array(low)

        new_df = new_df._append({
            'dr': group['dr'].iloc[0],
            'unification': group['unification'].iloc[0],
            'weight': group['weight'].iloc[0],
            #'temp_stress': np.sum((high - low) ** 2) / np.sum(high ** 2),
            'temp_spearman': stats.spearmanr(high, low)[0],
            'temp_pearson': stats.pearsonr(high, low)[0],
            'temp_kendall': stats.kendalltau(high, low)[0]
        }, ignore_index=True)

    return new_df

# merge static and dynamic metrics into one dataframe
def merge_static_dynamic_for_table(static_df, dynamic_df):
    static_df_avg = average_neighbourhood_metrics(static_df)
    cluster_df = connect_clustering_metrics(static_df)
    static_df_new = static_df_avg.drop(columns=['name', 'wave', 'cluster_size', 'cluster_adjacency'])

    dynamic_df = compute_dynamic_from_raw(dynamic_df)

    static_df_new = static_df_new.groupby(['dr', 'unification', 'weight']).agg({col: np.mean for col in static_df_new.columns[3:]}).reset_index()
    #static_mean = static_df[list(static_df.columns[3:])]
    dynamic_df = dynamic_df.groupby(['dr', 'unification', 'weight']).mean().reset_index()
    #dynamic_mean = dynamic_df[list(dynamic_df.columns[3:])]

    df = pd.merge(static_df_new, cluster_df, on=['weight', 'unification', 'dr'])
    df = pd.merge(df, dynamic_df, on=['weight', 'unification', 'dr'])

    return df

# merge static, clustering and dynamic metrics into one dataframe
def merge_all_for_table(metrics_df, methods):
    df = metrics_df.copy()
    for method in ['dr', 'unification', 'weight']:
        if method not in methods:
            df = metrics_df.drop(columns=[method])
    cols = list(df.columns)
    for i, col in enumerate(
            ['cluster_size_pearson', 'cluster_size_spearman', 'cluster_size_kendall', 'cluster_adjacency_pearson',
             'cluster_adjacency_spearman', 'cluster_adjacency_kendall']):
        cols.insert(11 + i, col)
    cols.remove('cluster_size')
    cols.remove('cluster_adjacency')
    df_dict = {col: [] for col in cols}
    for name, group in df.groupby(methods):
        cluster_size = []
        for size in group['cluster_size'].values:
            cluster_size.extend(size)
        cluster_adjacency = []
        for adjacency in group['cluster_adjacency'].values:
            cluster_adjacency.extend(adjacency)
        cluster_size = np.array(cluster_size)
        cluster_adjacency = np.array(cluster_adjacency)

        for method in methods:
            df_dict[method].append(group[method].values[0])
        df_dict['cluster_size_pearson'].append(stats.pearsonr(cluster_size[:, 0], cluster_size[:, 1])[0])
        df_dict['cluster_size_spearman'].append(stats.spearmanr(cluster_size[:, 0], cluster_size[:, 1])[0])
        df_dict['cluster_size_kendall'].append(stats.kendalltau(cluster_size[:, 0], cluster_size[:, 1])[0])
        df_dict['cluster_adjacency_pearson'].append(stats.pearsonr(cluster_adjacency[:, 0], cluster_adjacency[:, 1])[0])
        df_dict['cluster_adjacency_spearman'].append(
            stats.spearmanr(cluster_adjacency[:, 0], cluster_adjacency[:, 1])[0])
        df_dict['cluster_adjacency_kendall'].append(
            stats.kendalltau(cluster_adjacency[:, 0], cluster_adjacency[:, 1])[0])

        for col in cols:
            if col not in ['unification', 'weight', 'dr', 'cluster_size_pearson', 'cluster_size_spearman',
                           'cluster_size_kendall', 'cluster_adjacency_pearson', 'cluster_adjacency_spearman',
                           'cluster_adjacency_kendall']:
                df_dict[col].append(group[col].mean())

    df = pd.DataFrame(df_dict)
    return df


def get_values_for_scatter(method, metric, df_metrics):
    return df_metrics[df_metrics['method'] == method][metric].values, df_metrics[df_metrics['method'] == method][
        metric].mean()


def make_cell(method, methods, metric, invert_colormap, ax, df_metrics):
    values, avg = get_values_for_scatter(method, metric, df_metrics)

    means = []
    for m in methods:
        df_0 = df_metrics[df_metrics['method'] == m]
        means.append(df_0[metric].mean())

    colormap_max = max(means)
    colormap_min = min(means)
    metric_max = max(df_metrics[metric])
    metric_min = min(df_metrics[metric])

    interval = abs(colormap_max - colormap_min)
    if metric in invert_colormap:
        cmap = plt.cm.get_cmap('YlGn')
        norm = matplotlib.colors.Normalize(vmin=colormap_min,
                                           vmax=colormap_max + 0.3 * interval)
    else:
        cmap = plt.cm.get_cmap('YlGn_r')
        norm = matplotlib.colors.Normalize(vmin=colormap_min - 0.3 * interval,
                                           vmax=colormap_max)

    cell_color = cmap(norm(avg))
    ax.set_facecolor(cell_color)

    text = str(avg)[:5]
    if text == "-0.00":
        text = "0.00"

    ax.text(0.5, 0.5, text, ha='center', va='center', fontsize=10,
            transform=ax.transAxes)  # Need to be sure of the center
    ax.set_ylim((-0.0007, 0.01))
    ax.set_xlim((metric_min - 0.3 * interval, metric_max + 0.3 * interval))
    plt.subplots_adjust(left=0.1, right=0.9, top=1.0, bottom=0.0)
    ax.set_yticks([])
    ax.set_xticks([])


def create_table(metrics, methods, invert_colormap, metrics_df, make_cell_func=make_cell):
    widths = []
    heights = []

    for metric in metrics:
        if metric == 'v_spacer':
            widths.append(0.3)
        else:
            widths.append(1.0)
    for method in methods:
        if method.startswith('h_spacer'):
            heights.append(0.1)
        else:
            heights.append(1.0)

    fig, axs = plt.subplots(len(methods), len(metrics), sharex='col', sharey='row', figsize=(5, 2),
                            gridspec_kw={'hspace': 0, 'wspace': 0,
                                         'width_ratios': widths, 'height_ratios': heights}
                            )

    for col_index, metric in enumerate(metrics):
        if metric == 'temp_stress':
            for row_index, method in enumerate(methods):
                ax = axs[row_index, col_index]
                ax.set_visible(False)
        elif metric.startswith('v_spacer'):
            for row_index, method in enumerate(methods):
                ax = axs[row_index, col_index]
                ax.set_visible(False)
        else:
            for row_index, method in enumerate(methods):
                if method == 'h_spacer':
                    ax = axs[row_index, col_index]
                    ax.set_visible(False)
                else:
                    ax = axs[row_index, col_index]

                    if col_index == 0:
                        method_text = method if 'core_core' not in method else method.replace('core_core', 'core')
                        ax.set_ylabel(method_text, rotation=0, ha='right', va='center', labelpad=5, fontsize=14)
                    if row_index == 0:
                        metric_text = metric if metric != 'stress' and metric != 'temp_stress' else metric+'*'
                        ax.set_title(metric_text, rotation=90, ha='center', va='bottom', fontsize=14)

                    make_cell_func(method, [m for m in methods if m != "h_spacer"], metric, invert_colormap, ax, metrics_df)

    fig.subplots_adjust(top=0.6, bottom=0.15, left=0.2, hspace=0.8)

# green table variation 1
def aggregate_over_weight(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)
    #df = metrics_df.drop(columns=['cluster_size', 'cluster_adjacency'])
    #df = average_neighbourhood_metrics(metrics_df)

    df = merge_all_for_table(metrics_df, ['dr', 'unification'])
    df['method'] = df['dr'] + '_' + df['unification']
    df = df.drop(columns=['dr', 'unification'])

    methods = df['method']
    methods = methods.to_list()
    methods.insert(len(methods) // 2, 'h_spacer')
    metrics = df.columns[0:-1].to_list()
    #metrics.insert(len(metrics) // 2, 'v_spacer')
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, df) #potentially send df_metrics instead of df
    plt.show()
    return df

# green table variation 2
def aggregate_over_unification(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)
    #df = metrics_df.drop(columns=['cluster_size', 'cluster_adjacency'])
    #df = average_neighbourhood_metrics(df)

    df = merge_all_for_table(metrics_df, ['dr', 'weight'])
    df['method'] = df['dr'] + '_' + df['weight']
    df = df.drop(columns=['dr', 'weight'])

    df['method'] = pd.Categorical(df['method'],
["NNP_equal", "NNP_073_027", "NNP_082_018", "NNP_091_009", "NNP_core", "TF_equal", "TF_073_027", "TF_082_018", "TF_091_009", "TF_core"]
    )
    df.sort_values("method", inplace=True)

    methods = df['method']
    methods = methods.to_list()
    methods.insert(len(methods) // 2, 'h_spacer')
    metrics = df.columns[0:-1].to_list()
    # metrics.insert(len(metrics) // 2, 'v_spacer')
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, df)
    plt.show()

    return df

# green table variation 3
def aggregate_over_dr(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)
    df = merge_all_for_table(metrics_df, ['unification', 'weight'])

    df['method'] = df['unification'] + '_' + df['weight']
    df = df.drop(columns=['unification', 'weight'])

    df['method'] = pd.Categorical(df['method'],
                                  ["ae_equal", "ae_073_027", "ae_082_018", "ae_091_009", "core_core", "distr_equal",
                                   "distr_073_027", "distr_082_018", "distr_091_009"]
                                  )
    df.sort_values("method", inplace=True)

    methods = df['method']
    methods = methods.to_list()
    methods.insert(len(methods) // 2, 'h_spacer')
    methods.insert(len(methods) // 2 + 1, 'h_spacer')
    metrics = df.columns[0:-1].to_list()
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, df)
    plt.show()

    return df

# green table variation 4
def aggregate_show_dr(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)

    df = merge_all_for_table(metrics_df, ['dr'])
    df['method'] = df['dr']
    df = df.drop(columns=['dr'])

    methods = df['method']
    methods = methods.to_list()
    methods.insert(len(methods) // 2, 'h_spacer')
    metrics = df.columns[0:-1].to_list()
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, df)
    plt.show()


# final green table without aggregation
def table_no_aggregate(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)

    df = merge_all_for_table(metrics_df, ['dr', 'unification', 'weight'])
    df['method'] = df['dr'] + '_' + df['unification'] + '_' + df['weight']
    df = df.drop(columns=['dr', 'unification', 'weight'])

    df['method'] = pd.Categorical(df['method'],
                                  ["NNP_distr_equal", "NNP_distr_073_027", "NNP_distr_082_018", "NNP_distr_091_009", "NNP_ae_equal", "NNP_ae_073_027", "NNP_ae_082_018", "NNP_ae_091_009", "NNP_core_core", "TF_distr_equal", "TF_distr_073_027", "TF_distr_082_018", "TF_distr_091_009", "TF_ae_equal", "TF_ae_073_027", "TF_ae_082_018", "TF_ae_091_009",  "TF_core_core"]
                                  )
    df.sort_values("method", inplace=True)

    methods = df['method']
    methods = methods.to_list()
    methods.insert(len(methods) // 2, 'h_spacer')
    metrics = df.columns[0:-1].to_list()
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, df)
    plt.show()

    return df


def make_difference_cell(method, methods, metric, invert_colormap, ax, df_metrics):
    avg = df_metrics[df_metrics['method'] == method][metric].mean()
    colormap_max = 1
    colormap_min = -1
    metric_max = 1
    metric_min = -1

    interval = abs(colormap_max - colormap_min)
    cmap = plt.cm.get_cmap('seismic_r')
    norm = matplotlib.colors.Normalize(vmin=colormap_min,
                                       vmax=colormap_max)

    cell_color = cmap(norm(avg))
    ax.set_facecolor(cell_color)

    text = str(avg)[:5]
    if text == "-0.00":
        text = "0.00"

    ax.text(0.5, 0.5, text, ha='center', va='center', fontsize=10,
            transform=ax.transAxes)  # Need to be sure of the center
    ax.set_ylim((-0.0007, 0.01))
    ax.set_xlim((metric_min - 0.3 * interval, metric_max + 0.3 * interval))
    plt.subplots_adjust(left=0.1, right=0.9, top=1.0, bottom=0.0)
    ax.set_yticks([])
    ax.set_xticks([])

def difference_table_no_aggregate(static_df, dynamic_df):
    metrics_df = merge_static_dynamic_for_table(static_df, dynamic_df)

    df = merge_all_for_table(metrics_df, ['dr', 'unification', 'weight'])
    df['method'] = df['dr'] + '_' + df['unification'] + '_' + df['weight']
    df = df.drop(columns=['dr', 'unification', 'weight'])

    df['method'] = pd.Categorical(df['method'],
                                  ["NNP_distr_equal", "NNP_distr_073_027", "NNP_distr_082_018", "NNP_distr_091_009",
                                   "NNP_ae_equal", "NNP_ae_073_027", "NNP_ae_082_018", "NNP_ae_091_009", "NNP_core_core",
                                   "TF_distr_equal", "TF_distr_073_027", "TF_distr_082_018", "TF_distr_091_009",
                                   "TF_ae_equal", "TF_ae_073_027", "TF_ae_082_018", "TF_ae_091_009", "TF_core_core"]
                                  )
    df.sort_values("method", inplace=True)

    nnp_df = df[df['method'].str.contains('NNP')]
    tf_df = df[df['method'].str.contains('TF')]
    methods = nnp_df['method'].to_list()
    methods = [method.replace("NNP_", "") for method in methods]

    diff_array = nnp_df.drop(columns=['method']).to_numpy() - tf_df.drop(columns=['method']).to_numpy()
    diff_df = pd.DataFrame(diff_array, columns=nnp_df.columns[:-1])
    diff_df['method'] = methods

    #methods.insert(len(methods) // 2, 'h_spacer')
    metrics = diff_df.columns[0:-1].to_list()
    metrics.insert(4, 'v_spacer')
    metrics.insert(9, 'v_spacer')
    metrics.insert(16, 'v_spacer')
    invert_colormap = ['stress', 'temp_stress']

    create_table(metrics, methods, invert_colormap, diff_df, make_cell_func=make_difference_cell)
    plt.show()

# plot blue and purple table in the big overall table
def plot_corr_and_stress(ax, df):
    metric_ids = list(df.drop(columns=['method']).columns)
    if "temp_stress" in metric_ids:
        metric_ids.remove("temp_stress")
    num_metrics = len(metric_ids)

    matrix = np.zeros((len(df['method'].unique()), num_metrics)) # 4 for spearman, pearson, kendall and stress
    groupby = df.groupby('method')
    index = 0
    for name, group in groupby:
        group = group.drop(columns=['method'])
        if num_metrics == 3:
            group = group.drop(columns=['temp_stress'])
        mean = group.mean()
        matrix[index] = mean
        index += 1

    # Display the first 3 columns (correlations) with colormap in range [0,1]
    m = np.ones_like(matrix)
    for i in range(3):
        m[:, i:i+1] = 0
        masked = np.ma.masked_array(matrix, m)
        colormap = plt.cm.Purples_r
        mat = ax.matshow(masked, cmap=colormap, aspect='auto')
        mat.set_clim(0, 1)

    if num_metrics == 4:
        # Display the last 2 columns (stress) with different colormap
        m = np.ones_like(matrix)
        m[:, 3:] = 0
        masked = np.ma.masked_array(matrix, m)
        colormap = plt.cm.Blues
        mat = ax.matshow(masked, cmap=colormap, aspect='auto')
        mat.set_clim(0, 1)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    ax.set_xticklabels([])
    ax.set_yticklabels([])

# plot the orange table in the big overall table
def plot_k_based(ax, df, metric):
    matrix = np.zeros((len(df['method'].unique()), 20)) # 5 will become 20
    groupby = df.groupby('method')
    index = 0
    for name, group in groupby:
        local_matrix = np.zeros((len(group), 20))

        def parse_list_cell(row):
            row_index = row.name
            x = row[metric]
            x = x[1:-1].split(',')
            x = [float(i) for i in x]
            local_matrix[row_index] = np.array(x)

        group = group.apply(parse_list_cell, axis=1)
        matrix[index:index + len(group)] = local_matrix.mean(axis=0)
        index += 1
    #matrix = df.values
    colormap = plt.cm.magma
    mat = ax.matshow(matrix, cmap=colormap, aspect='auto')
    mat.set_clim(0, 1)
    ax.tick_params(axis=u'both', which=u'both', length=0)
    ax.set_xticklabels([])
    ax.set_yticklabels([])

# plot the green table in the big overall table
def plot_cluster_preservation_and_adjacency(ax, df, metric):
    metric_ids = []
    if metric == "cluster_size":
        metric_ids = ["cs_pearson", "cs_spearman", "cs_kendall"]
    elif metric == "cluster_adjacency":
        metric_ids = ["ca_pearson", "ca_spearman", "ca_kendall"]

    num_metrics = len(metric_ids)
    matrix = np.zeros((len(df['method'].unique()), num_metrics))
    groupby = df.groupby('method')
    index = 0
    for name, group in groupby:
        metric_list = []
        size = group[metric]

        for i, x in enumerate(size):
            metric_list.extend(x)

        metric_list = np.array(metric_list)

        size_pearson = stats.pearsonr(metric_list[:, 0], metric_list[:, 1])[0]
        size_spearman = stats.spearmanr(metric_list[:, 0], metric_list[:, 1])[0]
        size_kendal = stats.kendalltau(metric_list[:, 0], metric_list[:, 1])[0]

        matrix[index] = [size_spearman, size_pearson, size_kendal]
        index += 1

    m = np.ones_like(matrix)
    for i in range(num_metrics):
        m[:, i:i+1] = 0
        masked = np.ma.masked_array(matrix, m)
        colormap = plt.cm.Greens_r
        mat = ax.matshow(masked, cmap=colormap, aspect='auto')
        mat.set_clim(0, 1)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    ax.set_xticklabels([])
    ax.set_yticklabels([])


# the big overall table
def big_summary(df_static, df_dynamic):
    unifications = ['core', 'distr', 'ae']
    fig, axs = plt.subplots(nrows=3, ncols=8, figsize=(12, 11),
                            gridspec_kw={'width_ratios': [1, 1, 1, 1, 1, 1, 1, 1],
                                         'height_ratios': [1, 1, 1],
                                         'wspace': 0.05, 'hspace': 0.05})

    for i, unification in enumerate(unifications):
        df = df_static[df_static['unification'] == unification]
        df['method'] = df['dr'] + '_' + df['weight']
        df = df.drop(columns=['dr', 'weight', 'unification'])
        row_index = i
        col_index = 0

        if unification == 'core':
            df['method'] = pd.Categorical(df['method'],
                                      ["NNP_core", "TF_core"]
                                      )
        else:
            df['method'] = pd.Categorical(df['method'],
                                          ["NNP_equal", "NNP_073_027", "NNP_082_018",
                                           "NNP_091_009", "TF_equal", "TF_073_027",
                                           "TF_082_018", "TF_091_009"]
                                          )
        df.sort_values("method", inplace=True)

        #df = df[~df['method'].isin(['NNP_equal', 'TF_equal'])]

        ax = axs[row_index, col_index + 0]
        df_0 = df[['method', 'spearman', 'pearson', 'kendall', 'stress']]
        #df_0 = df_0.set_index('method')
        #     plot_matrix(ax, df_0, invert_colormap=['spat_stress_n', 'spat_stress_s'])
        num_methods = len(df['method'].unique())
        ax.text(-1, num_methods - (num_methods / 1.5), unification, ha='right', va='center', rotation=90, fontsize=15)
        plot_corr_and_stress(ax, df_0)

        if col_index == 0:
            ax.set_yticks(range(len(df['method'].unique())), minor=False)

        if row_index == 0:
            ax.text(-0.2, -0.6, 'Spearman', fontsize=12, rotation=90)
            ax.text(0.8, -0.6, 'Pearson', fontsize=12, rotation=90)
            ax.text(1.8, -0.6, 'Kendall', fontsize=12, rotation=90)
            ax.text(2.8, -0.6, 'Stress', fontsize=12, rotation=90)

        ax = axs[row_index, col_index + 1]
        df_1 = df[['method', 'trustworthiness']]
        plot_k_based(ax, df_1, 'trustworthiness')
        if row_index == 0:
            ax.set_title('Trustworthiness\nk = [1,...,20]')

        ax = axs[row_index, col_index + 2]
        df_2 = df[['method', 'continuity']]
        plot_k_based(ax, df_2, 'continuity')
        if row_index == 0:
            ax.set_title('Continuity\nk = [1,...,20]')

        ax = axs[row_index, col_index + 3]
        df_3 = df[['method', 'neighbourhood_preservation']]
        plot_k_based(ax, df_3, 'neighbourhood_preservation')
        if row_index == 0:
            ax.set_title('Neighbourhood preservation\nk = [1,...,20]', pad=20.0)

        ax = axs[row_index, col_index + 4]
        df_4 = df[['method', 'neighbourhood_hit']]
        plot_k_based(ax, df_4, 'neighbourhood_hit')
        if row_index == 0:
            ax.set_title('Neighbourhood hit\nk = [0.25,...,5]')

        #cluster preservation and adjacenty
        ax = axs[row_index, col_index + 5]
        df_5 = df[['method', 'cluster_size']]
        plot_cluster_preservation_and_adjacency(ax, df_5, 'cluster_size')
        if row_index == 0:
            ax.text(-0.2, -0.6, 'CS Spearman', fontsize=12, rotation=90)
            ax.text(0.8, -0.6, 'CS Pearson', fontsize=12, rotation=90)
            ax.text(1.8, -0.6, 'CS Kendall', fontsize=12, rotation=90)


        ax = axs[row_index, col_index + 6]
        df_6 = df[['method', 'cluster_adjacency']]
        plot_cluster_preservation_and_adjacency(ax, df_6, 'cluster_adjacency')
        if row_index == 0:
            ax.text(-0.2, -0.6, 'CA Spearman', fontsize=12, rotation=90)
            ax.text(0.8, -0.6, 'CA Pearson', fontsize=12, rotation=90)
            ax.text(1.8, -0.6, 'CA Kendall', fontsize=12, rotation=90)



        ax = axs[row_index, col_index + 7]
        df_dynamic2 = compute_dynamic_from_raw(df_dynamic)
        df = df_dynamic2[df_dynamic2['unification'] == unification]
        df['method'] = df['dr'] + '_' + df['weight']
        df = df.drop(columns=['dr', 'weight', 'unification'])

        if unification == 'core':
            df['method'] = pd.Categorical(df['method'],
                                      ["NNP_core", "TF_core"]
                                      )
        else:
            df['method'] = pd.Categorical(df['method'],
                                          ["NNP_equal", "NNP_073_027", "NNP_082_018",
                                           "NNP_091_009", "TF_equal", "TF_073_027",
                                           "TF_082_018", "TF_091_009"]
                                          )
        df.sort_values("method", inplace=True)

        #df = df[~df['method'].isin(['NNP_equal', 'TF_equal'])]
        df_5 = df[['method', 'temp_spearman', 'temp_pearson', 'temp_kendall', 'temp_stress']]
        plot_corr_and_stress(ax, df_5)

        if row_index == 0:
            ax.text(-0.2, -0.6, 'Temp Spearman', fontsize=12, rotation=90)
            ax.text(0.8, -0.6, 'Temp Pearson', fontsize=12, rotation=90)
            ax.text(1.8, -0.6, 'Temp Kendall', fontsize=12, rotation=90)
            #ax.text(2.8, -0.6, 'Temp Stress', fontsize=12, rotation=90)

        ax.yaxis.tick_right()
        ax.set_yticklabels(df_5['method'])
        ax.set_yticks(range(len(df_5)), minor=False)

    fig.subplots_adjust(top=0.8, bottom=0.1, left=0.05)
    plt.show()

    #for i in range(len(unifications)):
    #    axs[i, 6].set_visible(False)

# next three functions are for generating clustering histograms of different aggregations of scenarios
def cluster_scatterplots_over_weight_square(static_df):
    df = static_df.copy()
    df = df.drop(columns=['name', 'trustworthiness', 'continuity', 'neighbourhood_hit', 'neighbourhood_preservation'])
    df['method'] = df['dr'] + '_' + df['unification']
    df = df.drop(columns=['dr', 'unification'])

    color_map = {wave: plt.cm.get_cmap('tab20')(wave / 20) for wave in range(5)}

    row_index = 0
    col_index = 0
    cols_per_row = 3
    num_cols = cols_per_row * 2 + 1
    num_rows = (len(df['method'].unique()) // cols_per_row) * 3
    height_ratios = [100, 50, 1] * (num_rows // 3)
    width_ratios = [3] * cols_per_row + [1] + [3] * cols_per_row

    cols_per_row += 1
    fig, ax = plt.subplots(nrows=num_rows, ncols=num_cols, figsize=(5, 2 * len(df['method'].unique())), gridspec_kw={'height_ratios': height_ratios, 'width_ratios': width_ratios})

    for name, group in df.groupby('method'):

        method = group['method'].to_list()[0]
        if method == 'core_core':
            method = 'core'

        waves_cluster_size = []
        waves_cluseter_adjacency = []
        cluster_size_list = []
        adjacency_list = []
        size = group['cluster_size']

        for i, x in enumerate(size):
            cluster_size_list.extend(x)
            waves_cluster_size.extend([group['wave'].iloc[i]] * len(x))

        adjacency = group['cluster_adjacency']
        for i, x in enumerate(adjacency):
            adjacency_list.extend(x)
            waves_cluseter_adjacency.extend([group['wave'].iloc[i]] * len(x))

        cluster_size_list = np.array(cluster_size_list)
        adjacency_list = np.array(adjacency_list)
        mean_x_size, max_y_size = (max(cluster_size_list[:,0]) + min(cluster_size_list[:,0])) / 2 ,max(cluster_size_list[:,1])
        mean_x_adj, max_y_adj = (max(adjacency_list[:,0]) + min(adjacency_list[:,0])) / 2, max(adjacency_list[:,1])

        ax[row_index, col_index].set_title(method, fontsize=10)
        ax[row_index, col_index + cols_per_row].set_title(method, fontsize=10)

        if row_index == 0 and col_index == 1:
            ax[row_index, col_index].text(mean_x_size, max_y_size + max_y_size * 0.35, "Cluster size", ha='center', va='bottom', fontsize=15)
            ax[row_index, col_index + cols_per_row].text(mean_x_adj, max_y_adj + max_y_adj * 0.15, "Cluster adjacency", ha='center', va='bottom', fontsize=15)

        colors_cluster_size = [color_map[wave] for wave in waves_cluster_size]
        colors_adjacency = [color_map[wave] for wave in waves_cluseter_adjacency]
        ax[row_index, col_index].scatter(cluster_size_list[:,0], cluster_size_list[:,1], label=name, c=colors_cluster_size, s=8)
        ax[row_index, col_index + cols_per_row].scatter(adjacency_list[:,0], adjacency_list[:,1], label=name, c=colors_adjacency, s=5)

        size_pearson = stats.pearsonr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_spearman = stats.spearmanr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_kendal = stats.kendalltau(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        adjacency_pearson = stats.pearsonr(adjacency_list[:,0], adjacency_list[:,1])[0]
        adjacency_spearman = stats.spearmanr(adjacency_list[:,0], adjacency_list[:,1])[0]
        adjacency_kendal = stats.kendalltau(adjacency_list[:,0], adjacency_list[:,1])[0]

        for col in [0, cols_per_row]:
            ax[row_index + 1, col_index + col].text(0.15, 0.75, 'Pearson', ha='center', va='bottom', fontsize=8)
            ax[row_index + 1, col_index + col].text(0.5, 0.75, 'Spearman', ha='center', va='bottom', fontsize=8)
            ax[row_index + 1, col_index + col].text(0.85, 0.75, 'Kendall', ha='center', va='bottom', fontsize=8)

            ax[row_index + 1, col_index + col].set_xticks([])
            ax[row_index + 1, col_index + col].set_xticklabels([])
            ax[row_index + 1, col_index + col].set_yticks([])
            ax[row_index + 1, col_index + col].set_yticklabels([])
            ax[row_index + 1, col_index + col].set_frame_on(False)

            ax[row_index][col_index + col].set_box_aspect(1)
            ax[row_index][col_index + col].tick_params(axis='both', which='major', labelsize=6)

        table = ax[row_index + 1, col_index].table(
            cellText=[[f'{size_pearson:.2f}', f'{size_spearman:.2f}', f'{size_kendal:.2f}']], loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)
        table = ax[row_index + 1, col_index + cols_per_row].table(
            cellText=[[f'{adjacency_pearson:.2f}', f'{adjacency_spearman:.2f}', f'{adjacency_kendal:.2f}']],
            loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)

        col_index = (col_index + 1) % (cols_per_row - 1)
        row_index = row_index + 3 if col_index == 0 else row_index

    for row in range(num_rows):
        ax[row, 3].set_visible(False)
        if row % 3 == 2:
            for col in range(cols_per_row - 1):
                ax[row, col].set_visible(False)
                ax[row, col + cols_per_row].set_visible(False)

    legend = [Rectangle((0, 0), 1, 1, fc=color_map[wave]) for wave in range(5)]
    ax[num_rows - 2, 4].legend(legend, [f'Wave {wave + 1}' for wave in range(5)], loc='lower center', ncols=5, bbox_to_anchor=(-0.5, -1))

    plt.show()

def cluster_scatterplots_over_unification_square(static_df):
    df = static_df.copy()
    df = df.drop(columns=['name', 'trustworthiness', 'continuity', 'neighbourhood_hit', 'neighbourhood_preservation'])
    df['method'] = df['dr'] + '_' + df['weight']
    df = df.drop(columns=['weight', 'dr', 'unification'])
    df['method'] = pd.Categorical(df['method'],
                                  ["NNP_equal", "NNP_073_027", "NNP_082_018", "NNP_091_009", "NNP_core", "TF_equal",
                                   "TF_073_027", "TF_082_018", "TF_091_009", "TF_core"]
                                  )
    df.sort_values("method", inplace=True)

    color_map = {wave: plt.cm.get_cmap('tab20')(wave / 20) for wave in range(5)}

    row_index = 0
    col_index = 0
    cols_per_row = 5
    num_cols = cols_per_row * 2 + 1
    num_rows = ((len(df['method'].unique()) // cols_per_row)) * 3
    height_ratios = [100, 50, 1] * (num_rows // 3)
    width_ratios = [3] * cols_per_row + [1] + [3] * cols_per_row

    cols_per_row += 1
    fig, ax = plt.subplots(nrows=num_rows, ncols=num_cols, figsize=(5, 2 * len(df['method'].unique())), gridspec_kw={'height_ratios': height_ratios, 'width_ratios': width_ratios})

    for name, group in df.groupby('method'):

        method = group['method'].to_list()[0]
        if method == 'core_core':
            method = 'core'

        waves_cluster_size = []
        waves_cluseter_adjacency = []
        cluster_size_list = []
        adjacency_list = []
        size = group['cluster_size']

        for i, x in enumerate(size):
            cluster_size_list.extend(x)
            waves_cluster_size.extend([group['wave'].iloc[i]] * len(x))

        adjacency = group['cluster_adjacency']
        for i, x in enumerate(adjacency):
            adjacency_list.extend(x)
            waves_cluseter_adjacency.extend([group['wave'].iloc[i]] * len(x))

        cluster_size_list = np.array(cluster_size_list)
        adjacency_list = np.array(adjacency_list)
        mean_x_size, max_y_size = (max(cluster_size_list[:,0]) + min(cluster_size_list[:,0])) / 2 ,max(cluster_size_list[:,1])
        mean_x_adj, max_y_adj = (max(adjacency_list[:,1]) + min(adjacency_list[:,1])) / 2, max(adjacency_list[:,0])

        ax[row_index, col_index].set_title(method, fontsize=10)
        ax[row_index, col_index + cols_per_row].set_title(method, fontsize=10)

        if row_index == 0 and col_index == 2:
            ax[row_index, col_index].text(mean_x_size, max_y_size + max_y_size * 0.5, "Cluster size", ha='center', va='bottom', fontsize=15)
            ax[row_index, col_index + cols_per_row].text(mean_x_adj, max_y_adj + max_y_adj * 0.5, "Cluster adjacency", ha='center', va='bottom', fontsize=15)

        colors_cluster_size = [color_map[wave] for wave in waves_cluster_size]
        colors_adjacency = [color_map[wave] for wave in waves_cluseter_adjacency]
        ax[row_index, col_index].scatter(cluster_size_list[:,0], cluster_size_list[:,1], label=name, c=colors_cluster_size, s=8)
        ax[row_index, col_index + cols_per_row].scatter(adjacency_list[:,1], adjacency_list[:,0], label=name, c=colors_adjacency, s=5)

        size_pearson = stats.pearsonr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_spearman = stats.spearmanr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_kendal = stats.kendalltau(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        adjacency_pearson = stats.pearsonr(adjacency_list[:,0], adjacency_list[:,1])[0]
        adjacency_spearman = stats.spearmanr(adjacency_list[:,0], adjacency_list[:,1])[0]
        adjacency_kendal = stats.kendalltau(adjacency_list[:,0], adjacency_list[:,1])[0]

        for col in [0, cols_per_row]:
            ax[row_index + 1, col_index + col].text(0.15, 0.75, 'Pearson', ha='center', va='bottom', fontsize=6, rotation=45)
            ax[row_index + 1, col_index + col].text(0.5, 0.75, 'Spearman', ha='center', va='bottom', fontsize=6, rotation=45)
            ax[row_index + 1, col_index + col].text(0.85, 0.75, 'Kendall', ha='center', va='bottom', fontsize=6, rotation=45)

            ax[row_index + 1, col_index + col].set_xticks([])
            ax[row_index + 1, col_index + col].set_xticklabels([])
            ax[row_index + 1, col_index + col].set_yticks([])
            ax[row_index + 1, col_index + col].set_yticklabels([])
            ax[row_index + 1, col_index + col].set_frame_on(False)

            ax[row_index][col_index + col].set_box_aspect(1)
            ax[row_index][col_index + col].tick_params(axis='both', which='major', labelsize=6)

        table = ax[row_index + 1, col_index].table(
            cellText=[[f'{size_pearson:.2f}', f'{size_spearman:.2f}', f'{size_kendal:.2f}']], loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)
        table = ax[row_index + 1, col_index + cols_per_row].table(
            cellText=[[f'{adjacency_pearson:.2f}', f'{adjacency_spearman:.2f}', f'{adjacency_kendal:.2f}']],
            loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)

        col_index = (col_index + 1) % (cols_per_row - 1)
        row_index = row_index + 3 if col_index == 0 else row_index

    for row in range(num_rows):
        ax[row, 5].set_visible(False)
        if row % 3 == 2:
            for col in range(cols_per_row - 1):
                ax[row, col].set_visible(False)
                ax[row, col + cols_per_row].set_visible(False)
        #elif row == num_rows - 3 or row == num_rows - 2:
        #    for col in range(2):
        #        ax[row, 2 + col].set_visible(False)
        #        ax[row, 2 + col + cols_per_row].set_visible(False)

    legend = [Rectangle((0, 0), 1, 1, fc=color_map[wave]) for wave in range(5)]
    ax[num_rows - 2, 6].legend(legend, [f'Wave {wave + 1}' for wave in range(5)], loc='lower center', ncols=5, bbox_to_anchor=(-0.5, -0.5))

    plt.show()

def cluster_scatterplots_over_dr_square(static_df):
    df = static_df.copy()
    df = df.drop(columns=['name', 'trustworthiness', 'continuity', 'neighbourhood_hit', 'neighbourhood_preservation'])
    df['method'] = df['unification'] + '_' + df['weight']
    df = df.drop(columns=['weight', 'dr', 'unification'])
    df['method'] = pd.Categorical(df['method'],
                                  ["ae_equal", "ae_073_027", "ae_082_018", "ae_091_009", "core_core", "distr_equal",
                                   "distr_073_027", "distr_082_018", "distr_091_009"]
                                  )
    df.sort_values("method", inplace=True)

    color_map = {wave: plt.cm.get_cmap('tab20')(wave / 20) for wave in range(5)}

    row_index = 0
    col_index = 0
    cols_per_row = 3
    num_cols = cols_per_row * 2 + 1
    num_rows = (len(df['method'].unique()) // cols_per_row) * 3
    height_ratios = [100, 50, 1] * (num_rows // 3)
    width_ratios = [3] * cols_per_row + [1] + [3] * cols_per_row

    cols_per_row += 1
    fig, ax = plt.subplots(nrows=num_rows, ncols=num_cols, figsize=(5, 2 * len(df['method'].unique())), gridspec_kw={'height_ratios': height_ratios, 'width_ratios': width_ratios})

    for name, group in df.groupby('method'):

        method = group['method'].to_list()[0]
        if method == 'core_core':
            method = 'core'

        waves_cluster_size = []
        waves_cluseter_adjacency = []
        cluster_size_list = []
        adjacency_list = []
        size = group['cluster_size']

        for i, x in enumerate(size):
            cluster_size_list.extend(x)
            waves_cluster_size.extend([group['wave'].iloc[i]] * len(x))

        adjacency = group['cluster_adjacency']
        for i, x in enumerate(adjacency):
            adjacency_list.extend(x)
            waves_cluseter_adjacency.extend([group['wave'].iloc[i]] * len(x))

        cluster_size_list = np.array(cluster_size_list)
        adjacency_list = np.array(adjacency_list)
        mean_x_size, max_y_size = (max(cluster_size_list[:,0]) + min(cluster_size_list[:,0])) / 2 ,max(cluster_size_list[:,1])
        mean_x_adj, max_y_adj = (max(adjacency_list[:,1]) + min(adjacency_list[:,1])) / 2, max(adjacency_list[:,0])

        ax[row_index, col_index].set_title(method, fontsize=10)
        ax[row_index, col_index + cols_per_row].set_title(method, fontsize=10)

        if row_index == 0 and col_index == 1:
            ax[row_index, col_index].text(mean_x_size, max_y_size + max_y_size * 0.5, "Cluster size", ha='center', va='bottom', fontsize=15)
            ax[row_index, col_index + cols_per_row].text(mean_x_adj, max_y_adj + max_y_adj * 0.5, "Cluster adjacency", ha='center', va='bottom', fontsize=15)

        colors_cluster_size = [color_map[wave] for wave in waves_cluster_size]
        colors_adjacency = [color_map[wave] for wave in waves_cluseter_adjacency]
        ax[row_index, col_index].scatter(cluster_size_list[:,0], cluster_size_list[:,1], label=name, c=colors_cluster_size, s=8)
        ax[row_index, col_index + cols_per_row].scatter(adjacency_list[:,1], adjacency_list[:,0], label=name, c=colors_adjacency, s=5)

        size_pearson = stats.pearsonr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_spearman = stats.spearmanr(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        size_kendal = stats.kendalltau(cluster_size_list[:,0], cluster_size_list[:,1])[0]
        adjacency_pearson = stats.pearsonr(adjacency_list[:,1], adjacency_list[:,0])[0]
        adjacency_spearman = stats.spearmanr(adjacency_list[:,1], adjacency_list[:,0])[0]
        adjacency_kendal = stats.kendalltau(adjacency_list[:,1], adjacency_list[:,0])[0]

        for col in [0, cols_per_row]:
            ax[row_index + 1, col_index + col].text(0.15, 0.75, 'Pearson', ha='center', va='bottom', fontsize=8)
            ax[row_index + 1, col_index + col].text(0.5, 0.75, 'Spearman', ha='center', va='bottom', fontsize=8)
            ax[row_index + 1, col_index + col].text(0.85, 0.75, 'Kendall', ha='center', va='bottom', fontsize=8)

            ax[row_index + 1, col_index + col].set_xticks([])
            ax[row_index + 1, col_index + col].set_xticklabels([])
            ax[row_index + 1, col_index + col].set_yticks([])
            ax[row_index + 1, col_index + col].set_yticklabels([])
            ax[row_index + 1, col_index + col].set_frame_on(False)

            ax[row_index][col_index + col].set_box_aspect(1)
            ax[row_index][col_index + col].tick_params(axis='both', which='major', labelsize=6)

        table = ax[row_index + 1, col_index].table(
            cellText=[[f'{size_pearson:.2f}', f'{size_spearman:.2f}', f'{size_kendal:.2f}']], loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)
        table = ax[row_index + 1, col_index + cols_per_row].table(
            cellText=[[f'{adjacency_pearson:.2f}', f'{adjacency_spearman:.2f}', f'{adjacency_kendal:.2f}']],
            loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(8)
        table.scale(1, 4)

        col_index = (col_index + 1) % (cols_per_row - 1)
        row_index = row_index + 3 if col_index == 0 else row_index

    for row in range(num_rows):
        ax[row, 3].set_visible(False)
        if row % 3 == 2:
            for col in range(cols_per_row - 1):
                ax[row, col].set_visible(False)
                ax[row, col + cols_per_row].set_visible(False)

    legend = [Rectangle((0, 0), 1, 1, fc=color_map[wave]) for wave in range(5)]
    ax[num_rows - 2, 4].legend(legend, [f'Wave {wave + 1}' for wave in range(5)], loc='lower center', ncols=5, bbox_to_anchor=(-0.5, -1))

    plt.show()


def temporal_scatterplots(dynamic_df):
    dr = "TF"  # TF | NNP | *
    unification = "*"  # distr | ae | core | *
    weight = "*"  # equal | 073_027 | 082_018 | 091_009 | core | *
    high, low = [], []

    df = dynamic_df.copy()

    if dr != "*":
        df = df[df['dr'] == dr]
    if unification != "*":
        df = df[df['unification'] == unification]
    if weight != "*":
        df = df[df['weight'] == weight]

    groupby = df.groupby(['dr', 'unification', 'weight'])
    for name, group in groupby:
        mahalanobis_high = []
        mahalanobis_low = []
        cosine_high = []
        cosine_low = []

        for i in range(len(group)):
            mahalanobis_high.append(group['mahalanobis_dist_high'][i])
            mahalanobis_low.append(group['mahalanobis_dist_low'][i])
            cosine_high.append(group['cosine_sim_high'][i])
            cosine_low.append(group['cosine_sim_low'][i])

        for i in range(len(mahalanobis_high)):
            high_val = 0.5 * (1 / (1 + mahalanobis_high[i])) + 0.5 * cosine_high[i]
            low_val = 0.5 * (1 / (1 + mahalanobis_low[i])) + 0.5 * cosine_low[i]
            high.append(high_val)
            low.append(low_val)

    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    ax.set_box_aspect(1)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.scatter(high, low, c="red")
    ax.set_xlabel("Similarity in high dimensions")
    ax.set_ylabel("Similarity in low dimensions")
    plt.show()

def time_efficiency():
    #NNP times recorded previously
    nnp_dr = [75.09522533416748, 67.33199906349182, 68.58274936676025]
    nnp_train = [131.9035987854004, 94.45246171951294, 131.15243196487427]
    nnp_predict = [8.291822671890259, 9.347064971923828, 10.274733066558838]

    #TF times times recorded previously
    wave_1 = [37.81500315666199, 39.45599985122681, 37.878604888916016]
    wave_2 = [81.39157152175903, 73.78100490570068, 70.09500098228455]
    wave_3 = [103.04268097877502, 82.76799941062927, 76.92210340499878]
    wave_4 = [128.8400158882141, 120.28399848937988, 118.21907806396484]
    wave_5 = [129.1852672100067, 111.10858368873596, 109.86452889442444]

    mean_nnp_dr = np.mean(nnp_dr)
    mean_nnp_train = np.mean(nnp_train)
    mean_nnp_predict = np.mean(nnp_predict)
    mean_wave_1 = np.mean(wave_1)
    mean_wave_2 = np.mean(wave_2)
    mean_wave_3 = np.mean(wave_3)
    mean_wave_4 = np.mean(wave_4)
    mean_wave_5 = np.mean(wave_5)
    print(mean_nnp_dr + mean_nnp_train + mean_nnp_predict)
    print(mean_wave_1 + mean_wave_2 + mean_wave_3 + mean_wave_4 + mean_wave_5)

    nnp_color_map = {
        "dr": plt.cm.get_cmap('tab20b')(13/20),
        "train": plt.cm.get_cmap('tab20b')(14/20),
        "predict": plt.cm.get_cmap('tab20b')(15/20)
    }
    wave_color_map = {wave: plt.cm.get_cmap('Pastel1')((wave + 1) / 9) for wave in range(5)}

    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    ax.bar(0, height=mean_nnp_dr, color=nnp_color_map["dr"])
    ax.bar(0, height=mean_nnp_train, bottom=mean_nnp_dr, color=nnp_color_map["train"])
    ax.bar(0, height=mean_nnp_predict, bottom=mean_nnp_dr + mean_nnp_train, color=nnp_color_map["predict"])
    ax.bar(1, height=mean_wave_1, color=wave_color_map[0])
    ax.bar(1, height=mean_wave_2, color=wave_color_map[1], bottom=mean_wave_1)
    ax.bar(1, height=mean_wave_3, color=wave_color_map[2], bottom=mean_wave_1 + mean_wave_2)
    ax.bar(1, height=mean_wave_4, color=wave_color_map[3], bottom=mean_wave_1 + mean_wave_2 + mean_wave_3)
    ax.bar(1, height=mean_wave_5, color=wave_color_map[4], bottom=mean_wave_1 + mean_wave_2 + mean_wave_3 + mean_wave_4)
    ax.set_xticks([0, 1])
    ax.set_xticklabels(["NNP", "TF"])
    ax.set_ylabel("Time (s)")
    #ax.set_xticklabels(["","NNP", "Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"])
    plt.legend(["NNP DR", "NNP Train", "NNP Predict", "Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"])
    plt.show()

def make_colorbars():
    fig, ax = plt.subplots(1, 1)
    def diff_formatter(x, _):
        return "TF" if x == -1 else "NNP" if x == 1 else "equal"
    # YlGn_r, seismic_r
    colorbars = {
        "green": {
            "cmap": plt.cm.get_cmap('YlGn_r'),
            "norm": matplotlib.colors.Normalize(vmin=0, vmax=1),
            "ticks": [0, 1],
            "tick_formatter": matplotlib.ticker.FuncFormatter(lambda x, _: "good" if x == 1 else "poor")
        },
        "diff": {
            "cmap": plt.cm.get_cmap('seismic_r'),
            "norm": matplotlib.colors.Normalize(vmin=-1, vmax=1),
            "ticks": [-1, 0, 1],
            "tick_formatter": matplotlib.ticker.FuncFormatter(diff_formatter)
        }
    }

    bar = colorbars["diff"]
    colorbar = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=bar["cmap"], norm=bar["norm"]), ax=ax, aspect=20, fraction=0.05, ticks=bar["ticks"], format=bar["tick_formatter"], orientation='horizontal')
    plt.show()

if __name__ == '__main__':
    all_static_df = collect_static_data()
    all_dynamic_df = collect_dynamic_data()
    #make_colorbars()
    #big_summary(all_static_df, all_dynamic_df)
    #df_table_1 = aggregate_over_weight(all_static_df, all_dynamic_df)
    #df_table_2 = aggregate_over_unification(all_static_df, all_dynamic_df)
    #df_table_3 = aggregate_over_dr(all_static_df, all_dynamic_df)
    #df_table_4 = table_no_aggregate(all_static_df, all_dynamic_df)
    #df_table_5 = aggregate_show_dr(all_static_df, all_dynamic_df)
    #difference_table_no_aggregate(all_static_df, all_dynamic_df)

    #cluster_scatterplots_over_weight_square(all_static_df)
    #cluster_scatterplots_over_unification_square(all_static_df)
    #cluster_scatterplots_over_dr_square(all_static_df)
    #normal_tsne()

    #temporal_scatterplots(all_dynamic_df)
    #time_efficiency()
