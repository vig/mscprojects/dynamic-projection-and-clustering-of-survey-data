##
## Main file for experimenting and creating some figures
##

import copy

import pandas as pd
import time
pd.options.mode.chained_assignment = None  # default='warn'
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from scipy.stats import norm, shapiro
import os
from eval import *
from clusters import *
from nnp import nnp
from datasets import DynamicDataset
from unify_dimensions import *

def prepare_EVS():
    DATASET_PATH = os.path.join(os.path.dirname(__file__), '..', 'dataset', 'ZA7503_v3-0-0_TREND_FILE.dta')
    df = pd.read_stata(DATASET_PATH, convert_categoricals=False)
    print(df.shape)

    # Only keep records with countries that appeared in every wave - reduces the dataset size to 55k samples
    # df = df[df[country_col].isin(allowed_countries)]
    # print(df.shape)

    print(df[wave_col].unique())

    print(len(df[df[wave_col] == 1]))
    print(len(df[df[wave_col] == 2]))
    print(len(df[df[wave_col] == 3]))
    print(len(df[df[wave_col] == 4]))
    print(len(df[df[wave_col] == 5]))

    # Only keep records with valid values for trust questions
    df = df[~df[trust_questions_cols].isin([-5, -3, -2, -1]).any(axis=1)]
    print(df.shape)

    df = df[[wave_col, country_col] + trust_questions_cols]
    print(df.shape)

    # Remove Estonia, Latvia and Lithuania from wave 2, because they did not include some questions in the survey
    df = df[~(df[country_col].isin(["EE", "LT", "LV"]) & df[wave_col].isin([2]))]

    # Reduce the dataset due to computational constraints by a set percentage
    #percent_keep = 0.10
    #for wave in df[wave_col].unique():
    #    df_wave = df[df[wave_col] == wave]
    #    df_sampled = df_wave.sample(frac=percent_keep, random_state=1)
    #    df = df[~df[wave_col].isin([wave])]
    #    df = df._append(df_sampled)
    #df.reset_index(drop=True, inplace=True)

    # check if there are any missing values
    all_counts = {val: 0 for val in trust_questions_values}
    for col in trust_questions_cols:
        counts = df[col].value_counts()
        for val, count in counts.items():
            all_counts[val] += count
    print(all_counts)

    # replace negative values with None
    mask = df[trust_questions_cols] < 0
    df[mask] = None

    # normalize trust question columns between 0 and 1
    def normalize(series):
        s = series[series.notnull()]
        return (s - s.min()) / (s.max() - s.min())

    df[trust_questions_cols] = df[trust_questions_cols].apply(lambda x: normalize(x))

    # remove trust_question_cols that are not in core_cols
    # df = df[core_cols + [wave_col, country_col]]

    print(len(df[df[wave_col] == 1]))
    print(len(df[df[wave_col] == 2]))
    print(len(df[df[wave_col] == 3]))
    print(len(df[df[wave_col] == 4]))
    print(len(df[df[wave_col] == 5]))

    df_in = df.drop(country_col, axis=1)
    dataset = DynamicDataset(df_in, core_cols)
    dataset.init_waves(wave_col)

    return dataset

def prepare_PPP():
    WAVE_1_PATH = os.path.join(os.path.dirname(__file__), '..', 'dataset', 'PPP', 'cv08a_1.1p_EN.dta')
    WAVE_2_PATH = os.path.join(os.path.dirname(__file__), '..', 'dataset', 'PPP', 'cv09b_2.1p_EN.dta')
    WAVE_3_PATH = os.path.join(os.path.dirname(__file__), '..', 'dataset', 'PPP', 'cv10c_EN_1.0p.dta')

    wave1_df = pd.read_stata(WAVE_1_PATH, convert_categoricals=False)
    wave2_df = pd.read_stata(WAVE_2_PATH, convert_categoricals=False)
    wave3_df = pd.read_stata(WAVE_3_PATH, convert_categoricals=False)

    print(wave1_df.shape)
    print(wave2_df.shape)
    print(wave3_df.shape)


def elbow_plots(df):
    distortions = []
    distortions_whole = []

    ks = range(1, 10)
    for k in ks:
        print("k = {}".format(k))
        dataset = get_clusters_for_waves(df, k=k, columns="all")
        for i in range(dataset.num_waves):
            if k == 1:
                distortions.append([dataset[i].cluster_distortions])
            else:
                distortions[i].append(dataset[i].cluster_distortions)

        result = get_clusters_for_waves_partition(df.df, k=k, columns="all")
        distortions_whole.append(result["distortion"])

    for i in range(dataset.num_waves):
        plt.plot(ks, distortions[i], 'x-', lw=2, label=f"Wave {i+1}")
    plt.plot(ks, distortions_whole, 'x-', lw=2, label="Whole dataset", color="black")

    plt.xlabel("k")
    #plt.yscale("log")
    plt.ylabel("Normalized Distortion")
    #plt.title("Elbow Method - TF-kMeans")
    plt.legend()
    plt.show()

# functions for color manipulation
def cluster_color_bar(answerId, color):
    # Calculate darker color
    ratio = 0.3
    r_dark = math.ceil(int(color[1:3], 16) * ratio)
    g_dark = math.ceil(int(color[3:5], 16) * ratio)
    b_dark = math.ceil(int(color[5:7], 16) * ratio)
    darker = '#%02x%02x%02x' % (r_dark, g_dark, b_dark)

    interpolated_color = interpolate_color(color, darker, (3 - (answerId - 1)) / 3)
    return interpolated_color

def interpolate_color(light_color, dark_color, percentage):
    # Validate the input percentage
    percentage = max(0, min(1, percentage))

    # Convert hex strings to RGB tuples
    light_rgb = tuple(int(light_color[i:i+2], 16) for i in (1, 3, 5))
    dark_rgb = tuple(int(dark_color[i:i+2], 16) for i in (1, 3, 5))

    # Interpolate between light and dark colors
    interpolated_rgb = (
        int(light_rgb[0] + percentage * (dark_rgb[0] - light_rgb[0])),
        int(light_rgb[1] + percentage * (dark_rgb[1] - light_rgb[1])),
        int(light_rgb[2] + percentage * (dark_rgb[2] - light_rgb[2]))
    )

    # Convert interpolated RGB tuple to hex color
    interpolated_hex = "#{:02X}{:02X}{:02X}".format(*interpolated_rgb)

    return interpolated_hex

# create the cluster-wave diagram (grid of barcharts)
def cluster_question_stats(dataset, draw="bar", match=None, suptitle="", transform=False):
    if match is not None:
        dataset = match_clusters(dataset, method=match, transform=transform)

    max_num_clusters = max([len(np.unique(wave.cluster_labels)) for wave in dataset.waves])
    # matrix of counts with these dimensions:
    # wave index, cluster index, question index, answer index
    count_matrix = np.zeros((dataset.num_waves, max_num_clusters, len(dataset.all_cols), 4))
    sum_matrix = np.zeros((dataset.num_waves, max_num_clusters, len(dataset.all_cols), 4))

    for wave_index, wave in enumerate(dataset.waves):
        for cluster_index in range(len(wave.cluster_centers)):
            cluster = wave.df
            bool_arr = [True if label == cluster_index else False for label in wave.cluster_labels]
            cluster = cluster.loc[bool_arr]
            common_cols = [(i, col) for i, col in enumerate(trust_questions_cols) if col in cluster.columns]
            cols = [col for i, col in common_cols]
            if transform:
                cluster.loc[:, cols] = cluster[cols].replace(1, 4)
                cluster.loc[:, cols] = cluster[cols].replace(3 / 4, 3)
                cluster.loc[:, cols] = cluster[cols].replace(1 / 4, 2)
                cluster.loc[:, cols] = cluster[cols].replace(0, 1)
            else:
                cluster.loc[:, cols] = cluster[cols].replace(1, 4)
                cluster.loc[:, cols] = cluster[cols].replace(2 / 3, 3)
                cluster.loc[:, cols] = cluster[cols].replace(1 / 3, 2)
                cluster.loc[:, cols] = cluster[cols].replace(0, 1)
                #cluster.loc[:, cols] = cluster[cols].apply(lambda x: ((x - x.min()) / (x.max() - x.min())) * (4 - 1) + 1, axis=0)

            cluster.loc[:,cols] = cluster[cols].astype(int)
            for question_index, question in common_cols:
                for answer_index, answer in enumerate(allowed_values):
                    try:
                        count_matrix[wave_index, cluster_index, question_index, answer_index] = cluster[question].value_counts()[answer]
                        sum_matrix[wave_index, cluster_index, answer_index] += cluster[question].value_counts()[answer]
                    except KeyError:
                        count_matrix[wave_index, cluster_index, question_index, answer_index] = 0

    #closets_pairs_wave4_wave5_emd = answer_sum_emd(sum_matrix[3, :, :], sum_matrix[4, :, :])


    if draw == "line":
        # this option is not up to date after refactoring
        fig, axs = plt.subplots(len(clusterings), len(clusterings["wave1"]["centers"]))
        for wave_index in range(len(clusterings)):
            for cluster_index in range(len(clusterings["wave1"]["centers"])):
                for question_index, color in zip(range(len(trust_questions_cols)), ["b", "g", "r", "c", "m", "y", "k"]):
                    axs[wave_index, cluster_index].plot(allowed_values, count_matrix[wave_index, cluster_index, question_index], color=color, label=trust_questions_labels[question_index])
                #axs[wave_index, cluster_index].plot(allowed_values, sum_matrix[wave_index, cluster_index] / len(trust_questions_values), color="grey", ls="--", label="Avg")
                axs[wave_index, cluster_index].text(1, np.max(count_matrix[wave_index, cluster_index]) * 0.85, str(int(np.sum(count_matrix[wave_index, cluster_index]) / 7)), fontsize=10, fontweight="bold")
            axs[wave_index, len(clusterings["wave1"]["centers"]) - 1].text(4.5, np.max(count_matrix[wave_index, cluster_index]) * 0.85, str(int(np.sum(count_matrix[wave_index]) / 7)), fontsize=10, fontweight="bold")
        for cluster_index in range(len(clusterings["wave1"]["centers"])):
            axs[4, cluster_index].text(1, -np.max(count_matrix[4, cluster_index]) * 0.5, str(int(np.sum(count_matrix[:, cluster_index]) / 7)), fontsize=10, fontweight="bold")
        axs[4, len(clusterings["wave1"]["centers"]) - 1].text(4.5, -np.max(count_matrix[4, cluster_index]) * 0.5, str(int(np.sum(count_matrix) / 7)), fontsize=10, fontweight="bold")

        for ax, col in zip(axs[0], ["Cluster {}".format(i) for i in range(len(clusterings["wave1"]["centers"]))]):
            ax.set_title(col)
        for ax, row in zip(axs[:,0], ["Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"]):
            ax.set_ylabel(row, rotation=90, size='large')
        plt.legend(loc='upper right', bbox_to_anchor=(0, -0.45), ncol=8)
        fig.suptitle("Cluster stats with propagated centroids", fontsize=16)
        #fig.suptitle("Cluster stats of clustering whole dataset and partitioning by wave", fontsize=16)
        plt.show()

    elif draw == "bar":
        fig, axs = plt.subplots(dataset.num_waves, max_num_clusters, gridspec_kw={'wspace': 0.1, 'hspace': 0.1})
        bars = []
        labels = ["A great deal", "Quite a lot", "Not very much", "None at all"]
        for wave_index, wave in enumerate(dataset.waves):
            for cluster_index in range(len(wave.cluster_centers)):
                #q_label_indexes = [i for i, col in enumerate(trust_questions_cols) if col in wave["df"].columns]
                q_label_indexes = [i for i, col in enumerate(trust_questions_cols)]
                q_labels = [trust_questions_labels[i] for i in q_label_indexes]
                bottom = np.zeros(len(q_labels))
                for answer_index in range(len(allowed_values)):
                    #color = cluster_color_bar(answer_index + 1, cluster_colors[cluster_index])
                    color = ["b", "g", "r", "c"][answer_index]
                    values = count_matrix[wave_index, cluster_index, :, answer_index]
                    bar = axs[wave_index, cluster_index].bar(q_labels, count_matrix[wave_index, cluster_index, :, answer_index][q_label_indexes], color=color, bottom=bottom)
                    axs[wave_index, cluster_index].xaxis.set_ticks([])
                    bottom += count_matrix[wave_index, cluster_index, :, answer_index][q_label_indexes]
                    bars.append(bar)
                for i, index in enumerate(q_label_indexes):
                    if np.sum(count_matrix[wave_index, cluster_index, index, :]) == 0:
                        continue
                    q_label = trust_questions_labels[index]
                    axs[wave_index, cluster_index].text(i - 0.25, np.sum(count_matrix[wave_index, cluster_index, index, :]) * 0.1, q_label, fontsize=8, rotation=90)
                axs[wave_index, cluster_index].set_yticks([])

        for ax, col in zip(axs[0], ["Cluster {}".format(i) for i in range(max_num_clusters)]):
            ax.set_title(col)
        for ax, row in zip(axs[:,0], ["Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"]):
            ax.set_ylabel(row, rotation=90, size='large')
        plt.legend(bars, labels, loc='upper right', bbox_to_anchor=(-0.7, 0), ncol=8)
        #plt.rcParams['font.size'] = 0.5
        #fig.suptitle("Cluster stats with propagated centroids", fontsize=16)
        fig.suptitle(suptitle, fontsize=16)
        fig.set_size_inches((15, 8.5), forward=False)
        plt.show()
        #plt.savefig(suptitle + ".png", bbox_inches='tight', dpi=200)
        print("show")

# project the dynamic dataset as a per-timeframe projection
def project_df(dataset, match=None):
    if match is not None:
        dataset = match_clusters(dataset, method=match)

    projections = []
    times = []
    for wave in dataset.waves:
        time_start = time.time()
        projection = dr(wave.df)
        time_end = time.time()
        projections.append(projection)
        times.append(time_end - time_start)

    for i, _time in enumerate(times):
        print("Wave " + str(i+1) + ": " + str(_time))

    draw_projections(dataset, projections, "Random Suptitle", False)

    indexes = []
    for wave in dataset.waves:
        indexes.append([i for i in range(len(wave.df))])

    print("computing distances and similarities")
    mahalanobis_dists = []
    mahalanobis_dists_norm = []
    cosine_sims = []
    combined = []
    for i in range(0, dataset.num_waves - 1):
        wave_a = dataset.waves[i]
        wave_b = dataset.waves[i + 1]
        labels_a = wave_a.cluster_labels
        labels_b = wave_b.cluster_labels
        projection_a = projections[i]
        projection_b = projections[i + 1]
        for cluster in range(6):
            cluster_a = [projection_a[i] for i in indexes[i] if labels_a[i] == cluster]
            cluster_b = [projection_b[i] for i in indexes[i + 1] if labels_b[i] == cluster]
            mah = metric_cluster_movement(cluster_a, cluster_b)
            mah_norm = 1 / (1 + mah)
            mahalanobis_dists.append(mah)
            mahalanobis_dists_norm.append(mah_norm)
            cos_sim = metric_cosine_sim(cluster_a, cluster_b)
            if cos_sim == 0:
                print("cosine sim is 0 between wave {} cluster {} and wave {} cluster {}, color {}".format(i, cluster, i + 1, cluster, cluster_colors[cluster]))
            cosine_sims.append(cos_sim)
            combined.append((mah_norm * 0.5 + cos_sim * 0.5))

    fix, (ax1, ax2, ax3, ax4) = plt.subplots(4)
    ax1.scatter(mahalanobis_dists, [1 for _ in range(len(mahalanobis_dists))], label="Mahalanobis Distance", marker="o", c="red")
    ax2.scatter(mahalanobis_dists_norm, [1 for _ in range(len(mahalanobis_dists_norm))], label="Normalized Mahalanobis Distance", marker="o", c="green")
    ax3.scatter(cosine_sims, [1 for _ in range(len(cosine_sims))], label="Cosine Similarity", marker="o", c="blue")
    ax4.scatter(combined, [1 for _ in range(len(combined))], label="Combined", marker="o", c="black")
    ax1.set_yticks([1])
    ax2.set_yticks([1])
    ax3.set_yticks([1])
    ax4.set_yticks([1])
    ax1.set_xlabel("Distance")
    ax2.set_xlabel("Similarity")
    ax3.set_xlabel("Similarity")
    ax4.set_xlabel("Combined Similarity")
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    print("max mahalanobis:", max(mahalanobis_dists), "min mahalanobis:", min(mahalanobis_dists), "avg mahalanobis:", sum(mahalanobis_dists) / len(mahalanobis_dists))
    print("max norm mahalanobis:", max(mahalanobis_dists_norm), "min norm mahalanobis:", min(mahalanobis_dists_norm), "avg norm mahalanobis:", sum(mahalanobis_dists_norm) / len(mahalanobis_dists_norm))
    print("max cosine:", max(cosine_sims), "min cosine:", min(cosine_sims), "avg cosine:", sum(cosine_sims) / len(cosine_sims))
    print("max combined:", max(combined), "min combined:", min(combined), "avg combined:", sum(combined) / len(combined))
    plt.show()


def neural_projection(dataset, train_cols="core", match=None):
    if match is not None:
        dataset = match_clusters(dataset, method=match)

    nnp_df, _, _ = nnp(dataset, train_cols)

    projections = []
    for i, wave in enumerate(dataset.waves):
        projection = nnp_df[nnp_df[wave_col] == i + 1][["x", "y"]].to_numpy()
        projections.append(projection)

    indexes = []
    for wave in dataset.waves:
        indexes.append([i for i in range(len(wave.df))])

    print("computing distances and similarities")
    mahalanobis_dists = []
    cosine_sims = []
    for i in range(0, dataset.num_waves - 1):
        wave_a = dataset.waves[i]
        wave_b = dataset.waves[i + 1]
        labels_a = wave_a.cluster_labels
        labels_b = wave_b.cluster_labels
        projection_a = projections[i]
        projection_b = projections[i + 1]
        for cluster in range(6):
            cluster_a = [projection_a[i] for i in indexes[i] if labels_a[i] == cluster]
            cluster_b = [projection_b[i] for i in indexes[i + 1] if labels_b[i] == cluster]
            mahalanobis_dists.append(metric_cluster_movement(cluster_a, cluster_b))
            cos_sim = metric_cosine_sim(cluster_a, cluster_b)
            if cos_sim == 0:
                print("cosine sim is 0 between wave {} cluster {} and wave {} cluster {}", i, cluster, i + 1, cluster)
            cosine_sims.append(cos_sim)

    fig, axs = plt.subplots(1, 5)
    for cluster, color in zip(range(6), cluster_colors):
        axs[0].scatter(projections[0][[i for i in indexes[0] if dataset.waves[0].cluster_labels[i] == cluster], 0],
                       projections[0][[i for i in indexes[0] if dataset.waves[0].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[1].scatter(projections[1][[i for i in indexes[1] if dataset.waves[1].cluster_labels[i] == cluster], 0],
                       projections[1][[i for i in indexes[1] if dataset.waves[1].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[2].scatter(projections[2][[i for i in indexes[2] if dataset.waves[2].cluster_labels[i] == cluster], 0],
                       projections[2][[i for i in indexes[2] if dataset.waves[2].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[3].scatter(projections[3][[i for i in indexes[3] if dataset.waves[3].cluster_labels[i] == cluster], 0],
                       projections[3][[i for i in indexes[3] if dataset.waves[3].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[4].scatter(projections[4][[i for i in indexes[4] if dataset.waves[4].cluster_labels[i] == cluster], 0],
                       projections[4][[i for i in indexes[4] if dataset.waves[4].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)

        axs[0].set(adjustable='box', aspect='equal')
        axs[1].set(adjustable='box', aspect='equal')
        axs[2].set(adjustable='box', aspect='equal')
        axs[3].set(adjustable='box', aspect='equal')
        axs[4].set(adjustable='box', aspect='equal')
    for ax, row in zip(axs, ["Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"]):
        ax.set_xlabel(row, size='large')
    plt.legend(loc='upper right', bbox_to_anchor=(0, -0.3), ncol=8)
    # fig.suptitle("DR with propagated centroids", fontsize=16)
    fig.suptitle(
        "NNP with core dimensions, matched with Pearson correlation",
        fontsize=16)
    plt.show()

    fix, (ax1, ax2) = plt.subplots(2)
    ax1.scatter(mahalanobis_dists, [1 for _ in range(len(mahalanobis_dists))], label="Mahalanobis Distance", marker="o",
                c="red")
    ax2.scatter(cosine_sims, [1 for _ in range(len(cosine_sims))], label="Cosine Similarity", marker="o", c="blue")
    ax1.set_yticks([1])
    ax2.set_yticks([1])
    ax1.set_xlabel("Distance")
    ax2.set_xlabel("Similarity")
    ax1.legend()
    ax2.legend()
    print("max mahalanobis:", max(mahalanobis_dists), "min mahalanobis:", min(mahalanobis_dists), "avg mahalanobis:",
          sum(mahalanobis_dists) / len(mahalanobis_dists))
    print("max cosine:", max(cosine_sims), "min cosine:", min(cosine_sims), "avg cosine:",
          sum(cosine_sims) / len(cosine_sims))
    plt.show()

def draw_projections(dataset, projections, suptitle="", save=False):
    indexes = []
    for wave in dataset.waves:
        indexes.append([i for i in range(len(wave.df))])

    px = 1 / plt.rcParams['figure.dpi']  # pixel in inches
    fig, axs = plt.subplots(1, 5, figsize=(1080 * px, 720 * px))
    for cluster, color in zip(range(6), cluster_colors):
        axs[0].scatter(projections[0][[i for i in indexes[0] if dataset.waves[0].cluster_labels[i] == cluster], 0],
                       projections[0][[i for i in indexes[0] if dataset.waves[0].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[1].scatter(projections[1][[i for i in indexes[1] if dataset.waves[1].cluster_labels[i] == cluster], 0],
                       projections[1][[i for i in indexes[1] if dataset.waves[1].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[2].scatter(projections[2][[i for i in indexes[2] if dataset.waves[2].cluster_labels[i] == cluster], 0],
                       projections[2][[i for i in indexes[2] if dataset.waves[2].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[3].scatter(projections[3][[i for i in indexes[3] if dataset.waves[3].cluster_labels[i] == cluster], 0],
                       projections[3][[i for i in indexes[3] if dataset.waves[3].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)
        axs[4].scatter(projections[4][[i for i in indexes[4] if dataset.waves[4].cluster_labels[i] == cluster], 0],
                       projections[4][[i for i in indexes[4] if dataset.waves[4].cluster_labels[i] == cluster], 1],
                       c=color, label="Cluster {}".format(cluster), s=1)

        axs[0].set(adjustable='box', aspect='equal')
        axs[1].set(adjustable='box', aspect='equal')
        axs[2].set(adjustable='box', aspect='equal')
        axs[3].set(adjustable='box', aspect='equal')
        axs[4].set(adjustable='box', aspect='equal')

    for ax, row in zip(axs, ["Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"]):
        ax.set_xlabel(row, size='large')
    # remove figure ticks
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])

    legend = []
    labels = []
    for cluster, color in zip(range(6), cluster_colors):
        legend.append(Line2D([0], [0], marker='o', color="white", markerfacecolor=color, markersize=5))
        labels.append("Cluster {}".format(cluster))
    plt.legend(legend, labels, loc='upper right', bbox_to_anchor=(1, -0.3), ncol=8)
    if save:
        plt.savefig(suptitle + ".png", bbox_inches="tight", dpi=200)
        fig.suptitle(suptitle, fontsize=16)
        plt.savefig(suptitle + "_labelled.png", dpi=200)
    else:
        plt.show()

# create barcharts for distribution of answers in each question, and the gaussian curve
def analyze_distribution(dataset):

    data = dataset.df.drop(columns=[dataset.wave_col])
    num_features = len(data.columns)
    means = np.mean(data, axis=0)
    std_devs = np.std(data, axis=0)

    # Print mean and standard deviation for each feature
    for i in range(num_features):
        print(f"Feature {i + 1}:")
        print(f"Mean: {means[i]}")
        print(f"Standard Deviation: {std_devs[i]}")
        print()

    fig, axs = plt.subplots(1, num_features, gridspec_kw={'wspace': 0.5})
    for i in range(num_features):
        x = [1, 2, 3, 4]
        x = [0/3, 1/3, 2/3, 3/3]
        counts = data.iloc[:, i].value_counts()
        counts = {val: count / len(data.iloc[:, i]) for val, count in counts.items()}
        y = [counts[x[i]] for i in range(4)]
        axs[i].bar(x, y, color=["b", "g", "r", "c"], alpha=0.6, width=0.2)

        print(shapiro(data.iloc[:, i].dropna()))
        mean, std = norm.fit(data.iloc[:, i].dropna())
        xmin, xmax = plt.xlim()
        x = np.linspace(xmin - 0.5, xmax + 0.5, 1000)
        p = norm.pdf(x, mean, std)
        axs[i].plot(x, p, 'k', linewidth=0.5)
        axs[i].set_xlabel(col2label[data.columns[i]])
        axs[i].set_xticks([])
        axs[i].set_yticks([])
        axs[i].set_box_aspect(2)
        #title = f"Feature {i + 1}: Fit results: mean = {mean:.2f}, std dev = {std:.2f}"
        #axs[i].title(title)

    legend = [
        Line2D([0], [0], color='b', lw=4, label='A great deal'),
        Line2D([0], [0], color='g', lw=4, label='Quite a lot'),
        Line2D([0], [0], color='r', lw=4, label='Not very much'),
        Line2D([0], [0], color='c', lw=4, label='None at all'),
        Line2D([0], [0], color='k', lw=1, label='Normal Distribution')
    ]
    plt.legend(legend, ["A great deal", "Quite a lot", "Not very much", "None at all", "Normal Distribution"], loc='center', bbox_to_anchor=(-7, -0.5), ncol=5)
    plt.show()


def weight_features(survey, weights=None):
    if weights is None:
        weights = {"core": 0.75, "extra": 0.25}

    column_weights = []
    num_core = len(core_cols)
    if survey.wave_col in survey.df.columns:
        num_extra = len(survey.df.columns) - 1 - num_core
    else:
        num_extra = len(survey.df.columns) - num_core
    for col in survey.df.columns:
        if col in core_cols:
            column_weights.append(weights["core"] / num_core)
        elif col == survey.wave_col:
            column_weights.append(1)
        else:
            column_weights.append(weights["extra"] / num_extra)
    print("weight sum", np.sum(column_weights))
    df = survey.df
    df = df * column_weights
    df = df.astype({wave_col: int})
    columns = df.columns
    columns = columns.drop(wave_col)
    scaler = MinMaxScaler().fit(df[columns])
    scales = scaler.scale_
    core_scale = scales[0]
    df[columns] = df[columns] * core_scale
    #df[columns] = MinMaxScaler().fit_transform(df[columns])
    survey.df = df
    survey.waves = []
    survey.init_waves(wave_col)

# make a figure with 5 frames, each frame showing the projection of the dataset with different feature weights
# not used in the thesis, but used during the development
def generate_weight_transformation(survey):
    num_frames = 5
    start_core = 1
    end_core = len(survey.core_cols) / (len(survey.all_cols) - 1)
    start_extra = 0
    end_extra = 1 - end_core
    core_weights = np.linspace(start_core, end_core, num_frames)
    extra_weights = np.linspace(start_extra, end_extra, num_frames)

    original_survey = copy.deepcopy(survey)
    fig, axs = plt.subplots(num_frames, original_survey.num_waves)
    for frame in range(num_frames):
        print("Frame", frame)
        frame_survey = copy.deepcopy(original_survey)

        # feature weighting
        weight_features(frame_survey, weights={"core": core_weights[frame], "extra": extra_weights[frame]})
        frame_survey.waves = []
        frame_survey.init_waves(wave_col)

        # clustering
        frame_survey = get_clusters_for_waves(frame_survey, columns="all")
        frame_survey = match_clusters(frame_survey, method="centroid")

        # projecting
        projections = []
        for wave in frame_survey.waves:
            projection = dr(wave.df)
            projections.append(projection)

        indexes = []
        for wave in frame_survey.waves:
            indexes.append([i for i in range(len(wave.df))])

        # plotting
        for cluster, color in zip(range(6), cluster_colors):
            axs[frame, 0].scatter(projections[0][[i for i in indexes[0] if frame_survey.waves[0].cluster_labels[i] == cluster], 0],
                           projections[0][[i for i in indexes[0] if frame_survey.waves[0].cluster_labels[i] == cluster], 1],
                           c=color, label="Cluster {}".format(cluster), s=1)
            axs[frame, 1].scatter(projections[1][[i for i in indexes[1] if frame_survey.waves[1].cluster_labels[i] == cluster], 0],
                           projections[1][[i for i in indexes[1] if frame_survey.waves[1].cluster_labels[i] == cluster], 1],
                           c=color, label="Cluster {}".format(cluster), s=1)
            axs[frame, 2].scatter(projections[2][[i for i in indexes[2] if frame_survey.waves[2].cluster_labels[i] == cluster], 0],
                           projections[2][[i for i in indexes[2] if frame_survey.waves[2].cluster_labels[i] == cluster], 1],
                           c=color, label="Cluster {}".format(cluster), s=1)
            axs[frame, 3].scatter(projections[3][[i for i in indexes[3] if frame_survey.waves[3].cluster_labels[i] == cluster], 0],
                           projections[3][[i for i in indexes[3] if frame_survey.waves[3].cluster_labels[i] == cluster], 1],
                           c=color, label="Cluster {}".format(cluster), s=1)
            axs[frame, 4].scatter(projections[4][[i for i in indexes[4] if frame_survey.waves[4].cluster_labels[i] == cluster], 0],
                           projections[4][[i for i in indexes[4] if frame_survey.waves[4].cluster_labels[i] == cluster], 1],
                           c=color, label="Cluster {}".format(cluster), s=1)

            axs[frame, 0].set(adjustable='box', aspect='equal')
            axs[frame, 1].set(adjustable='box', aspect='equal')
            axs[frame, 2].set(adjustable='box', aspect='equal')
            axs[frame, 3].set(adjustable='box', aspect='equal')
            axs[frame, 4].set(adjustable='box', aspect='equal')

        #plt.show()

    for ax, col in zip(axs[0], ["Wave 1", "Wave 2", "Wave 3", "Wave 4", "Wave 5"]):
        ax.set_title(col)

    labels = ["Core {}, Extra {}".format(round(core, 2), round(extra, 2)) for core, extra in zip(core_weights, extra_weights)]
    for ax, row in zip(axs[:, 0], labels):
        if row == labels[-1]:
            row += "\n Default"
        ax.set_ylabel(row, rotation=70, size='large')

    fig.suptitle("Effect of changing feature weights on static projection with filled in dimensions", fontsize=16)
    plt.show()

# uncomment functions to run them
# always has to run prepare_EVS() first
if __name__ == '__main__':
    #prepare_PPP()
    survey = prepare_EVS()
    #analyze_distribution(survey)
    #survey = fill_missing_values_by_distribution(survey)
    #survey = unify_with_autoencoder(survey, extra_n=2)
    #weight_features(survey, weights={"core": 0.82, "extra": 0.18})
    #analyze_distribution(survey)
    #elbow_plots(survey)

    #survey = get_clusters_for_waves(survey, columns="all")
    #survey = match_clusters(survey, method="centroid")
    #survey = unify_cluster_encoding(survey)

    #survey = get_clusters_for_waves(survey, columns="core")
    #neural_projection(survey, match="centroid")
    #project_df(survey, match="centroid")

    #cluster_question_stats(survey, match=None, suptitle="No cluster matching")
    #cluster_question_stats(survey, match="centroid", suptitle="Clusters matched using centroid linkage")
    #cluster_question_stats(survey, match="pearson", suptitle="Clusters matched using Pearson correlation")
    #cluster_question_stats(survey, match="average", suptitle="Clusters matched using average linkage")

    #project_df(survey, match="pearson")
    #neural_projection(survey, match="pearson")
    #df_unified = unify_dimensions(df, 3, "simple")
    #neural_projection(df_unified, df_unified)
    #generate_weight_transformation(survey)