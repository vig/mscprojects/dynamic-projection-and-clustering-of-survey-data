import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from eval import metric_cluster_movement, metric_cluster_movement_one_way
# activate latex text rendering

mean_1a = np.array([0, 10])
mean_1b = np.array([0, 0])
mean_1c = np.array([0, -10])
mean_1d = np.array([0, -20])
std_1x = 0.3
std_1y = 1

mean_2 = np.array([5, 10])
std_2x = 0.3
std_2y = 1

mean_3 = np.array([5, 0])
std_3x = 0.6
std_3y = 1

mean_4 = np.array([5, -10])
std_4x = 0.3
std_4y = 2

mean_5 = np.array([5, -20])
std_5x = 0.6
std_5y = 2

n = 1000
cluster_1a = np.random.normal(mean_1a, [std_1x, std_1y], (n, 2))
cluster_1b = np.random.normal(mean_1b, [std_1x, std_1y], (n, 2))
cluster_1c = np.random.normal(mean_1c, [std_1x, std_1y], (n, 2))
cluster_1d = np.random.normal(mean_1d, [std_1x, std_1y], (n, 2))
cluster_2 = np.random.normal(mean_2, [std_2x, std_2y], (n, 2))
cluster_3 = np.random.normal(mean_3, [std_3x, std_3y], (n, 2))
cluster_4 = np.random.normal(mean_4, [std_4x, std_4y], (n, 2))
cluster_5 = np.random.normal(mean_5, [std_5x, std_5y], (n, 2))

def rotation_matrix_from_degrees(degrees):
    theta = np.radians(degrees)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c, -s), (s, c)))
    return R

def rotate_cluster(cluster, degrees):
    translation_matrix = np.array([np.mean(cluster, axis=0)])
    translation_matrix_neg = -translation_matrix
    rotation_matrix = rotation_matrix_from_degrees(degrees)
    cluster = cluster + translation_matrix_neg
    cluster = np.dot(cluster, rotation_matrix)
    cluster = cluster + translation_matrix
    return cluster


cluster_2 = rotate_cluster(cluster_2, 45)
cluster_3 = rotate_cluster(cluster_3, 45)
cluster_4 = rotate_cluster(cluster_4, 45)
cluster_5 = rotate_cluster(cluster_5, 45)

clusters = [cluster_1a, cluster_1b, cluster_1c, cluster_1d, cluster_2, cluster_3, cluster_4, cluster_5]
colors = ['red', 'red', 'red', 'red', 'green', 'cyan', 'yellow', 'magenta']
cluster_labels = [1, 1, 1, 1, 2, 3, 4, 5]

def cluster_average_linkage(cluster_1, cluster_2):
    dist_matrix = sp.spatial.distance.cdist(cluster_1, cluster_2, metric="euclidean")
    return np.mean(dist_matrix)

def cluster_cosine_sim(cluster_1, cluster_2):
    cov_1 = np.cov(cluster_1.T)
    cov_2 = np.cov(cluster_2.T)

    eigenvalues_1, eigenvectors_1 = np.linalg.eig(cov_1)
    eigenvalues_2, eigenvectors_2 = np.linalg.eig(cov_2)

    eigenvectors_1_sorted = eigenvectors_1[:, eigenvalues_1.argsort()]
    eigenvectors_2_sorted = eigenvectors_2[:, eigenvalues_2.argsort()]

    avg_sim = 0
    for i in range(len(eigenvectors_1_sorted)):
        v_1 = eigenvectors_1_sorted[i]
        v_2 = eigenvectors_2_sorted[i]
        sim = np.dot(v_1, v_2) / (np.linalg.norm(v_1) * np.linalg.norm(v_2))
        avg_sim += sim

    return avg_sim / len(eigenvectors_1_sorted)

def centroid_linkage(cluster_1, cluster_2):
    return np.linalg.norm(np.mean(cluster_1, axis=0) - np.mean(cluster_2, axis=0))

def compute_value(metric, cluster_a, cluster_b):
    return metric(cluster_a, cluster_b)

triples = [
    (compute_value(metric_cluster_movement, cluster_1a, cluster_2), compute_value(metric_cluster_movement, cluster_1b, cluster_3), compute_value(metric_cluster_movement, cluster_1c, cluster_4), compute_value(metric_cluster_movement, cluster_1d, cluster_5)),
    (compute_value(metric_cluster_movement_one_way, cluster_1a, cluster_2), compute_value(metric_cluster_movement_one_way, cluster_1b, cluster_3), compute_value(metric_cluster_movement_one_way, cluster_1c, cluster_4), compute_value(metric_cluster_movement_one_way, cluster_1d, cluster_5)),
    (compute_value(cluster_cosine_sim, cluster_1a, cluster_2), compute_value(cluster_cosine_sim, cluster_1b, cluster_3), compute_value(cluster_cosine_sim, cluster_1c, cluster_4), compute_value(cluster_cosine_sim, cluster_1d, cluster_5)),
    (compute_value(centroid_linkage, cluster_1a, cluster_2), compute_value(centroid_linkage, cluster_1b, cluster_3), compute_value(centroid_linkage, cluster_1c, cluster_4), compute_value(centroid_linkage, cluster_1d, cluster_5)),
    (compute_value(cluster_average_linkage, cluster_1a, cluster_2), compute_value(cluster_average_linkage, cluster_1b, cluster_3), compute_value(cluster_average_linkage, cluster_1c, cluster_4), compute_value(cluster_average_linkage, cluster_1d, cluster_5)),
]
legend_labels = [
    ("Mahalanobis 1, 2", "Mahalanobis 1, 3", "Mahalanobis 1, 4", "Mahalanobis 1, 5"),
    ("Mahalanobis one way 2->1", "Mahalanobis one way 3->1", "Mahalanobis one way 4->1", "Mahalanobis one way 5->1"),
    ("Cosine Sim 1, 2", "Cosine Sim 1, 3", "Cosine Sim 1, 4", "Cosine Sim 1, 5"),
    ("Centroid 1, 2", "Centroid 1, 3", "Centroid 1, 4", "Centroid 1, 5"),
    ("Avg Linkage 1, 2", "Avg Linkage 1, 3", "Avg Linkage 1, 4", "Avg Linkage 1, 5")
]

fig, ax = plt.subplots()
for i in range(len(clusters)):
    mean = np.mean(clusters[i], axis=0)
    color = colors[i]
    ax.scatter(clusters[i][:, 0], clusters[i][:, 1], c=color)
    ax.scatter(mean[0], mean[1], c='black', marker='x')
    ax.text(mean[0], mean[1], f"Cluster {cluster_labels[i]}", fontsize=12)


legend = []
for i in range(len(triples)):
    v1, v2, v3, v4 = triples[i]
    label1, label2, label3, label4 = legend_labels[i]
    max_v = max(v1, v2, v3)
    min_v = min(v1, v2, v3)
    if ("Cosine" in label1 and max_v == v1) or (not "Cosine" in label1 and min_v == v1):
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}*".format(label1, round(v1, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label2, round(v2, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label3, round(v3, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label4, round(v4, 2)), markerfacecolor='white', markersize=0)),
    elif ("Cosine" in label2 and max_v == v2) or (not "Cosine" in label2 and min_v == v2):
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label1, round(v1, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}*".format(label2, round(v2, 2)), markerfacecolor='white',markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label3, round(v3, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label4, round(v4, 2)), markerfacecolor='white', markersize=0)),
    elif ("Cosine" in label3 and max_v == v3) or (not "Cosine" in label3 and min_v == v3):
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label1, round(v1, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label2, round(v2, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}*".format(label3, round(v3, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label4, round(v4, 2)), markerfacecolor='white', markersize=0)),
    else:
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label1, round(v1, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label2, round(v2, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}".format(label3, round(v3, 2)), markerfacecolor='white', markersize=0)),
        legend.append(plt.Line2D([0], [0], color='w', label="{}: {}*".format(label4, round(v4, 2)), markerfacecolor='white', markersize=0)),

    legend.append(plt.Line2D([0], [0], color='w', label='-----------------------', markerfacecolor='white', markersize=0)),

ax.legend(handles=legend, loc='upper right')
plt.suptitle("Distances and similarities between clusters using various metrics")
ax.set_xlim(-5, 25)
ax.set_ylim(-25, 15)
plt.show()








