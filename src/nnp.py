from keras import datasets as kdatasets
from keras.callbacks import EarlyStopping
from keras.initializers import Constant
from keras.layers import Dense
from keras.models import Sequential
import pandas as pd
import time

from distances import *
from consts import *

# train_cols = "all" | "core" | list of columns
def nnp(dataset, train_cols="core"):
    new_df = dataset.df

    # select correct dimensions
    if train_cols == "core":
        new_df = new_df[dataset.core_cols]
        new_df[dataset.wave_col] = dataset.df[dataset.wave_col]
    elif train_cols != "all":
        new_df = new_df[dataset.all_cols]
        new_df[dataset.wave_col] = dataset.df[dataset.wave_col]

    print(new_df.shape)

    # get training data
    num_samples = 30000
    wave1_count = len(new_df[new_df[wave_col] == 1])
    wave2_count = len(new_df[new_df[wave_col] == 2])
    wave3_count = len(new_df[new_df[wave_col] == 3])
    wave4_count = len(new_df[new_df[wave_col] == 4])
    wave5_count = len(new_df[new_df[wave_col] == 5])

    all_count = wave1_count + wave2_count + wave3_count + wave4_count + wave5_count

    train_df = pd.concat([
        new_df[new_df[wave_col] == 1].sample(n=int(num_samples * (wave1_count / all_count))),
        new_df[new_df[wave_col] == 2].sample(n=int(num_samples * (wave2_count / all_count))),
        new_df[new_df[wave_col] == 3].sample(n=int(num_samples * (wave3_count / all_count))),
        new_df[new_df[wave_col] == 4].sample(n=int(num_samples * (wave4_count / all_count))),
        new_df[new_df[wave_col] == 5].sample(n=int(num_samples * (wave5_count / all_count))),
    ])

    # get test data
    test_df = new_df[~new_df.index.isin(train_df.index)]

    X_train = train_df.drop([wave_col], axis=1)
    X_test = test_df.drop([wave_col], axis=1)
    X_train_numpy = X_train.to_numpy()
    X_test_numpy = X_test.to_numpy()

    # project the train data to 2D
    start_time_dr = time.time()
    X_train_2D = dr(X_train)
    end_time_dr = time.time()

    # train the model
    start_time_train = time.time()
    model, hist = train_model(X_train_numpy, X_train_2D)
    end_time_train = time.time()
    loss = hist.history['loss']
    val_loss = hist.history['val_loss']

    # project the test data to 2D
    start_time_predict = time.time()
    X_test_2D = model.predict(X_test_numpy)
    end_time_predict = time.time()

    X_train["x"] = X_train_2D[:, 0]
    X_train["y"] = X_train_2D[:, 1]
    X_train[wave_col] = train_df[wave_col]

    X_test["x"] = X_test_2D[:, 0]
    X_test["y"] = X_test_2D[:, 1]
    X_test[wave_col] = test_df[wave_col]

    output_df = pd.concat([X_train, X_test]).sort_index()

    print("DR time:", end_time_dr - start_time_dr)
    print("Train time:", end_time_train - start_time_train)
    print("Predict time:", end_time_predict - start_time_predict)

    return output_df, loss, val_loss

def train_model(X, X_2d, loss='mean_squared_error', epochs=None):
    callbacks = []

    if not epochs:
        stop = EarlyStopping(verbose=1, min_delta=0.00001, mode='min', patience=10, restore_best_weights=True)
        callbacks.append(stop)

    m = Sequential()
    m.add(Dense(256, activation='relu',
                kernel_initializer='he_uniform',
                bias_initializer=Constant(0.0001),
                input_shape=(X.shape[1],)))
    m.add(Dense(512, activation='relu',
                kernel_initializer='he_uniform',
                bias_initializer=Constant(0.0001)))
    m.add(Dense(256, activation='relu',
                kernel_initializer='he_uniform',
                bias_initializer=Constant(0.0001)))
    m.add(Dense(2, activation='sigmoid',
                kernel_initializer='he_uniform',
                bias_initializer=Constant(0.0001)))
    m.compile(loss=loss, optimizer='adam')

    hist = m.fit(X, X_2d, batch_size=32, epochs=200, verbose=1, validation_split=0.05, callbacks=callbacks)

    return m, hist
