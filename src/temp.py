from glob import glob

results_path = '../experiments/results/dynamic_results_tf_ae_073_027.csv'
csv_paths = sorted(glob(results_path))

for csv_path in csv_paths:
    with open(csv_path, 'r') as f:
        contents = f.read().split('\n')

    with open(csv_path, 'w') as f:
        final_text = ''
        for line in contents:
            split_line = line.split(',')
            new_line = split_line[1:]
            final_text += (';'.join(new_line) + '\n')
        f.write(final_text)

