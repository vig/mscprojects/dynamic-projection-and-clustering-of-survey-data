import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import pandas as pd
from scipy.stats import ttest_ind, ttest_rel


dist_high_NNP_core = np.load("dists_high_NNP_core_wave_0.npy")
dist_high_TF_core = np.load("dists_high_TF_core_wave_0.npy")

dists_low_NNP_core = np.array([])
dists_low_TF_core = np.array([])

for i in range(0, 5):
    dists_low_NNP_core = np.append(dists_low_NNP_core, np.load(f"dists_low_NNP_core_wave_{i}.npy"))
    dists_low_TF_core = np.append(dists_low_TF_core, np.load(f"dists_low_TF_core_wave_{i}.npy"))

print(np.equal(dist_high_NNP_core, dist_high_TF_core).all())

#print(sp.stats.shapiro(dist_low_NNP_core))
#print(sp.stats.shapiro(dist_low_TF_core))
rel_test = ttest_rel(dists_low_NNP_core, dists_low_TF_core)
tf_std = np.std(dists_low_TF_core)
nnp_std = np.std(dists_low_NNP_core)
pooled_std = np.sqrt((tf_std ** 2 + nnp_std ** 2) / 2)
cohen_d = (np.mean(dists_low_NNP_core) - np.mean(dists_low_TF_core)) / pooled_std

low_diff = dists_low_NNP_core - dists_low_TF_core
diff_mean = np.mean(low_diff)
diff_std = np.std(low_diff)
n = len(low_diff)

t = diff_mean / (diff_std / np.sqrt(n))
print(t)
print(rel_test)
print(rel_test.confidence_interval(0.95))
print(cohen_d)
