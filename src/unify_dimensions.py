import keras.backend as K
from keras.layers import Input, Dense
from keras.models import Model
from keras.callbacks import EarlyStopping
from sklearn.base import BaseEstimator, TransformerMixin
import os
from enum import Enum
from clusters import *
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
from scipy.stats import norm

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def fill_missing_values_by_distribution(dataset):
    data = dataset.df.drop(columns=[dataset.wave_col])

    num_features = len(data.columns)

    for i in range(num_features):
        mean, std = norm.fit(data.iloc[:, i].dropna())
        missing_indices = np.isnan(data.iloc[:, i])
        imputed_values = norm.rvs(mean, std, size=np.sum(missing_indices))

        if len(imputed_values) > 0:
            imputed_values = MinMaxScaler().fit_transform(imputed_values.reshape(-1, 1)).flatten()

            # change imputed values to possible values
            possible_values = [0/3, 1/3, 2/3, 3/3]
            for j in range(len(imputed_values)):
                imputed_values[j] = possible_values[np.argmin(np.abs(possible_values - imputed_values[j]))]
        data.iloc[missing_indices, i] = imputed_values

    data.loc[:, dataset.wave_col] = dataset.df[dataset.wave_col]
    dataset.df = data
    dataset.waves = []
    dataset.init_waves(wave_col)
    #dataset.update_wave_dfs()
    return dataset

# cluster encoding unification method (not used in thesis, but mentioned)
def unify_cluster_encoding(dataset):
    all_labels = []
    for wave in range(dataset.num_waves):
        all_labels.extend(dataset[wave].cluster_labels)
    extra_n = len(np.unique(all_labels))
    extra_labels = [f"extra_{i}" for i in range(extra_n)]
    df = dataset.df
    extra_df = pd.DataFrame(np.zeros((df.shape[0], extra_n)), columns=extra_labels, index=df.index)
    for wave in range(dataset.num_waves):
        print("wave", wave + 1)
        df_wave = dataset.df[dataset.df[dataset.wave_col] == wave + 1]
        wave_labels = dataset[wave].cluster_labels
        extra_dims = np.zeros((len(wave_labels), extra_n))
        extra_df.loc[df_wave.index, :] = extra_dims
        print(extra_dims.shape)
        for i in range(extra_n):
            extra_dims[wave_labels == i, i] = 1
        extra_df.loc[df_wave.index, :] = extra_dims
    new_df = pd.concat([dataset.df[[dataset.wave_col] + dataset.core_cols], extra_df], axis=1)
    dataset.df = new_df
    dataset.all_cols = new_df.columns
    dataset.waves = []
    dataset.init_waves(wave_col)
    return dataset

def unify_with_autoencoder(dataset, extra_n = 3):
    extra_labels = [f"extra_{i}" for i in range(extra_n)]
    au = AutoencoderProjection(extra_n, ModelSize.SMALL)
    df = dataset.df
    extra_df = pd.DataFrame(np.zeros((df.shape[0], extra_n)), columns=extra_labels, index=df.index)
    for wave in range(dataset.num_waves):
        df_wave = df[df[dataset.wave_col] == wave + 1]
        df_wave = df_wave.drop([dataset.wave_col] + dataset.core_cols, axis=1)
        df_wave = df_wave.loc[:, df_wave.notna().all()]
        if len(df_wave.columns) == 0:
            # input random values
            extra_df.loc[df_wave.index, :] = np.random.rand(df_wave.shape[0], extra_n)
            continue

        extra_dims = au.fit_transform(df_wave.to_numpy())
        if len(df_wave.columns) < extra_n:
            random_values = np.random.standard_normal((extra_dims.shape[0], extra_n - len(df_wave.columns)))
            extra_dims[:, len(df_wave.columns):] = random_values + extra_dims[:, len(df_wave.columns):]

        extra_dims = MinMaxScaler().fit_transform(extra_dims)
        extra_df.loc[df_wave.index, :] = extra_dims
    new_df = pd.concat([df[[wave_col] + core_cols], extra_df], axis=1)
    dataset.df = new_df
    dataset.all_cols = new_df.columns
    dataset.waves = []
    dataset.init_waves(wave_col)
    return dataset

class ModelSize(Enum):
    SMALL = 0
    MEDIUM = 1
    LARGE = 2

# taken from https://github.com/mespadoto/dlmp
class AutoencoderProjection(BaseEstimator, TransformerMixin):
    def __init__(self, target_dim=2, model_size=ModelSize.SMALL):
        self.autoencoder = None
        self.encoder = None
        self.model_size = model_size
        self.target_dim = target_dim
        self.stopper = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=50, verbose=False, mode='min')

    def set_params(self, n_components, model_size):
        self.model_size = model_size

    def fit_transform(self, X, y=None, **fit_params: dict):
        K.clear_session()
        #tf.reset_default_graph()

        if self.model_size == ModelSize.SMALL:
            ae_input = Input(shape=(X.shape[1],))
            encoded = Dense(self.target_dim, activation='linear')(ae_input)
            decoded = Dense(X.shape[1], activation='sigmoid')(encoded)
        elif self.model_size == ModelSize.MEDIUM:
            ae_input = Input(shape=(X.shape[1],))
            encoded = Dense(16, activation='sigmoid')(ae_input)
            encoded = Dense(self.target_dim, activation='linear')(encoded)
            decoded = Dense(16, activation='sigmoid')(encoded)
            decoded = Dense(X.shape[1], activation='sigmoid')(decoded)
        elif self.model_size == ModelSize.LARGE:
            ae_input = Input(shape=(X.shape[1],))
            encoded = Dense(128, activation='sigmoid')(ae_input)
            encoded = Dense(32, activation='sigmoid')(encoded)
            encoded = Dense(self.target_dim, activation='linear')(encoded)
            decoded = Dense(32, activation='sigmoid')(encoded)
            decoded = Dense(128, activation='sigmoid')(decoded)
            decoded = Dense(X.shape[1], activation='sigmoid')(decoded)

        self.encoder = Model(inputs=ae_input, outputs=encoded)
        self.autoencoder = Model(ae_input, decoded)
        self.autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

        self.autoencoder.fit(X, X, epochs=1000, batch_size=32, shuffle=True,
                             validation_split=0.1, verbose=True, callbacks=[self.stopper])
        return self.encoder.predict(X)